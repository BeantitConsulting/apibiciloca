<?php 
namespace Fuel\Migrations;

class Config
{

    function up()
    {
        \DBUtil::create_table('config', array(
            'id' => array('type' => 'int', 'constraint' => 5),
            'persons' => array('type' => 'int', 'constraint' => 2),
        ), array('id'));
    }

    function down()
    {
       \DBUtil::drop_table('config');
    }
}