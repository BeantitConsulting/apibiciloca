<?php 
namespace Fuel\Migrations;

class Bookings
{

    function up()
    {
        \DBUtil::create_table('bookings', array(
            'id' => array('type' => 'int', 'constraint' => 5),
            'name' => array('type' => 'varchar', 'constraint' => 100),
            'surname' => array('type' => 'varchar', 'constraint' => 100),
            'persons' => array('type' => 'varchar', 'constraint' => 5),
            'date' => array('type' => 'varchar', 'constraint' => 20),
            'start_hour' => array('type' => 'varchar', 'constraint' => 11),
            'end_hour' => array('type' => 'varchar', 'constraint' => 11),
            'phone' => array('type' => 'varchar', 'constraint' => 30),
            'email' => array('type' => 'varchar', 'constraint' => 100),
            'price' => array('type' => 'varchar', 'constraint' => 11),
            'created_at' => array('type' => 'varchar', 'constraint' => 20),
            'updated_at' => array('type' => 'varchar', 'constraint' => 20),

        ), array('id'));
    }

    function down()
    {
       \DBUtil::drop_table('bookings');
    }
}
