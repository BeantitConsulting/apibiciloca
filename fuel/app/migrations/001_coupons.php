<?php 
namespace Fuel\Migrations;

class Coupons
{

    function up()
    {
        \DBUtil::create_table('coupons', array(
            'id' => array('type' => 'int', 'constraint' => 5),
            'coupon' => array('type' => 'varchar', 'constraint' => 100),
            'client' => array('type' => 'varchar', 'constraint' => 50),
            'alias' => array('type' => 'varchar', 'constraint' => 5),
            'discount_percentaje' => array('type' => 'int', 'constraint' => 2),
            'discount_value' => array('type' => 'int', 'constraint' => 5),
        ), array('id'));

        \DB::query("ALTER TABLE `coupons` ADD UNIQUE (`coupon`)")->execute();
    }

    function down()
    {
       \DBUtil::drop_table('coupons');
    }
}