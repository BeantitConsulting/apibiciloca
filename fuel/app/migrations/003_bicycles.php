<?php 
namespace Fuel\Migrations;

class Bicycles
{

    function up()
    {
        \DBUtil::create_table('bicycles', array(
            'id' => array('type' => 'int', 'constraint' => 5),
            'day' => array('type' => 'varchar', 'constraint' => 10),
            'numberBicycles' => array('type' => 'int', 'constraint' => 5),
        ), array('id'));
    }

    function down()
    {
       \DBUtil::drop_table('bicycles');
    }
}