<!doctype html>
<html lang="en">
    <head>

    </head>
    <body>
        <img src="http://biciloca.com/img/emailLogo.png">
        <p>We confirm that your reservation has been successfully completed. These are the data of it:</p>

        <h3>Contact information</h3>
        <p><b>Name: </b> <?php echo $nombre . ' ' . $apellidos ?></p>
        <p><b>Email: </b> <?php echo $email ?></p>
        <p><b>Phone: </b> <?php echo $telefono ?></p>
        <p><b>People: </b> <?php echo $personas ?></p>

        <h3>Details</h3>
        <p><b>Date: </b> <?php echo $fecha ?></p>
        <p><b>Start: </b> <?php echo $horainicio ?></p>
        <p><b>End: </b> <?php echo $horafin ?></p>
        <p><b>Price </b> <?php echo $precio ?>€</p>
        <p><b>Meeting point: </b> St. Rambla Santa Monica 7, Barcelona. Metro stop Drasanes</p>
        <img src="http://biciloca.com/img/punto-encuentro.jpg">
        <p>Booking system</p>
        <p>(+34) 634556224</p>
    </body>
</html>