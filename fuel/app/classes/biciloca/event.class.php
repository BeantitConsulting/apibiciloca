<?php 
namespace Biciloca;

class Event
{
    private $client;
    private $schedule;
    private $title;
    private $interval;
    
    function __construct(Client $client, Schedule $schedule, $event)
    {
        $this->client = $client;
        $this->schedule = $schedule;
        $this->title = $event['summary'];
        
        $start = $event['start']['dateTime'];
        $end = $event['end']['dateTime'];

        $start = explode('T', $start);

        $startDay = $start[0];

        $this->interval['startDay'] = $startDay;

        $startTimeWithoutGMT = explode('+', $start[1]);

        $starTimeHourMin = explode(':', $startTimeWithoutGMT[0]);

        $startTime = $starTimeHourMin[0] . ':' . $starTimeHourMin[1];

        $this->interval['startTime'] = $startTime;

        $end = explode('T', $end);

        $endDay = $end[0];

        $this->interval['endDay'] = $endDay;

        $endTimeWithoutGMT = explode('+', $end[1]);

        $endimeHourMin = explode(':', $endTimeWithoutGMT[0]);

        $endTime = $endimeHourMin[0] . ':' . $endimeHourMin[1];

        $this->interval['endTime'] = $endTime;

        // $this->setSchedule($startTime, $endTime);

    }

    private function setSchedule($startTime, $endTime)
    {
        if (strtotime($startTime) > strtotime($endTime) )
        {
            throw new Exception('start time must be below end time');
        }

        $schedule = $this->schedule->getSchedule();

        Helper::dd($schedule);

        foreach ($schedule as $key => $time) 
        {
            if ( strtotime($startTime) <= strtotime($time) and strtotime($endTime) >= strtotime($time) ) 
            {
                $newSchedule[] = $time;
            }
        }

        $schedule = new Schedule();
        $schedule->setSchedule($newSchedule);
        $this->schedule = $schedule;
    }

    public function getSchedule()
    {
        return $this->schedule;
    }

    public function getTitle()
    {
        return $this->title;
    }

    public function getScheduleToArray()
    {
        return $this->schedule->getScheduleToArray();
    }

    public function getStartDay()
    {
        return  $this->interval['startDay'];
    }

    public function getEndDay()
    {
        return $this->interval['endDay'];
    }

    public function getStartTime()
    {
        return $this->interval['startTime'];
    }

    public function getEndTime()
    {
        return $this->interval['endTime'];
    }

    public function getNumberPeople()
    {
        if (trim($this->getType()) == '[share]') 
        {
            return $this->client->getNumberPeople();
        }

        return 18; 
    }

    public function getType()
    {
        return $this->client->getType();
    }

    public function getInterval()
    {
        $startTime = $this->interval['startTime'];
        $endTime = $this->interval['endTime'];
        $hours = $this->schedule->getSimpleHours();
        $interval = [];
        foreach ($hours as $key => $hour) 
        {
            if (strtotime($startTime) <= strtotime($hour) and strtotime($hour) <= strtotime($endTime)) 
            {
                $interval[] = $hour;
            }
        }

        return $interval;
    }

    public function getIntervalRemove30min()
    {
        $startTime = $this->removeTime($this->interval['startTime'], 30);
        $endTime = $this->removeTime($this->interval['endTime'], 30);
        $hours = $this->schedule->getSimpleHours();

        $interval = [];
        foreach ($hours as $key => $hour) 
        {
            if (strtotime($startTime) <= strtotime($hour) and strtotime($hour) <= strtotime($endTime)) 
            {
                $interval[] = $hour;
            }
        }

        return $interval;
    }

    public function getStartTimeRemove30min()
    {
        $intervarRemove30min = $this->getIntervalRemove30min();
        return $intervarRemove30min[0];
    }

    private function removeTime($time, $removeMinutes)
    {
        $timeToSeconds = strtotime($time);
        $removeMinutesToSeconds = $removeMinutes * 60;
        $newTime = date("H:i", $timeToSeconds - $removeMinutesToSeconds);
        return $newTime;
    }
}