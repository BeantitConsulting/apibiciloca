<?php 
namespace Biciloca;

define('ATRIBUTES_OF_PUBLIC_EVENT', 7);
define('ATRIBUTES_OF_PRIVATE_EVENT', 6);
define('MAX_PEOPLE', 18);

class Client
{
    private $name = null;
    private $numberPeople = null;
    private $time = null;
    private $phone = null;
    private $location = null;
    private $money = null;
    private $type = null;

    function __construct($event)
    {
        $splitSummary = explode('-', $event['summary']);
        // Helper::dd($splitSummary);

        if (count($splitSummary) == ATRIBUTES_OF_PUBLIC_EVENT) 
        {
            // Helper::dd($splitSummary);
            $this->type = $splitSummary[0];
            $this->name = $splitSummary[1];
            $this->numberPeople = $this->getNumbersString($splitSummary[2]);
            $this->time = $this->getNumbersString($splitSummary[3]);
            $this->phone = $splitSummary[4];
            $this->location = $splitSummary[5];
            $this->money = $this->getNumbersString($splitSummary[6]);
        }

        if (count($splitSummary) == ATRIBUTES_OF_PRIVATE_EVENT) 
        {
            // Helper::dd($splitSummary);
            $this->type = '';
            $this->name = $splitSummary[0];
            $this->numberPeople = $this->getNumbersString($splitSummary[1]);
            $this->time = $this->getNumbersString($splitSummary[2]);
            $this->phone = $splitSummary[3];
            $this->location = $splitSummary[4];
            $this->money = $this->getNumbersString($splitSummary[5]);
        }
        
    }

    private function getNumbersString($string)
    {
        return intval(preg_replace('/[^0-9]+/', '', $string), 10); 
    }

    public function getNumberPeople()
    {
        return $this->numberPeople;
    }

    public function getType()
    {
        return $this->type;
    }

    public function isFull()
    {
        return $this->numberPeople >= MAX_PEOPLE;
    }
}