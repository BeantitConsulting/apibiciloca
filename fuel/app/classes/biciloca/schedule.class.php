<?php 
namespace Biciloca;

class Schedule 
{
    private $hours;

    function __construct()
    {
        $this->hours = [
                           [
                            '10:00', '10:30', '11:00', '11:30', '12:00', '12:30', '13:00', '13:30', '14:00',
                            '14:30', '15:00', '15:30', '16:00', '16:30', '17:00', '17:30', '18:00', '18:30',
                            '19:00', '19:30', '20:00', '20:30', '21:00', '21:30', '22:00'
                           ],
                           [
                            '10:00', '10:30', '11:00', '11:30', '12:00', '12:30', '13:00', '13:30', '14:00',
                            '14:30', '15:00', '15:30', '16:00', '16:30', '17:00', '17:30', '18:00', '18:30',
                            '19:00', '19:30', '20:00', '20:30', '21:00', '21:30', '22:00'
                           ]
                        ];
    }

    public function getHours()
    {
        return $this->hours;
    }

    public function getSimpleHours()
    {
        return $this->hours[0];
    }

    public function getScheduleToArray()
    {
        foreach ($this->hours as $key => $time) 
        {
            $arraySchedule[] = $time;
        }

        // Helper::dd($arraySchedule);

        return $arraySchedule;
    }

    public function setSchedule($hours)
    {
        $this->hours = $hours;
    }
  
}