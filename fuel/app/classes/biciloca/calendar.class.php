<?php 
namespace Biciloca;

define('HOUR_MILISECONDS', 3600000);
define('LIMIT_OPEN_NEW_PUBLIC_BICYCLE', 12);

class Calendar 
{
    private $events = [];
    private $schedule;

    function __construct($events, Schedule $schedule)
    {
        $this->events = $events;
        $this->schedule = $schedule;
    }

    public function getEvents($day)
    {
        $events = [];

        foreach ($this->events as $key => $event) 
        {
            if ( $event->getStartDay() == $day) 
            {
                $events[] = $event;
            } 
        }

        // helper::dd($events);

        return $events;
    }

    public function getEvent($day, $startTime, $title)
    {
        $events = [];

        foreach ($this->events as $key => $event) 
        {
            if ( $event->getStartDay() == $day && $event->getStartTime() == $startTime &&  $event->getTitle() == $title) 
            {
                $events[] = $event;
            } 
        }

        // helper::dd($events);

        return $events;
    }

    private function orderByStartTime($events)
    {
        do {
            $repeat = false;
            for ($i = 0; $i < count($events); $i++) 
            { 
                if ( isset($events[$i + 1]) and $events[$i]->getStartTime() > $events[$i + 1]->getStartTime() ) 
                {
                    
                    $aux = $events[$i];
                    $events[$i] = $events[$i + 1];
                    $events[$i + 1] = $aux;
                    $repeat = true;
                    // helper::dd($events);

                }
            }

        } while ($repeat);

        // helper::dd($events);
        return $events;
    }

    

    private function getPublicEvents($day)
    {
        $events = [];

        foreach ($this->events as $key => $event) 
        {
            if ( $event->getStartDay() == $day and trim($event->getType()) == '[share]') 
            {
                $events[] = $event;
            } 
        }

        return $events;
    }

    public function getBicycle($day, $intervalTime)
    {
        $events = $this->getEvents($day);
        $existBicycle = false;
        $persons = 0;

        foreach ($events as $key => $event) 
        {
            $intervalEvent = $event->getIntervalRemove30min();

            if ($this->isSameIntervals($intervalEvent, $intervalTime)) 
            {
                if (trim($event->getType()) == '[share]') 
                {
                    $existBicycle = true;
                    $persons = $persons + $event->getNumberPeople();
                }
                else
                {
                    return ['type' => '', 'persons' => $event->getNumberPeople()];
                }
            }
        }

        if ($existBicycle) 
        {
            return ['type' => 'share', 'persons' => $persons];
        }

        return [];
    }

    public function getBicycles($events)
    {
        $bicycles = [];
        $events = $this->orderByStartTime($events);
        $bicycle = ['type' => '', 'persons' => '', 'interval' => ''];

        foreach ($events as $key => $event) 
        {

            if ($key == 0) 
            {
                $bicycle['type'] = trim($event->getType());
                $bicycle['persons'] = $event->getNumberPeople();
                $bicycle['interval'] = $event->getIntervalRemove30min();
                $bicycles[] = $bicycle;   

                
                
            }
            else
            {
                if ($events[$key - 1]->getStartTime() != $events[$key]->getStartTime()) 
                {
                    $bicycle['type'] = trim($event->getType());
                    $bicycle['persons'] = $event->getNumberPeople();
                    $bicycle['interval'] = $event->getIntervalRemove30min();
                    $bicycles[] = $bicycle;      

                }
                else
                {
                    // helper::dd($bicycles);
                    $index = count($bicycles) - 1;
                    $eventPeople =  $event->getNumberPeople();

                    if ($eventPeople != 18)
                    {
                        $bicycles[$index]['persons'] = $bicycles[$index]['persons'] + $eventPeople;
                    }
                    

                    
                }
            }
        }

        return $bicycles;
    }

    private function getEventsSince($startDay)
    {
        $events = [];

        foreach ($this->events as $key => $event) 
        {
            if ( strtotime($event->getStartDay() ) >= strtotime($startDay) ) 
            {
                $events[] = $event;
            } 
        }

        return $events;
    }

    public function getNumberBicycles($day)
    {
        $events = $this->getEvents($day);
        $cont = 0;

        foreach ($events as $key => $event) 
        {
            if ($key == 0) 
            {
                $cont++;
            }
            else
            {
                if ($events[$key - 1]->getStartTime() != $events[$key]->getStartTime()) 
                {
                    $cont++;
                }
            }
        }
        
        return $cont;
    }

    public function existEventPublic($day, $hourIni)
    {
        $events = $this->getEvents($day);

        foreach ($events as $key => $event) 
        {
            if (trim($event->getType()) == '[share]' and $event->getStartTimeRemove30min() == $hourIni)  
            {
                return true;
            }
        }

        return false;
    }

    public function getHoursDisponible($day, $type)
    {
        $hours = $this->schedule->getSimpleHours();
        $events = $this->getEvents($day);
        $personsToOpenPublicEvent = $this->getPersonsToOpenPublicEvent();
        // Helper::dd($events);

        if ($type == 'privado') 
        {
            return $this->getHoursAvailabrePrivate($day);
        }

        // Helper::dd($type);

        if ($type == 'share') 
        {
            
            $numberPeople = 0;

            if ( ! $this->havePublicEvent($day) ) 
            {
                // Helper::dd('trace');
                return $this->getHoursAvailabrePrivate($day);
            }
            else
            {
                $events = $this->getEvents($day);
                // Helper::dd($events);
                foreach ($events as $key => $event) 
                {
                    if (trim($event->getType()) == '[share]') 
                    {
                        $publicEvents[] = $event;
                        $numberPeople = $numberPeople + $event->getNumberPeople();
                        // Helper::dd($event->getNumberPeople());
                    }
                }

                // Helper::dd($numberPeople);

                $isMultiple = $numberPeople % MAX_PEOPLE;

                // Helper::dd($isMultiple);

                if ( $isMultiple == 0 ) 
                {
                    return $this->getHoursAvailabrePrivate($day);
                }

                $publicHoursDisponible = $this->getHoursDisponibleWithPeopleOf($publicEvents);

                foreach ($publicHoursDisponible as $key => $publicHourDisponible) 
                {
                    if (count($publicHoursDisponible) == 1) 
                    {
                        if ($publicHourDisponible['persons'] >= $personsToOpenPublicEvent) 
                        {
                            $privateHoursDisponible = $this->getHoursAvailabrePrivate($day);
                            $privateHoursDisponible[] = $publicHourDisponible['time'];
                            $hoursDisponible = $privateHoursDisponible;
                        }
                        else
                        {
                            // Helper::dd($publicHourDisponible);
                            $hoursDisponible[] = $publicHourDisponible['time'];
                        }
                    }
                    else
                    {
                        $hoursDisponible[] = $publicHourDisponible['time'];
                    }
                }

                // Helper::dd($hoursDisponible);
                return $hoursDisponible;                
            }
        } 
    }

    public function getHoursPrivateDisponibleWithBicycles($bicycles)
    {
        $hours = $this->schedule->getSimpleHours();
        $hoursBusy = [];
        $keyHoursDisponible = 0;

        foreach ($bicycles as $key => $bicycle) 
        {
            foreach ($bicycle['interval'] as $key => $hour) 
            {
                if (isset($bicycle['interval'][$key + 1])) 
                {
                    $hoursBusy[] = $hour;
                }
            }
        }

        $hours = array_diff($hours, $hoursBusy);

        foreach ($hours as $key => $hour) 
        {
            $hoursDisponible[$keyHoursDisponible][0] = $hour;
            $hoursDisponible[$keyHoursDisponible][1] = MAX_PEOPLE;
            $keyHoursDisponible++;
        }

        // helper::dd($hoursDisponible);
        return $hoursDisponible;
    }

    public function getHoursPublicDisponibleWithBicycles($bicycles, $limitOpenNewPublicBicycle)
    {
        $hours = $this->schedule->getSimpleHours();
        $hoursBusy = [];
        $keyHoursDisponible = 0;
        $hoursDisponible = [[]];
        $publicBicycles = $this->getPublicBicycles($bicycles);


        // helper::dd($publicBicycles);

        // if (empty($publicBicycles)) 
        // {
        //     return [];
        // }

        
        // helper::dd($limitOpenNewPublicBicycle['value']);
        
        // Entra si hay bicis publicas con el numero de personas en la bici por debajo del límite para abrir el resto de horas del calendario
        // Añade todos los horarios donde hay bicis publicas con espacios disponibles a horas disponibles para mostrar
        // El resto de horarios se mostrarán no disponibles
        if ($this->AreTherePublicBicycleWithDownLimit($publicBicycles, $limitOpenNewPublicBicycle['value'])) 
        {


            foreach ($publicBicycles as $key => $bicycle) 
            {
                foreach ($bicycle['interval'] as $key => $hour) 
                {
                    if (isset($bicycle['interval'][$key + 1])) 
                    {
                        if ($bicycle['persons'] != MAX_PEOPLE && $bicycle['persons'] < $limitOpenNewPublicBicycle['value']) 
                        {
                            

                            $hoursDisponible[$keyHoursDisponible][0] = $hour;
                            $hoursDisponible[$keyHoursDisponible][1] = MAX_PEOPLE - $bicycle['persons'];
                            $keyHoursDisponible++;
                        }
                    }
                }
            }
            //helper::dd($hoursDisponible);
            return $hoursDisponible;
        }

        // Todos los horarios donde haya alguna bici reservada se van a añadir a un array de horas ocupadas
        // Esto se hace para despues comparar este array con un array con el horario total y saber en que horas no hay ninguna bici reservada
        // Tambien añade todos los horarios donde hay bicis publicas con espacios disponibles a horas disponibles para mostrar    
        foreach ($bicycles as $key => $bicycle) 
        {
            if (trim($bicycle['type'] == '')) 
            {
                // añadimos el horario de las bicis privadas a horario ocupado
                foreach ($bicycle['interval'] as $key => $hour) 
                {
                    if (isset($bicycle['interval'][$key + 1])) 
                    {
                        $hoursBusy[] = $hour;
                    }
                    
                }
            }
            else
            {
                foreach ($bicycle['interval'] as $key => $hour) 
                {
                    if (isset($bicycle['interval'][$key + 1])) 
                    {
                        $hoursBusy[] = $hour;
                        if ($bicycle['persons'] < MAX_PEOPLE) 
                        {
                            $hoursDisponible[$keyHoursDisponible][0] = $hour;
                            $hoursDisponible[$keyHoursDisponible][1] = MAX_PEOPLE - $bicycle['persons'];
                            $keyHoursDisponible++;
                        }
                    }
                }
            }
        }

        // Aqui nos quedamos con las horas que no tienen ninguna bici reservada
        $hours = array_diff($hours, $hoursBusy);

        // helper::dd($bicycles);

        // Aquí añadimos las horas que no tienen ninguna bici reservada a las horas que tienen bici publicas con espacios de personas disponible
        foreach ($hours as $key => $hour) 
        {
            $hoursDisponible[$keyHoursDisponible][0] = $hour;
            $hoursDisponible[$keyHoursDisponible][1] = MAX_PEOPLE;
            $keyHoursDisponible++;
        }

        return $hoursDisponible;
    }

    private function getPublicBicycles($bicycles)
    {
        $publicBicycles = [];

        foreach ($bicycles as $key => $bicycle) 
        {
            if (trim($bicycle['type'] == '[share]') )
            {
                $publicBicycles[] = $bicycle;
            }
        }

        return $publicBicycles;
    }

    private function AreTherePublicBicycleWithDownLimit($publicBicycles, $limit)
    {
        foreach ($publicBicycles as $key => $bicycle) 
        {
            if ($bicycle['persons'] < $limit) 
            {
                return true;
            }
        }

        return false;
    }

    private function getPersonsToOpenPublicEvent()
    {
        $queryPersons =  \Fuel\Core\DB::select()->from('config')->execute();
        $persons = $queryPersons[0]['persons'];
        return $persons;
    }

    private function getHoursAvailabrePrivate($day)
    {
        $hoursBusy = $this->getHoursBusyPrivate($day);
        $availableHours = $this->getAvailableHoursOf($hoursBusy);

        // Helper::dd($availableHours);
        return $availableHours;
    }

    private function getHoursBusyPrivate($day)
    {
        $events = $this->getEvents($day);
        $hoursBusy = [];
        $numberPeople = 0;

        // Helper::dd($events);

        if ( ! empty($events)) 
        {
            $hoursBusyPrivate = $this->getHoursBusyPrivateBooking($events);   
            $hoursBusyPublic = $this->getHoursBusyPublicBooking($events);
            $hoursBusy = array_merge($hoursBusyPrivate, $hoursBusyPublic);
        }

        // Helper::dd($hoursBusy);
        return $hoursBusy;
    }

    private function getHoursBusyPublicBooking($events)
    {
        $hoursBusy = [];

        foreach ($events as $key => $event) 
        {
            if (trim($event->getType()) != '') 
            {
                $eventStartTimeRemove30min = $this->removeTime($event->getStartTime(), 30);
                $eventEndTimeRemove30min = $this->removeTime($event->getEndTime(), 30);

                $eventStartTime30 = $this->addTime($eventStartTimeRemove30min, 30);
                $eventStartTime60 = $this->addTime($eventStartTimeRemove30min, 60);

                $hoursBusy[] = $eventStartTimeRemove30min;
                $hoursBusy[] = $eventStartTime30;
                $hoursBusy[] = $eventStartTime60;

                if ( ! in_array( $eventEndTimeRemove30min, $hoursBusy ) ) 
                {
                    $hoursBusy[] = $eventEndTimeRemove30min;
                }
            }
        }

        return $hoursBusy;
    }

    private function getHoursBusyPrivateBooking($events)
    {
        $hoursBusy = [];

        foreach ($events as $key => $event) 
        {
            if (trim($event->getType()) != '[share]') 
            {
                $eventStartTimeRemove30min = $this->removeTime($event->getStartTime(), 30);
                $eventEndTimeRemove30min = $this->removeTime($event->getEndTime(), 30);

                $eventStartTime30 = $this->addTime($eventStartTimeRemove30min, 30);
                $eventStartTime60 = $this->addTime($eventStartTimeRemove30min, 60);

                $hoursBusy[] = $eventStartTimeRemove30min;
                $hoursBusy[] = $eventStartTime30;
                $hoursBusy[] = $eventStartTime60;

                if ( ! in_array( $eventEndTimeRemove30min, $hoursBusy ) ) 
                {
                    $hoursBusy[] = $eventEndTimeRemove30min;
                }
            }
        }

        return $hoursBusy;
    }

    private function getHoursDisponibleWithPeopleOf($publicEvents)
    {
        foreach ($publicEvents as $key => $publicEvent) 
        {
            $publicEventStartTimeRemove30min = $this->removeTime($publicEvent->getStartTime(), 30);
            $publicEventEndTimeRemove30min = $this->removeTime($publicEvent->getEndTime(), 30);
            $eventStartTime60 = $this->addTime($publicEventStartTimeRemove30min, 60);

            if ($key == 0) 
            {
                $persons = 0;
                $persons = $persons + $publicEvent->getNumberPeople();
            }

            if (isset($publicEvents[$key + 1])) 
            {
                $nextEvent = $publicEvents[$key + 1];

                $nextPublicEventStartTimeRemove30min = $this->removeTime($nextEvent->getStartTime(), 30);

                if ($publicEventStartTimeRemove30min == $nextPublicEventStartTimeRemove30min) 
                {
                    if ($key != 0) 
                    {
                        $persons = $persons + $publicEvent->getNumberPeople();
                    }
                    
                }
                else
                {
                    if ($key != 0) 
                    {
                        $persons = $persons + $publicEvent->getNumberPeople();  
                    }

                    if ($persons < 18) 
                    {
                        $hoursDisponible[$key]['time'][] = $publicEventStartTimeRemove30min;
                        $hoursDisponible[$key]['time'][] = $eventStartTime60;
                        $hoursDisponible[$key]['persons'] = $persons;
                    }

                    $persons = 0;

                }
            }
            elseif(isset($publicEvents[$key - 1]))
            {
                $persons = $persons + $publicEvent->getNumberPeople();

                if ($persons < 18) 
                {
                    $hoursDisponible[$key]['time'][] = $publicEventStartTimeRemove30min;
                    $hoursDisponible[$key]['time'][] = $eventStartTime60;
                    $hoursDisponible[$key]['persons'] = $persons;
                }
            }
            else
            {
                if ($persons < 18) 
                {
                    $hoursDisponible[$key]['time'][] = $publicEventStartTimeRemove30min;
                    $hoursDisponible[$key]['time'][] = $eventStartTime60;
                    $hoursDisponible[$key]['persons'] = $persons;
                }
            }
        }

        return $hoursDisponible;
    }

    private function getAvailableHoursOf($hoursBusy)
    {
        $hours = $this->schedule->getSimpleHours();
        // Helper::dd($hoursBusy);
        $availableHours = [];

        foreach ($hours as $keyHours => $hour) 
        {
            $halfHourMore = $this->addTime($hour, 30);
            $hourMore = $this->addTime($hour, 60);
            $addHour = true;
            $addHourAndHalf = true;

            foreach ($hoursBusy as $key => $eventHourIni) 
            {
                if ($halfHourMore == $eventHourIni) 
                {
                    $addHour = false;
                    $addHourAndHalf = false;
                }

                if ($hourMore == $eventHourIni) 
                {
                    $addHourAndHalf = false;
                }
            }

            if ($addHour) 
            {
                $availableHours[$keyHours][] = $hour;
                $availableHours[$keyHours][] = $this->addTime($hour, 60);
            }

            if ($addHourAndHalf) 
            {
                $availableHours[$keyHours][] = $this->addTime($hour, 90);
            }  
        }

        // Helper::dd($availableHours);
        return $availableHours;
    }

    private function havePublicEvent($day)
    {
        $events = $this->getEvents($day);
        // Helper::dd($day);

        foreach ($events as $key => $event) 
        {
            if(trim($event->getType()) == '[share]')
            {
                return true;
            }
        }

        return false;
    }

    public function havePrivateEvent($day)
    {
        $events = $this->getEvents($day);
        // Helper::dd($day);

        foreach ($events as $key => $event) 
        {
            if(trim($event->getType()) == '')
            {
                return true;
            }
        }

        // Helpre::dd($type);
        return false;
    }

    private function getHoursAvailabrePublic($day)
    {
        if ( ! $this->havePublicEvent($day) ) 
        {
            $hoursBusy = $this->getHoursBusyPrivateBooking($events);
        }
        else
        {
            $events = $this->getEvents($day);
            foreach ($events as $key => $event) 
            {
                if (trim($event->getType) == '[share]') 
                {
                    $publicEvents[] = $event;
                    $numberPeople = $numberPeople + $event->getNumberPeople();
                }
            }

            $isMultiple = $numberPeople % MAX_PEOPLE;

            if ( $isMultiple == 0 ) 
            {
                $hoursBusy = $this->getHoursBusyPrivateBooking($events);
            }

            $numberPeople = 0;

            foreach ($publicEvents as $key => $publicEvent) 
            {
                $startTime = $publicEvent->getStartTime();
                if ( isset($publicEvents[$key + 1]) and $startTime == $publicEvents[$key + 1]->getStartTime() ) 
                {
                    $numberPeople = $numberPeople + $publicEvent->getNumberPeople();
                }
                else
                {
                    $numberPeople = $numberPeople + $publicEvent->getNumberPeople();
                    if ($numberPeople < MAX_PEOPLE) 
                    {
                        $eventStartTime30 = $this->addTime($publicEvent->getStartTime(), 30);
                        $eventStartTime60 = $this->addTime($publicEvent->getStartTime(), 60);

                        $hoursDisponible[] = $publicEvent->getStartTime();
                        $hoursDisponible[] = $eventStartTime30;
                        $hoursDisponible[] = $eventStartTime60;

                        if ( ! in_array( $publicEvent->getEndTime(), $hoursDisponible ) ) 
                        {
                            $hoursDisponible[] = $publicEvent->getEndTime();
                        }

                        $hours = $this->schedule->getSimpleHours();

                        $hour = $hour[0];

                        foreach ($hoursDisponible as $key => $hourDisponible) 
                        {
                            if ($hour < $hourDisponibleStart ) 
                            {
                                $hoursBusy[] = $hour;
                                $hour = $this->addTime($publicEvent->getStartTime(), 30);
                            }
                        }

                        foreach ($hours as $key => $hour) 
                        {
                            $hour60 = $this->addTime($hour, 60);

                            foreach ($hoursDisponible as $key => $hourDisponible) 
                            {
                                if ($hour != $hourDisponible) 
                                {
                                    $hoursBusy[] = $hour;
                                    break;
                                }
                            }
                        }
                        
                    }
                }
            }

        }

        foreach ($events as $key => $event) 
        {
            $eventStartTime30 = $this->addTime($event->getStartTime(), 30);
            $eventStartTime60 = $this->addTime($event->getStartTime(), 60);

            if (trim($event->getType()) == '') 
            {
                $hoursBusy[] = $event->getStartTime();
                $hoursBusy[] = $eventStartTime30;
                $hoursBusy[] = $eventStartTime60;

                if ( ! in_array( $event->getEndTime(), $hoursBusy ) ) 
                {
                    $hoursBusy[] = $event->getEndTime();
                }
            }
            else
            {
                
                if ($event->getNumberPeople() >= MAX_PEOPLE) 
                {
                    $hoursBusy[] = $event->getStartTime();
                    $hoursBusy[] = $eventStartTime30;
                    $hoursBusy[] = $eventStartTime60;

                    if ( ! in_array( $event->getEndTime(), $hoursBusy ) ) 
                    {
                        $hoursBusy[] = $event->getEndTime();
                    }

                }
                else
                {
                    $startTime = $this->addTime($event->getStartTime(), 60);

                    if ($startTime != $event->getEndTime()) 
                    {
                        $startTime = $this->addTime($startTime, 30);
                    }

                    while ( strtotime($startTime) < strtotime('22:00') ) 
                    {
                        $startTime30 = $this->addTime($startTime, 30);
                        $startTime60 = $this->addTime($startTime, 60);

                        $hoursBusy[] = $startTime;
                        $hoursBusy[] = $startTime30;
                        $hoursBusy[] = $startTime60;

                        $startTime = $startTime30;
                    }
                }  

            }
        }
    }

    private function busySchedules($eventsDay)
    {
        $busySchedules = [];
        foreach ($eventsDay as $key => $event) 
        {
            
            $busySchedules = array_merge($busySchedules, $event->getScheduleToArray());
        }

        // Helper::dd($busySchedules);

        return $busySchedules;
    }

    private function addTime( $time, $addMinutes ) 
    { 
       $timeToSeconds = strtotime($time);
       $addMinutesToSeconds = $addMinutes * 60;
       $newTime = date("H:i", $timeToSeconds + $addMinutesToSeconds);
       return $newTime;
    }

    private function removeTime($time, $removeMinutes)
    {
        $timeToSeconds = strtotime($time);
        $removeMinutesToSeconds = $removeMinutes * 60;
        $newTime = date("H:i", $timeToSeconds - $removeMinutesToSeconds);
        return $newTime;
    }

    public function isEventPublicDisponible($day, $intervalTime, $people, $limitOpenNewPublicBicycle)
    {
        if ( $this->isEmptyIntervalTime($day, $intervalTime) and $this->AllMorePublicBicyclesWhitMore($limitOpenNewPublicBicycle['value'], $day)) 
        {
            return true;
        }

        if ($this->haveBicycleIntervalTime($day, $intervalTime)) 
        {
            $bicycle = $this->getBicycle($day, $intervalTime);
           
            if ($bicycle['type'] == 'share') 
            {
                $maxPeople = MAX_PEOPLE - $bicycle['persons'];

                if ($people <= $maxPeople) 
                {
                    return true;
                }
            }
        }

        return false;
    }

    private function AllMorePublicBicyclesWhitMore($persons, $day)
    {
        $events = $this->getPublicEvents($day);
        $bicycles = $this->getBicycles($events);

        foreach ($bicycles as $key => $bicycle) 
        {
            if ($bicycle['persons'] < $persons) 
            {
                return false;
            }
        }

        return true;
    }

    private function haveBicycleIntervalTime($day, $intervalTime)
    {
        $bicycle = $this->getBicycle($day, $intervalTime);
        // Helper::dd($bicycle);

        if (empty($bicycle)) 
        {
            return false;
        }

        return true;
    }

    public function isEventPrivateDisponible($day, $intervalTime)
    {
        return $this->isEmptyIntervalTime($day, $intervalTime);
    }

    public function isEmptyIntervalTime($day, $intervalTime)
    {
        $events = $this->getEvents($day);

        // Helper::dd($events);

        foreach ($events as $key => $event) 
        {
            $intervalEvent = $event->getIntervalRemove30min();
            // Helper::dd(array_merge($intervalEvent, $intervalTime));
            if ($this->isCrossIntervals($intervalEvent, $intervalTime)) 
            {
                return false;  
            }
        }

        return true;
    }

    private function isCrossIntervals_old($intervalEvent, $intervalTime)
    {
        // Helper::dd($intervalTime);
        $sizeIntervalEvent = count($intervalEvent);
        $sizeIntervalTime = count($intervalTime);

        foreach ($intervalEvent as $keyEvent => $hourEvent) 
        {
            foreach ($intervalTime as $keyTime => $hourTime) 
            {
                if (
                    ($keyEvent != ($sizeIntervalEvent - 1)) and ($keyTime != 0)
                    and
                    ($keyEvent != 0) and ($keyTime != ($sizeIntervalTime - 1))
                   ) 
                {
                    if ($hourEvent == $hourTime) 
                    {
                        return true;
                    }
                }
                
            }
        }

        return false;
    }

    private function isCrossIntervals($intervalEvent, $intervalTime)
    {
        foreach ($intervalEvent as $keyEvent => $hourEvent) 
        {
            foreach ($intervalTime as $keyTime => $hourTime) 
            {
                if ($keyEvent != (count($intervalEvent) - 1) and $keyTime != (count($intervalTime) - 1)) 
                {
                    if ($hourEvent == $hourTime) 
                    {
                        return true;
                    }
                }
                
            }
        }

        return false;
    }

    private function isSameIntervals($intervalEvent, $intervalTime)
    {
        foreach ($intervalEvent as $key => $hourEvent) 
        {
            foreach ($intervalTime as $key => $hourTime) 
            {
                if ( ! in_array($hourEvent, $intervalTime) ) 
                {
                    return false;
                }
            }
        }

        return true;
    }

    public function maxNumberPeopleDisponible($day, $startTime)
    {
        $events = $this->getEvents($day);
        $numberPeopleDisponible = MAX_PEOPLE;
        // Helper::dd($events);

        foreach ($events as $key => $event) 
        {
            $eventStartTimeRemove30min = $this->removeTime($event->getStartTime(), 30);
            if ($eventStartTimeRemove30min == $startTime) 
            {
                $numberPeopleDisponible = $numberPeopleDisponible - $event->getNumberPeople();
            }
        }

        // Helper::dd($numberPeopleDisponible);

        return $numberPeopleDisponible;
    }

    public function isDisponible($day)
    {
        $eventsDay = $this->getEvents($day);
        $hoursDisponible = $this->schedule->getSimpleHours();
        $busyHours = [];

        foreach ($eventsDay as $key => $event) 
        {
            if ($this->isEventPublic($event)) 
            {
                if ( $this->isDisponibleEventPublic($event) ) 
                {
                    return true;
                }
            }

            $busyHours = $this->addHourIntervalToBusyHours($event, $busyHours);
        }

        if ( $this->isBusyHoursFull($hoursDisponible, $busyHours) ) 
        {
            return false;
        }

        return true;
    }

    private function isEventPublic($event)
    {
        if ($event->getType() == '[share]') 
        {
            return true;
        }

        return false;
    }

    private function isDisponibleEventPublic($eventPublic)
    {
        if ( $this->personsOfEventPublic($event) < 18 ) 
        {
            return true;
        }
    }

    public function personsOfEventPublic($day, $startTime)
    {
        $eventsDay = $this->getEvents($day);
        $persons = 0;

        foreach ($eventsDay as $key => $event) 
        {
            if ($event->getStartTimeRemove30min() == $startTime) 
            {
                $persons = $persons + $event->getNumberPeople();
            }
        }

        return MAX_PEOPLE - $persons;
    }

    private function addHourIntervalToBusyHours($event, $busyHours)
    {
        $interval = $event->getInterval();
        foreach ($interval as $key => $hour) 
        {
            if ( ! in_array($hour, $busyHours) ) 
            {
                $busyHours[] = $hour;
            }
            
        }

        return $busyHours;
    }

    private function isBusyHoursFull($hoursDisponible, $busyHours)
    {
        if ( count($hoursDisponible) == count($busyHours) ) 
        {
            return true;
        }

        return false;
    }

    // dias no disponibles desde un día dado
    public function daysNotDisponible($startDay)
    {
        $daysNotDisponible = [];
        $previewEventStartDay = date('yyyy-mm-dd', strtotime($startDay) - 86400000);
        $events = $this->getEventsSince($startDay);

        foreach($events as $key => $event) 
        {
            // Helper::dd($this->isDisponible( $event->getStartDay() ) );
            if ( $previewEventStartDay != $event->getStartDay() and ! $this->isDisponible( $event->getStartDay() ) ) 
            {
                $daysNotDisponible[$key]['start'] = $event->getStartDay();
                $daysNotDisponible[$key]['end'] = $event->getEndDay();
                $daysNotDisponible[$key]['rendering'] = 'background';
                $daysNotDisponible[$key]['backgroundColor'] = 'red';
            }

            $previewEventStartDay = $event->getStartDay();
        }

        // $daysNotDisponible[0]['start'] = '2017-03-27';
        // $daysNotDisponible[0]['end'] = '2017-03-27';
        // $daysNotDisponible[0]['rendering'] = 'background';
        // $daysNotDisponible[0]['backgroundColor'] = 'red';

        return $daysNotDisponible;
    }
 
}