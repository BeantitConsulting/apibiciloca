<?php 
namespace Biciloca;

class Coupon
{
    private $alias = null;
    public $name = null;
    private $minutos = null;
    private $priceWithDiscount = null;
    private $payOffline = null;

    public function getCouponInfoArray() {
        $coup['alias'] = $this->alias;
        $coup['name'] = $this->name;
        $coup['minutos'] = $this->minutos;
        $coup['priceWithDiscount'] = $this->priceWithDiscount;
        $coup['payOffline'] = $this->payOffline;
        return $coup;
    }

    function __construct($coupon, $price)
    {

        if ($this->exist($coupon)) 
        {
            $coupon = $this->get($coupon);
            $this->alias = $coupon->alias;
            $this->name = $coupon->name;
            $this->priceWithDiscount = $this->calculatePriceWithDiscount($coupon, $price);
            $this->payOffline = $this->calculatePayOffline($coupon, $price);
        }
        else
        {
            $this->priceWithDiscount = $price;
            $this->payOffline = 0;
        }  
    }

    private function exist($coupon)
    {
        $coupon = \Model_Coupons::find('all', array(
                    'where' => array(
                        array('coupon', $coupon),
                    )
        ));

        if (empty($coupon)) 
        {
            return false;
        }

        return true;
    }

    private function get($coupon)
    {
        $coupon = \Model_Coupons::find('all', array(
                    'where' => array(
                        array('coupon', $coupon),
                    )
        ));

        foreach ($coupon as $key => $value) 
        {
            $coupon = $value;
        }

        return $coupon;
    }

    private function calculatePriceWithDiscount($coupon, $price)
    {
        if ($coupon->discount_percentaje == 0) 
        {
            if (is_numeric($coupon->discount_value)) 
            {
                $price = $price - $coupon->discount_value;
            }
        }

        if ($coupon->discount_value == 0) 
        {
            if (is_numeric($coupon->discount_percentaje)) 
            {
                $price = $price - $coupon->discount_percentaje / 100 * $price;
            }
        }

        if ($coupon->name == "cupontest") {
            $price = 0.25;
        }

        return $price;
    }

    private function calculatePayOffline($coupon, $price)
    {
        if ($coupon->name == "cupontest") {
            return 0.25;
        }
        
        if ($price == 324) 
        {
            return $coupon->price60min;
        }
        elseif ($price == 414) 
        {
            return $coupon->price90min;
        }
        else
        {
            return 0;
        }
    }

    public function getPriceWithDiscount()
    {
        return $this->priceWithDiscount;
    }

    public function getAlias()
    {
        return $this->alias;
    }

    public function getPayOffline()
    {
        return $this->payOffline;
    }

    public function getName()
    {
        return $this->name;
    }
}