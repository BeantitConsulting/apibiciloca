<?php

class Model_Coupons extends Orm\Model
{
    protected static $_table_name = 'coupons';
    protected static $_primary_key = array('id');
    protected static $_properties = array(
        'id', // both validation & typing observers will ignore the PK
        'coupon' => array(
            'data_type' => 'varchar',
            
        ),
        'client' => array(
            'data_type' => 'varchar',
            
        ),
        'alias' => array(
            'data_type' => 'varchar',
           
        ),
        'name' => array(
            'data_type' => 'varchar',
           
        ),
        'discount_percentaje' => array(
            'data_type' => 'varchar',
           
        ),
        'discount_value' => array(
            'data_type' => 'varchar',
           
        ),
        'price60min' => array(
            'data_type' => 'float',
           
        ),
        'price90min' => array(
            'data_type' => 'float',
           
        ),
    );
}