<?php
class Model_Bicycles extends Orm\Model
{
    protected static $_table_name = 'bicycles';
    protected static $_primary_key = array('id');
    protected static $_properties = array(
        'id', // both validation & typing observers will ignore the PK
        'day' => array(
            'data_type' => 'varchar',
        ),
        'numberBicycles' => array(
            'data_type' => 'int',
        ),
    );
}