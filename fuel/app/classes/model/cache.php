<?php

class Model_Cache extends Orm\Model
{
    protected static $_table_name = 'cache';
    protected static $_primary_key = array('id');
    protected static $_properties = array(
        'id', // both validation & typing observers will ignore the PK
        'idRedsys',
        'data',
        'start',
        'end',
        'idCalendar',
        'nombre',
        'apellidos',
        'tipo',
        'personas',
        'fecha',
        'horainicio',
        'horafin',
        'duracion',
        'telefono',
        'email',
        'urlSuccess',
        'urlSuccessPosible',
        'urlError',
        'precio',
        'created_at',
        'updated_at',
    );
}

