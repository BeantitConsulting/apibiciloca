<?php

class Model_Redsys extends Orm\Model
{
    protected static $_table_name = 'redsys';
    protected static $_primary_key = array('id');
    protected static $_properties = array(
        'id', // both validation & typing observers will ignore the PK
        'idRedsys',
        'name' => array(
            'data_type' => 'varchar',
            
        ),
        'surname' => array(
            'data_type' => 'varchar',
            
        ),
        'type' => array(
            'data_type' => 'varchar',
            
        ),
        'persons' => array(
            'data_type' => 'varchar',
           
        ),
        'date' => array(
            'data_type' => 'varchar',
           
        ),
        'start_hour' => array(
            'data_type' => 'varchar',
           
        ),
        'end_hour' => array(
            'data_type' => 'varchar',
           
        ),
        'phone' => array(
            'data_type' => 'varchar',
           
        ),
        'email' => array(
            'data_type' => 'varchar',
           
        ),
        'order' => array(
            'data_type' => 'varchar',
           
        ),
        'created_at' => array(
            'data_type' => 'varchar',
           
        ),
        'updated_at' => array(
            'data_type' => 'varchar',
           
        ),
        'precio' => array(
            'data_type' => 'float',
           
        ),
    );
}

