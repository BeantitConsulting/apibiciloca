<?php
class Model_Config extends Orm\Model
{
    protected static $_table_name = 'config';
    protected static $_primary_key = array('id');
    protected static $_properties = array(
        'id', // both validation & typing observers will ignore the PK
        'description' => array(
            'data_type' => 'int',
        ),
        'value' => array(
            'data_type' => 'int',
        )
    );
}