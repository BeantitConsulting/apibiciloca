<?php
define('ACCESS_CONTROL_ALLOW_ORIGIN', '*');
use \Firebase\JWT\JWT;
use PHPMailer\PHPMailer\PHPMailer;

include_once APPPATH . "classes/helper.static.class.php";
use \Biciloca\Helper;

class Controller_Base extends Controller_Rest
{
    protected $key = 'Akdor7465jdjdkrj-lfkf/kdjielskdfm7465.,slk99578.d';
    protected $algorithm = array('HS256');

    protected function intentoReserva($input)
    {
                        return "<!doctype html>
                                    <html lang='es'>
                                        <head>

                                        </head>
                                        <body>
                                            <p><b>Intento de reserva<b></p>
                                            <p><b>Mensaje: </b>" . $input['mensaje'] . "</p>

                                            <h3>Información de contacto</h3>
                                            <p><b>Tipo: </b> " . $input['tipo'] . "</p>
                                            <p><b>Nombre: </b> " . $input['nombre'] . " " . $input['apellidos'] . "</p>
                                            <p><b>Email: </b> " . $input['email'] . "</p>
                                            <p><b>Teléfono: </b> " . $input['telefono'] . "</p>
                                            <p><b>Personas: </b> " . $input['personas'] . "</p>

                                            <h3>Detalles</h3>
                                            <p><b>Fecha: </b> " . $input['fecha'] . "</p>
                                            <p><b>Inicio: </b>  " . $input['horainicio'] . "</p>
                                            <p><b>Fin: </b>  " . $input['horafin'] . "</p>
                                            <p><b>Pago: </b>  " . $input['pago'] . "€ </p>
                                        </body>
                                </html>";
    }

    protected function reservaRealizada($input)
    {
        return "<!doctype html>
                        <html lang='en'>
                            <head>

                            </head>
                            <body>
                                <img src='http://biciloca.com/img/emailLogo.png'>
                                <p>We confirm that your reservation has been successfully completed. These are the data of it:</p>

                                <h3>Contact information</h3>
                                <p><b>Name: </b> " . $input['nombre'] . " " . $input['apellidos'] . "</p> 
                                <p><b>Email: </b> " . $input['email'] . "</p>
                                <p><b>Phone: </b> " . $input['telefono'] . "</p>
                                <p><b>People: </b> " . $input['personas'] . "</p>

                                <h3>Details</h3>
                                <p><b>Date: </b> " . $input['fecha'] . "</p>
                                <p><b>Start: </b> " . $input['horainicio'] . "</p>
                                <p><b>End: </b> " . $input['horafin'] . "</p>
                                <p><b>Price </b> " . $input['precio'] . " € </p>
                                <p><b>Meeting point: </b> St. Rambla Santa Monica 7, Barcelona. Metro stop Drasanes</p>
                                <img src='http://biciloca.com/img/punto-encuentro.jpg'>
                                <p>Booking system</p>
                                <p>(+34) 634556224</p>
                            </body>
                        </html>";
    }

    protected function reservaRealizadaSafety($input)
    {
        return "<!doctype html>
                    <html lang='es'>
                        <head>

                        </head>
                        <body>
                            <p><b>Intento de reserva<b></p>
                            <p><b>Mensaje: </b>  " . $input['mensaje'] . " </p>

                            <h3>Información de pago</h3>
                            <p><b>Comentarios: </b>  " . $input['comentarios'] . "€ </p>
                            <p><b>Personas: </b> " . $input['personas'] . "</p>
                            <p><b>Pago: </b>  " . $input['pago'] . "€ </p>
                        </body>
                </html>";
    }
    
    protected function error($code = 500, $mensaje = 'Internal Server Error', $descripcion = 'Unexpected Error')
    {
        return [
                    'code' => $code, 
                    'mensaje' => $mensaje,
                    'descripcion' => $descripcion,
                ];
    }

    protected function exito($code = 200, $mensaje = 'Success', $datos = null)
    {
        return [
                    'code' => $code, 
                    'mensaje' => $mensaje,
                    'datos' => $datos
                ];
    }

    protected function getCredentials()
    {
        return APPPATH . '/credentials/service-account-credentials.json';
    }

    protected function getAliasClient($coupon)
    {
        $coupon = Model_Coupons::find('all', array(
                    'where' => array(
                        array('coupon', $coupon),
                    )
        ));

        foreach ($coupon as $key => $value) 
        {
            $coupon = $value;
        }

        // Helper::dd($coupon->discount_percentaje);

        if (empty($coupon)) 
        {
            
            return '';
        }

        return $coupon->alias;
    }

    protected function sendEmail( 
                                $htmlBody = array(),
                                $to = array('email' => 'juliopc86@gmail.com', 'name' => 'Julio'),
                                $subject = 'test email subject',
                                $body = 'test email body',
                                $from = array('email' => 'reserve@online-bookingsystem.com', 'name' => 'BookingSystem')
                               )
    {
        
        $email = Email::forge();
        $email->from($from['email'], $from['name']);
        $email->to($to['email'], $to['name']);
        $email->subject($subject);

        if ( ! empty($htmlBody) ) 
        {
            $email->html_body( \View::forge( $htmlBody['template'], $htmlBody['data'] ) );
            $body = null;
        }

        if ($body != null) 
        {
            $email->body($body);
        }
        
        try
        {
            $email->send();
        }
        catch(\EmailValidationFailedException $e)
        {
            return [false, $e->getMessage()];
        }
        catch(\EmailSendingFailedException $e)
        {
            return [false, $e->getMessage()];
        }  

        return [true];
    }

    public function post_emailContacto()
    {
        $input = $_POST;
        
        // TODO METER CIUDAD
        $dataEmail = [
                            'nombre' => $input['nombre'],
                            'mensaje' => $input['mensaje'],
                            'personas' => $input['personas'],
                            'telefono' => $input['telefono'],
                            'email' => $input['email']
            ];

        if (array_key_exists('ciudad', $input)) {
            $dataEmail['ciudad'] = $input['ciudad'];
        }

        // Helper::dd($dataEmail);

            // helper::dd($input);

        $responseEmail = $this->sendEmail(
                  ['template' => 'emailContactoFormulario', 'data' => $dataEmail], 
                  ['email' => 'diversion@biciloca.com', 'name' => 'Biciloca'],
                  'Contacto a través de formulario'
                );


        if ($input['lang'] == 'es') 
        {
           Response::redirect('http://biciloca.com/biciloca/emailEnviadoCorrectamente.html', 'refresh');
        }
        
        if($input['lang'] == 'en')
        {
            Response::redirect('http://www.beerbike.es/beerbike/emailEnviadoCorrectamente.html', 'refresh');
        }    
        
    }

    public function post_emailContactoBeerBikeBarcelona()
    {
        $input = $_POST;

        $dataEmail = [
                            'nombre' => $input['nombre'],
                            'mensaje' => $input['mensaje'],
                            'telefono' => $input['telefono'],
                            'email' => $input['email']
            ];

        // Helper::dd($dataEmail);

        $responseEmail = $this->sendEmail(
                  ['template' => 'emailContactoBeerBikeBarcelona', 'data' => $dataEmail], 
                  ['email' => 'diversion@biciloca.com', 'name' => 'BeerBikeBarcelona'],
                  'Contacto a través de formulario'
                );


        Response::redirect('http://www.beerbikebarcelona.com/contactook.html', 'refresh');
    }

    public function get_bookings()
    {
        $bookings = Model_Bookings::find('all', array(
            'order_by' => array('id' => 'desc'),
        ));
        foreach ($bookings as $key => $booking) 
        {
            echo '<p>';
            echo '<b>nombre: </b>' . $booking->name . ' - ' . $booking->surname . '<b> tipo: </b>' . $booking->type . '<b> personas: </b>' . $booking->persons . '<b> fecha de reserva: </b>' . $booking->date . '<b> hora de inicio: </b>' . $booking->start_hour . '<b> hora de fin: </b>' . $booking->end_hour . '<b> telefono: </b>' . $booking->phone . '<b> email: </b>' . $booking->email . '<b> precio: </b>' . $booking->price  . '<b> Reserva creada el: </b>' . $booking->created_at; 
            echo '</pr>';
            echo '<hr>';
        }
        
    }

    protected function formatLog($message)
    {
        Log::warning('--------------------------');
        Log::warning($message);
        Log::warning('--------------------------');
    }

    protected function formatArrayToLog($array)
    {
        $log = '';
        foreach ($array as $key => $value) {
            $log = $log . $value . ' - ';
        }
        return $log;
    }


    protected function sendMail($to, $message, $template)
    {
        $mail = new PHPMailer;
        
        $mail->isSMTP();
        
        $mail->CharSet = 'UTF-8';
        
        $mail->SMTPDebug = 0;
        
        $mail->Host = 'smtps.aruba.it';
        
        $mail->Port = 465;

        $mail->SMTPSecure = true;
        
        $mail->SMTPAuth = true;
        
        $mail->Username = 'reserve@online-bookingsystem.com';
        
        $mail->Password = 'Avancar150';
        
        $mail->setFrom('reserve@online-bookingsystem.com', 'BookingSystem');
        
        $mail->addAddress($to['email'], $to['name']);
        
        $mail->Subject = $message['data']['mensaje'];
        
        $mail->msgHTML($this->$template($message['data']));
        
        if (!$mail->send()) {
            $this->formatLog('Fallo al enviar el email: ' . $mail->ErrorInfo);
        }
    }

    public function get_phpmailer()
    {
        $dataEmail = [
                                'mensaje' => 'Reserve successfully',
                                'nombre' => 'nombre',
                                'apellidos' => 'apellidos',
                                'personas' => 'personas',
                                'fecha' => 'fecha',
                                'horainicio' => 'horainicio',
                                'horafin' => 'horafin',
                                'telefono' => 'telefono',
                                'tipo' => 'tipo',
                                'pago' => 'pago',
                                'email' => 'email'
            ];
        $this->sendMail(['email' => 'yalach@gmail.com', 'name' => 'Julio'], ['data' => $dataEmail], 'reservaRealizada');
    }
}