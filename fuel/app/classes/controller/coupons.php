<?php

include_once APPPATH . "classes/helper.static.class.php";
include_once APPPATH . "classes/biciloca/coupon.class.php";
use \Biciloca\Helper;
use \Biciloca\Coupon;

class Controller_Coupons extends Controller_Base
{
    public function get_coupon($coupon, $price)
    {
        $headers = array (
            'Cache-Control'     => 'no-cache, no-store, max-age=0, must-revalidate',
            'Pragma'            => 'no-cache',
            'Access-Control-Allow-Origin'  => ACCESS_CONTROL_ALLOW_ORIGIN

        );

        $coupon = new Coupon($coupon, $price);
        $body = $coupon->getPriceWithDiscount();
        $response = new Response($body, 200, $headers);
        return $response;
    }

    public function post_coupon($coupon)
    {

    }

    public function post_delete($coupon)
    {

    }
}
