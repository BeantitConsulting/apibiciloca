<?php

require APPPATH . '../vendor/autoload.php';
include_once APPPATH . "classes/biciloca/client.class.php";
include_once APPPATH . "classes/biciloca/event.class.php";
include_once APPPATH . "classes/biciloca/calendar.class.php";
include_once APPPATH . "classes/biciloca/schedule.class.php";
include_once APPPATH . "classes/biciloca/coupon.class.php";
include_once APPPATH . "classes/helper.static.class.php";
include_once APPPATH . "classes/biciloca/apiRedsys.php";
include_once APPPATH . "classes/biciloca/config.php";
use \Firebase\JWT\JWT;
use \Stripe\Stripe;
use \Biciloca\Event;
use \Biciloca\Client;
use \Biciloca\Calendar;
use \Biciloca\Schedule;
use \Biciloca\Coupon;
use \Biciloca\Helper;
use \Biciloca\RedsysAPI;

define('NUMBER_BICYCLES_BOOKING_TO_OPEN_NEW_CALENDAR', 30);

class Controller_Events extends Controller_Base
{
    protected $key = 'Akdor7465jdjdkrj-lfkf/kdjielskdfm7465.,slk99578.d';
    protected $algorithm = array('HS256');
    private $calendars = [
                            '0msts28cgqnhfk9fd9o1edbup8@group.calendar.google.com',
                            '8pfkp4jvavll8jg5qrro3v8ml4@group.calendar.google.com',
                            '7tp3hfalkqen0gtvttggu0rk4g@group.calendar.google.com'
                         ];

    public function post_createUser()
    {
        
        // create a new user
        $resToCreateUser = Auth::create_user(
            'joaquinBackend',
            'Avancar100!',
            'diversion@biciloca.com',
            1,
            array(
                'fullname' => 'Joaquin',
            )
        );
        return $resToCreateUser;
    }

    private function getServiceCalendar()
    {
        $client = new Google_Client();

        $credentials_file = $this->getCredentials();
        $client->setAuthConfig($credentials_file);
        $client->setScopes(['https://www.googleapis.com/auth/calendar']);

        $service = new Google_Service_Calendar($client);

        return $service;
    }

    private function getCalendars()
    {
        foreach ($this->calendars as $key => $idCalendar) 
        {
            $calendars[] = $this->getCalendar($idCalendar);
        }

        return $calendars;
    }

    private function getCalendar($id = '0msts28cgqnhfk9fd9o1edbup8@group.calendar.google.com')
    {
        $service = $this->getServiceCalendar();
        $events = $service->events->listEvents($id, ['maxResults' => 2500]);
        $eventos = [];
        // Helper::dd($events);

        foreach ($events->getItems() as $event) 
        {
            // Helper::dd($event);
            $client = new Client($event);
            

            $evento = new Event($client, new Schedule(), $event);
            $eventos[] = $evento;
        }

        // Helper::dd($eventos);

        $calendar = new Calendar($eventos, new Schedule());

        return $calendar;
    }

    public function get_events()
    {

        $calendar = $this->getCalendar();

        $pastDays['start'] = '1970-01-01';
        $pastDays['end'] = date('Y-m-d');
        $pastDays['rendering'] = 'background';
        $pastDays['backgroundColor'] = 'red';

        // Helper::dd($pastDays);

        $daysNotDisponible = $calendar->daysNotDisponible(date('Y-m-d'));
        $daysNotDisponible[] = $pastDays;
        $daysNotDisponible = Arr::reindex($daysNotDisponible);

        // Helper::dd($daysNotDisponible);

        $body = $daysNotDisponible;
        $body = json_encode($body);
        // Helper::dd($body);


        $headers = array (
            'Cache-Control'     => 'no-cache, no-store, max-age=0, must-revalidate',
            'Pragma'            => 'no-cache',
            'Access-Control-Allow-Origin'  => ACCESS_CONTROL_ALLOW_ORIGIN
        );

        $response = new Response($body, 200, $headers);

        return $response;
        
    }

    public function get_test($fecha)
    {
        $numberBicycles = Model_Bicycles::find('all', ['where' => ['day' => $fecha]]);

        if (empty($numberBicycles)) 
        {
            $numberBicycles = 3;
        }
        else
        {
            foreach ($numberBicycles as $key => $number) 
            {
                $numberBicycles = $number['numberBicycles'];
            }
        }

        return $numberBicycles;
    }

    private function mergeAllCalendars($hoursDisponible)
    {
        $merge = [];

        foreach ($hoursDisponible as $key => $hours) 
        {
            foreach ($hours as $key => $hour) 
            {
                $merge[] = $hour;
            }
        }

        $hoursDisponible = array_map("unserialize", array_unique(array_map("serialize", $merge)));

        // helper::dd($hoursDisponible);
        return $hoursDisponible;
    }

    public function get_hours($day, $type)
    {
        $calendars = $this->getCalendars();
        $idAllCalendars = $this->calendars;
        $numberBicyclesToOpenCalendar = Model_Config::find(1);
        $numberBicyclesDisponiblesToDay = $this->getNumberBicyclesDisponibles($day);
        $limitOpenNewPublicBicycle = Model_Config::find(2);

        

        for ($i=0; $i < $numberBicyclesDisponiblesToDay; $i++) 
        { 
            $calendar = $this->getCalendar($idAllCalendars[$i]);
            $events = $calendar->getEvents($day);
            $bicycles = $calendar->getBicycles($events);

            if ($type == 'share') 
            {
                $hoursDisponible[] = $calendar->getHoursPublicDisponibleWithBicycles($bicycles, $limitOpenNewPublicBicycle); 
                //$this->formatLog($this->formatArrayToLog($hoursDisponible) . "METODO: " . 'get_hours($day, $type)');
                
                $peopleOnSharedBycicle = MAX_PEOPLE - $hoursDisponible[0][1][1];

                if ($peopleOnSharedBycicle < $limitOpenNewPublicBicycle->value)
                {
                    break;
                }

            }

            if ($type == 'privado') 
            {
                $hoursDisponible[] = $calendar->getHoursPrivateDisponibleWithBicycles($bicycles);
                if ($calendar->getNumberBicycles($day) < $numberBicyclesToOpenCalendar['value']) 
                {
                    break;
                }
            }  
        }

        


        $mergeCalendars = $this->mergeAllCalendars($hoursDisponible);

        // helper::dd($mergeCalendars);
        $headers = array (
            'Cache-Control'     => 'no-cache, no-store, max-age=0, must-revalidate',
            'Pragma'            => 'no-cache',
            'Access-Control-Allow-Origin'  => ACCESS_CONTROL_ALLOW_ORIGIN
        );

        $body = Arr::reindex($mergeCalendars);
        // $body = [["10:00", "18"], ["10:30", "18"]];
        $body = json_encode($body);
        

        $response = new Response($body, 200, $headers);

        return $response;
    }

    public function get_peopleDisponible($day, $hourIni)
    {
        $calendar = $this->getCalendar();

        if ( ! $calendar->existEventPublic($day, $hourIni)) 
        {
            $body = MAX_PEOPLE;
        }
        else
        {
            $body = $calendar->personsOfEventPublic($day, $hourIni);
        }

        $headers = array (
            'Cache-Control'     => 'no-cache, no-store, max-age=0, must-revalidate',
            'Pragma'            => 'no-cache',
            'Access-Control-Allow-Origin'  => ACCESS_CONTROL_ALLOW_ORIGIN
        );

        $response = new Response($body, 200, $headers);

        return $response;
    }

    private function codigoParaReservarBiciPublicaEnSegundoCalendario()
    {
        for ($i = 0; $i < $numberBicycles; $i++) 
        { 
            if ( $calendars[$i]->isEventPublicDisponible( Helper::reverseDate($input['fecha']), $intervalTime, $input['personas']) ) 
            { 
                if ($i == 0) 
                {
                    $isEventPublicDisponible = true;
                    $idCalendar = $this->calendars[$i];
                    break; 
                }
                else
                {
                   
                    if ($calendars[$i]->havePrivateEvent(Helper::reverseDate($input['fecha']))) 
                    {
                        $isEventPublicDisponible = true;
                        $idCalendar = $this->calendars[$i];
                        break; 
                    }
                }  
            }
        }

        if ( ! $isEventPublicDisponible) 
        {
            $body = 'Puede que el horario ya no esté disponible o que el numero de personas disponible para reservar en esta hora sea menor del que has solicitado. Ponte en contacto con nostros para hacer la reserva. Datos de contacto: diversion@biciloca.com | 634556224';
            $response = new Response($body, 200, $headers);
            return $response;  
        }            
    }

    public function post_event()
    {
        try {
            $service = $this->getServiceCalendar();
            $calendars = [];
            $input = $_POST;
            $isEventPublicDisponible = false;
            $isEventPrivateDisponible = false;
            $headers = $this->getHeadersHttpResponse();
            $numberBicycles = $this->getNumberBicyclesDisponibles(Helper::reverseDate($input['fecha']));
            $calendars = $this->getCalendars();
            $intervalTime = $this->getIntervalTime($input['horainicio'], $input['horafin']);
            $numberBicyclesToOpenCalendar = Model_Config::find(1);
            $limitOpenNewPublicBicycle = Model_Config::find(2);
            $inputPago = $input['pago'];
            $searchString = ',';

            if( strpos($inputPago, $searchString) !== false ) {
                 $inputPago = (float)str_replace(',', '.', $input['pago']);
            }

            // helper::dd($calendars);

            if ($input['tipo'] == 'share') 
            {
                for ($i = 0; $i < $numberBicycles; $i++) 
                { 
                    if ( $calendars[$i]->isEventPublicDisponible( Helper::reverseDate($input['fecha']), $intervalTime, $input['personas'], $limitOpenNewPublicBicycle) ) 
                    { 
                        $isEventPublicDisponible = true;
                        $idCalendar = $this->calendars[$i];
                    }

                    break;
                }

                if ( ! $isEventPublicDisponible) 
                {
                    $body = 'Puede que el horario ya no esté disponible o que el numero de personas disponible para reservar en esta hora sea menor del que has solicitado. Ponte en contacto con nostros para hacer la reserva. Datos de contacto: diversion@biciloca.com | 634556224';
                    $response = new Response($body, 200, $headers);
                    return $response;  
                }            
            }
            else
            {
                for ($i = 0; $i < $numberBicycles; $i++) 
                { 
                    if ( $calendars[$i]->isEventPrivateDisponible( Helper::reverseDate($input['fecha']), $intervalTime ) ) 
                    {
                        $isEventPrivateDisponible = true;
                        $idCalendar = $this->calendars[$i];
                        break;  
                    }
                    else
                    {
                        if ($calendars[$i]->getNumberBicycles(Helper::reverseDate($input['fecha'])) < $numberBicyclesToOpenCalendar['value'] ) 
                        {
                            $body = 'Puede que el horario ya no esté disponible. Si necesita atención personalizada para este caso pógase en contacto con nosotros. Datos de contacto: diversion@biciloca.com | 634556224';
                            $response = new Response($body, 200, $headers);
                            return $response;  
                        }
                    }
                }

                if ( ! $isEventPrivateDisponible) 
                {
                    $body = 'Puede que el horario ya no esté disponible. Si necesita atención personalizada para este caso pógase en contacto con nosotros. Datos de contacto: diversion@biciloca.com | 634556224';
                    $response = new Response($body, 200, $headers);
                    return $response;  
                }            
            }

            // $body = $input['pago'];
            // $response = new Response($body, 200, $headers);
            // return $response;

            $horainicio = Helper::addTime($input['horainicio'], 30);
            $horafin = Helper::addTime($input['horafin'], 30);
            $minutos = Helper::diferenceHours($horainicio, $horafin);
            $start = Helper::reverseDate($input['fecha']) . 'T' . $horainicio . ':00';
            $end = Helper::reverseDate($input['fecha']) . 'T' . $horafin . ':00';

            $coupon = new Coupon($input['codigo'], $inputPago);
            $alias = $coupon->getAlias();
            // Helper::dd($coupon);

            $input['nombre'] = $coupon->getName() . ' ' . $input['nombre'];

            if ($input['tipo'] == 'privado') 
            {
                $data = $input['nombre'] . ' ' . $input['apellidos'] . " ($alias)" .' - ' . $input['personas'] . 'Pax - ' . $minutos . 'min - ' . $input['telefono'] . ' - Ramblas - ' . $coupon->getPayOffline() . '#'; 
            }
            else
            {
                $input['tipo'] = '[share]';
                $data = $input['tipo'] . ' - ' . $input['nombre'] . ' ' . $input['apellidos'] . " ($alias)" .' - ' . $input['personas'] . 'Pax - ' . $minutos . 'min - ' . $input['telefono'] . ' - Ramblas - 0#';  
            }

            // Set your API key private test biciloca
            // Stripe::setApiKey("sk_test_lsAzc1wAsUiM7Fkje4bK1oOD");

            // Set your API key private live biciloca
            Stripe::setApiKey("sk_live_EDkX8hsyk81Sy422pgujnSDR");

            // Helper::dd($coupon->getPriceWithDiscount());
             
            try {
                if ($input['tipo'] == '[share]') 
                {
                    $precio = (float)number_format(25 * $input['personas'], 2, '.', '');

                    if ($coupon->getPriceWithDiscount() != 0) 
                    {
                        \Stripe\Charge::create([
                            'amount' => $precio * 100, // this is in cents: $20
                            'currency' => 'eur',
                            // 'card' => 'tok_1A2HsgISCEu4uG0ig62Oa51K',
                            'card' => $input['stripeToken'],
                            'description' => $data
                        ]);  
                    }                   
                }
                else
                {
                    if ($coupon->getPriceWithDiscount() != 0) 
                    {
                        $amountToPay = $coupon->getPriceWithDiscount() * 100;

                        if ($coupon->name == "cupontest") {
                            $amountToPay = (float)number_format(0.25, 2, '.', '');
                        }

                        \Stripe\Charge::create([
                            'amount' => $amountToPay, // this is in cents: $20
                            'currency' => 'eur',
                            // 'card' => 'tok_1A2HsgISCEu4uG0ig62Oa51K',
                            'card' => $input['stripeToken'],
                            'description' => $data
                        ]);

                        $precio = (float)number_format($coupon->getPriceWithDiscount(), 2, '.', '');

                        if ($coupon->name == "cupontest") {
                            $precio = (float)number_format(0.25, 2, '.', '');
                        }
                    }
                    else
                    {
                        $precio = (float)$inputPago;

                        if ($coupon->name == "cupontest") {
                            $precio = (float)number_format(0.25, 2, '.', '');
                        }
                    }  
                }
                
            } catch (Stripe_CardError $e) {
                // Declined. Don't process their purchase.
                // Go back, and tell the user to try a new card

                $body = 'error';
                $response = new Response($body, 200, $headers);
                return $response;
            }    

            $event = new Google_Service_Calendar_Event(array(
              'summary' => $data,
              'description' => $data,
              'start' => array(
                'dateTime' => $start,
                'timeZone' => 'Europe/Madrid',
              ),
              'end' => array(
                'dateTime' => $end,
                'timeZone' => 'Europe/Madrid',
              ),
            ));

            

            $event = $service->events->insert($idCalendar, $event);

            $dataEmail = [
                            'nombre' => $input['nombre'],
                            'apellidos' => $input['apellidos'],
                            'personas' => $input['personas'],
                            'fecha' => $input['fecha'],
                            'horainicio' => $input['horainicio'],
                            'horafin' => $input['horafin'],
                            'duracion' => $minutos,
                            'telefono' => $input['telefono'],
                            'precio' => $precio,
                            'email' => $input['email']
            ];

            $this->formatLog('Stripe POST' . $this->formatArrayToLog($dataEmail));

            $this->formatLog('Stripe Data email:' . $this->formatArrayToLog($_POST));

            // si hay error en el envio mirar el array que devuelve la funcion
            $responseEmail = $this->sendEmail(
                      ['template' => 'emailReserva', 'data' => $dataEmail], 
                      ['email' => $input['email'], 'name' => $input['nombre'] . ' ' . $input['apellidos']],
                      'BeerBikeBarcelona reserve complete'
                    );

            $this->sendEmail(
                      ['template' => 'emailReserva', 'data' => $dataEmail], 
                      ['email' => 'diversion@biciloca.com', 'name' => 'BookingSystem'],
                      'New reserve BeerBikeBarcelona'
                    );

            $this->sendEmail(
                      ['template' => 'emailReserva', 'data' => $dataEmail], 
                      ['email' => 'yalach@gmail.com', 'name' => 'BookingSystem'],
                      'New reserve BeerBikeBarcelona'
                    );

            try {
                $booking = new Model_Bookings();
                $booking->name = $input['nombre'];
                $booking->surname = $input['apellidos'];
                $booking->type = $input['tipo'];
                $booking->persons = $input['personas'];
                $booking->date = $input['fecha'];
                $booking->start_hour = $input['horainicio'];
                $booking->end_hour = $input['horafin'];
                $booking->phone = $input['telefono'];
                $booking->price = (float)number_format($coupon->getPriceWithDiscount(), 2, '.', '');
                $booking->email = $input['email'];
                $booking->created_at = date("Y-m-d H:i:s", strtotime ( '+1 hour' , strtotime ( date("Y-m-d H:i:s") ) ) );
                $booking->updated_at = date("Y-m-d H:i:s", strtotime ( '+1 hour' , strtotime ( date("Y-m-d H:i:s") ) ) );
                $booking->save();
            } catch (Exception $e) {
                
            }
            
            $body = 'Reserva realizada. Le acabamos de enviar un email de confirmación. En caso de no recibirlo póngase en contacto con nosotros. Datos de contacto: diversion@biciloca.com | 0034 634556224';
            $response = new Response($body, 200, $headers);
            return $response;
        } 
        catch (Exception $e) 
        {
            $body = $e;

            $headers = array (
                'Cache-Control'     => 'no-cache, no-store, max-age=0, must-revalidate',
                'Pragma'            => 'no-cache',
                'Access-Control-Allow-Origin'  => ACCESS_CONTROL_ALLOW_ORIGIN
            );

            $response = new Response($body, 500, $headers);
            return $response;
        }
    }

    public function post_sentTestEmail() {
        $input = $_POST;

        $dataEmail = [
                            'nombre' => $input['nombre'],
                            'apellidos' => $input['apellidos'],
                            'personas' => $input['personas'],
                            'fecha' => $input['fecha'],
                            'horainicio' => $input['horainicio'],
                            'horafin' => $input['horafin'],
                            'duracion' => $input['minutos'],
                            'telefono' => $input['telefono'],
                            'precio' => $input['precio'],
                            'email' => $input['email']
            ];

            $responseEmail = $this->sendEmail(
                      ['template' => 'emailReserva', 'data' => $dataEmail], 
                      ['email' => $input['email'], 'name' => 'titulo'],'Reserva de test');
    }

    public function post_create7PrivateBicycles()
    {
        $service = $this->getServiceCalendar();

        $fecha = '2017-12-01';
        $horaInicio = '11:00';
        $horaFin = '12:00';

        for ($i=0; $i < 7; $i++) 
        { 
            $event = new Google_Service_Calendar_Event(array(
              'summary' => 'julio prueba () - 12Pax - 1h - 649637377 - Ramblas - 165.40#',
              'description' => 'julio prueba () - 12Pax - 1h - 649637377 - Ramblas - 165.40#',
              'start' => array(
                'dateTime' => $fecha . 'T' . $horaInicio . ':00',
                'timeZone' => 'Europe/Madrid',
              ),
              'end' => array(
                'dateTime' => $fecha . 'T' . $horaFin . ':00',
                'timeZone' => 'Europe/Madrid',
              ),
            ));

            $horaInicio = $this->addHour($horaInicio);
            $horaFin = $this->addHour($horaFin);

            $event = $service->events->insert('7n6gcsuifsoa20rp2qkjfdognk@group.calendar.google.com', $event);
        }

        helper::dd('eventos creados');
    }

    private function addHour($hour)
    {
        $splitHour = explode(':', $hour);
        $hours = $splitHour[0] + 1;
        $hourFinish = $hours . ':00';
        return $hourFinish; 
    }

    private function mergeDisponibilityAllCalendars($hoursDisponible)
    {

        $mergeDisponibilityCalendars = [];

        foreach ($hoursDisponible as $key => $hours) 
        {
            foreach ($hours as $key => $hour) 
            {
                $mergeDisponibilityCalendars[] = $hour;
                // Helper::dd($mergeDisponibilityCalendars);
            }
        }

        // Helper::dd($mergeDisponibilityCalendars);

        $hoursDisponible = array_map("unserialize", array_unique(array_map("serialize", $mergeDisponibilityCalendars)));

        return $hoursDisponible;
    }

    public function post_redsys()
    {
        $input = $_POST;
        $headers = $this->getHeadersHttpResponse();
        $inputPago = $input['pago'];

        $searchString = ',';

        if( strpos($inputPago, $searchString) !== false ) {
             $inputPago = (float)str_replace(',', '.', $input['pago']);
        }

        $precio = (float)number_format($input['personas'] * $inputPago * 100, 2, '.', '');

        $session = Session::instance();
        $session->set('personas', $input['personas']);
        $session->set('pago', $inputPago);
        $session->set('comentarios', $input['comentarios']);
                  
        define('REQUEST_DEV', 'https://sis-t.redsys.es:25443/sis/realizarPago');
        define('REQUEST_PROD', 'https://sis.redsys.es/sis/realizarPago');

        $redsys = new RedsysAPI;
        $url = "";    
        $moneda="978";
        $trans="0";
        $id=time();
        
        $redsys->setParameter("DS_MERCHANT_ORDER", strval($id));
        $redsys->setParameter("DS_MERCHANT_MERCHANTCODE", MERCHANT_DEV);
        $redsys->setParameter("DS_MERCHANT_CURRENCY", $moneda);
        $redsys->setParameter("DS_MERCHANT_TRANSACTIONTYPE", $trans);
        $redsys->setParameter("DS_MERCHANT_TERMINAL", TERMINAL);
        $redsys->setParameter("DS_MERCHANT_MERCHANTURL", $url);
        $redsys->setParameter("DS_MERCHANT_URLOK", URL_RESPONSE_TPV);     
        $redsys->setParameter("DS_MERCHANT_URLKO", URL_ERROR_TPV);
        $redsys->setParameter("DS_MERCHANT_AMOUNT", $precio);
        
        $version="HMAC_SHA256_V1";
        $key = KEY; 
        $params = $redsys->createMerchantParameters();
        $signature = $redsys->createMerchantSignature($key); 

        $dataEmail = [
                                'mensaje' => 'inicio del proceso de pago con safety tpv',
                                'comentarios' => Session::get('comentarios'),
                                'personas' => Session::get('personas'),
                                'pago' => Session::get('pago')
                    ];

        $this->sendMail(['email' => 'yalach@gmail.com', 'name' => 'Julio'], ['data' => $dataEmail], 'reservaRealizadaSafety');

        echo "<html lang='es'>
                    <head>
                    </head>
                    <body>
                        <form name='frm' action='" . REQUEST_PROD . "' method='POST'>
                            <input type='hidden' name='Ds_SignatureVersion' value='" . $version . "'/></br>
                            <input type='hidden' name='Ds_MerchantParameters' value='" . $params . "'/></br>
                            <input type='hidden' name='Ds_Signature' value='" . $signature . "'/></br>
                        </form>
                        <script>document.forms[0].submit();</script>
                    </body>
                  </html>";           
    }

    public function get_redsysTpv()
    {
        $redsys = new RedsysAPI;
        $headers = $this->getHeadersHttpResponse();

        if ( ! empty( $_GET ) ) 
        {             
            $version = $_GET["Ds_SignatureVersion"];
            $datos = $_GET["Ds_MerchantParameters"];
            $signatureRecibida = $_GET["Ds_Signature"];
            
            $decodec = $redsys->decodeMerchantParameters($datos);    
            $kc = KEY;
            $firma = $redsys->createMerchantSignatureNotif($kc, $datos);  

            $redsysResponse = json_decode($decodec);
        
            if ($firma === $signatureRecibida and $redsysResponse->Ds_Response == 0000)
            {
                $dataEmail = [
                                'mensaje' => 'pago realizado con exito en safety tpv',
                                'comentarios' => Session::get('comentarios') . " - transaction: " . $redsysResponse->Ds_Order,
                                'personas' => Session::get('personas'),
                                'pago' => Session::get('pago')
                    ];

                $this->sendMail(['email' => 'diversion@biciloca.com', 'name' => 'Biciloca'], ['data' => $dataEmail], 'reservaRealizadaSafety');
                $this->sendMail(['email' => 'yalach@gmail.com', 'name' => 'Julio'], ['data' => $dataEmail], 'reservaRealizadaSafety');

                echo "<html lang='es'>
                    <head>
                    </head>
                    <body>
                        <form action='http://safety-payment.online/success.html' method='GET'>
                            
                        </form>
                        <script>document.forms[0].submit();</script>
                    </body>
                  </html>";  
            } 
            else 
            {
                
                $dataEmail = [
                                'mensaje' => 'error en el pago con safety tpv',
                                'comentarios' => Session::get('comentarios') . " - transaction: " . $redsysResponse->Ds_Order,
                                'personas' => Session::get('personas'),
                                'pago' => Session::get('pago')
                    ];

                    $this->sendEmail(
                                  ['template' => 'intentoReservaSafety', 'data' => $dataEmail], 
                                  ['email' => 'yalach@gmail.com', 'name' => 'Safety tpv'],
                                  'Intento de pago con safety tpv'
                                );

                echo "<html lang='es'>
                    <head>
                    </head>
                    <body>
                        <form action='http://safety-payment.online/error.html' method='GET'>
                            
                        </form>
                        <script>document.forms[0].submit();</script>
                    </body>
                  </html>";
            }
        }
        else
        {
            $body = 'Póngase en contacto con nosotros para confirmar el pago. Gracias. Datos de contacto: joaquin.t.a.89@gmail.com | +376 369 835';
            $response = new Response($body, 200, $headers);
            return $response;
        }
    }

    private function formatUrlResponse($url, $page)
    {
        $explodeUrl = explode("/", $url);
        $explodeUrl = array_diff($explodeUrl, ['confirmarpago.html']);
        $explodeUrl[] = $page;
        return implode("/", $explodeUrl);
    }

    private function validateRequiredParams($inputData, $validateData)
    {
        $cont = 0;
        foreach ($validateData as $keyValidateData => $valueValidateData) 
        {
            foreach ($inputData as $keyInputData => $valueInputData) 
            {
                if ($keyInputData == $keyValidateData) 
                {
                    $cont = $cont + 1;
                }
            }
        }

        if ($cont == count($validateData)) 
        {
             return true;
        }
        else
        {
            return false;
        }
        
    }

    private function isEmptySomeValue($inputData, $validateData)
    {
        foreach ($validateData as $keyValidateData => $valueValidateData) 
        {
            foreach ($inputData as $keyInputData => $valueInputData) 
            {
                if ($keyInputData == $keyValidateData && ($valueInputData == '' || $valueInputData == ' ')) 
                {
                    return true;
                }
            }
        }

        return false;
    }

    private function formatView($url)
    {
        return "<html lang='es'>
                            <head>
                            </head>
                            <body>
                                <form name='frm' action='" . $url . "' method='GET'>
                                    
                                </form>
                                <script>document.forms[0].submit();</script>
                            </body>
                          </html>";
    }

    public function post_testfinal()
    {
        
        try {
            $service = $this->getServiceCalendar();
            $calendars = [];
            $input = $_POST;
            $this->formatLog('Inicia un intento de reserva: ' . $this->formatArrayToLog($_POST));
            $isEventPublicDisponible = false;
            $isEventPrivateDisponible = false;
            $headers = $this->getHeadersHttpResponse();
            $numberBicycles = $this->getNumberBicyclesDisponibles(Helper::reverseDate($input['fecha']));
            $calendars = $this->getCalendars();
            $intervalTime = $this->getIntervalTime($input['horainicio'], $input['horafin']);
            $numberBicyclesToOpenCalendar = Model_Config::find(1);
            $limitOpenNewPublicBicycle = Model_Config::find(2);
            $conCupon = false;

            $inputPago = $input['pago'];

            $searchString = ',';

            if( strpos($inputPago, $searchString) !== false ) {
                 $inputPago = (float)str_replace(',', '.', $input['pago']);
            }

            $validateData = ['nombre' => '', 'apellidos' => '', 'personas' => '', 'fecha' => '',  'horainicio' =>  '', 'horafin' => '', 'telefono' => '', 'tipo' => '', 'pago' => '', 'email' => '' ];
            if ( ! $this->validateRequiredParams($input, $validateData)) 
            {
                $this->formatLog('No se enviaron todos los parémetros requeridos: ' . $this->formatArrayToLog($input));

                $url = $this->formatUrlResponse($input['cliente'], 'errorGeneral.html');
                echo $this->formatView($url);
                exit;
            }

            if ( $this->isEmptySomeValue($input, $validateData)) 
            {
                $this->formatLog('Algún parámetro requerido está vacío: ' . $this->formatArrayToLog($input));

                $url = $this->formatUrlResponse($input['cliente'], 'errorGeneral.html');
                echo $this->formatView($url);
                exit;
            }

            $dataEmail = [
                                'mensaje' => 'inicio del intento de reserva',
                                'nombre' => $input['nombre'],
                                'apellidos' => $input['apellidos'],
                                'personas' => $input['personas'],
                                'fecha' => $input['fecha'],
                                'horainicio' => $input['horainicio'],
                                'horafin' => $input['horafin'],
                                'telefono' => $input['telefono'],
                                'tipo' => $input['tipo'],
                                'pago' => $inputPago,
                                'email' => $input['email']
            ];

            $this->sendMail(['email' => 'yalach@gmail.com', 'name' => 'Julio'], ['data' => $dataEmail], 'intentoReserva');
            
            // helper::dd($calendars);

            if ($input['tipo'] == 'share') 
            {
                for ($i = 0; $i < $numberBicycles; $i++) 
                { 
                    if ( $calendars[$i]->isEventPublicDisponible( Helper::reverseDate($input['fecha']), $intervalTime, $input['personas'], $limitOpenNewPublicBicycle) ) 
                    { 
                        $isEventPublicDisponible = true;
                        $idCalendar = $this->calendars[$i];
                    }

                    break;
                }

                if ( ! $isEventPublicDisponible) 
                {
                    // $body = 'Puede que el horario ya no esté disponible o que el numero de personas disponible para reservar en esta hora sea menor del que has solicitado. Ponte en contacto con nostros para hacer la reserva. Datos de contacto: diversion@biciloca.com | 634556224';
                    // $response = new Response($body, 200, $headers);
                    // return $response;  
                    $this->formatLog('El evento público no está disponible: ' . $this->formatArrayToLog($_POST));
                    $dataEmail = [
                                'mensaje' => 'el evento publico no esta disponible',
                                'nombre' => $input['nombre'],
                                'apellidos' => $input['apellidos'],
                                'personas' => $input['personas'],
                                'fecha' => $input['fecha'],
                                'horainicio' => $input['horainicio'],
                                'horafin' => $input['horafin'],
                                'telefono' => $input['telefono'],
                                'tipo' => $input['tipo'],
                                'pago' => $inputPago,
                                'email' => $input['email']
                    ];

                    $this->sendEmail(
                                  ['template' => 'intentoReserva', 'data' => $dataEmail], 
                                  ['email' => 'yalach@gmail.com', 'name' => 'Biciloca'],
                                  'Intento Reserva Biciloca'
                                );

                    $url = $this->formatUrlResponse($input['cliente'], 'errorInsuficienteEspacio.html');

                    echo "<html lang='es'>
                                <head>
                                </head>
                                <body>
                                    <form name='frm' action='" . $url . "' method='GET'>
                                        
                                    </form>
                                    <script>document.forms[0].submit();</script>
                                </body>
                              </html>";

                    
                }            
            }
            else
            {
                

                for ($i = 0; $i < $numberBicycles; $i++) 
                { 
                    if ( $calendars[$i]->isEventPrivateDisponible( Helper::reverseDate($input['fecha']), $intervalTime ) ) 
                    {
                        $isEventPrivateDisponible = true;
                        $idCalendar = $this->calendars[$i];
                        break;  
                    }
                    else
                    {
                        if ($calendars[$i]->getNumberBicycles(Helper::reverseDate($input['fecha'])) < $numberBicyclesToOpenCalendar['value'] ) 
                        {
                            $this->formatLog('El número de bicicletas disponibles no es suficiente: ' . $this->formatArrayToLog($_POST));
                            $dataEmail = [
                                        'mensaje' => 'El numero de bicicletas disponibles no es suficiente',
                                        'nombre' => $input['nombre'],
                                        'apellidos' => $input['apellidos'],
                                        'personas' => $input['personas'],
                                        'fecha' => $input['fecha'],
                                        'horainicio' => $input['horainicio'],
                                        'horafin' => $input['horafin'],
                                        'telefono' => $input['telefono'],
                                        'tipo' => $input['tipo'],
                                        'pago' => $inputPago,
                                        'email' => $input['email']
                            ];

                            $this->sendEmail(
                                          ['template' => 'intentoReserva', 'data' => $dataEmail], 
                                          ['email' => 'yalach@gmail.com', 'name' => 'Biciloca'],
                                          'Intento Reserva Biciloca'
                                );

                            $url = $this->formatUrlResponse($input['cliente'], 'errorInsuficienteEspacio.html');

                            echo "<html lang='es'>
                                        <head>
                                        </head>
                                        <body>
                                            <form name='frm' action='" . $url . "' method='GET'>
                                                
                                            </form>
                                            <script>document.forms[0].submit();</script>
                                        </body>
                                  </html>"; 
                            $this->formatLog('Se muestra vista con error de insuficiente espacio: ' . $this->formatArrayToLog($_POST));
                            echo "<p>Puede que el horario ya no esté disponible o que el numero de personas disponible para reservar en esta hora sea menor del que has solicitado. Ponte en contacto con nosotros para hacer la reserva. Datos de contacto: diversion@biciloca.com | 634556224</p>";
                            exit; 
                        }
                    }
                }

                if ( ! $isEventPrivateDisponible) 
                {
                    $this->formatLog('El evento privado no está  disponible: ' . $this->formatArrayToLog($_POST));
                    $dataEmail = [
                                'mensaje' => 'el evento privado no esta disponible',
                                'nombre' => $input['nombre'],
                                'apellidos' => $input['apellidos'],
                                'personas' => $input['personas'],
                                'fecha' => $input['fecha'],
                                'horainicio' => $input['horainicio'],
                                'horafin' => $input['horafin'],
                                'telefono' => $input['telefono'],
                                'tipo' => $input['tipo'],
                                'pago' => $inputPago,
                                'email' => $input['email']
                    ];

                    $this->sendEmail(
                                  ['template' => 'intentoReserva', 'data' => $dataEmail], 
                                  ['email' => 'yalach@gmail.com', 'name' => 'Biciloca'],
                                  'Intento Reserva Biciloca'
                                );

                    $url = $this->formatUrlResponse($input['cliente'], 'errorInsuficienteEspacio.html');

                    echo "<html lang='es'>
                                <head>
                                </head>
                                <body>
                                    <form name='frm' action='" . $url . "' method='GET'>
                                        
                                    </form>
                                    <script>document.forms[0].submit();</script>
                                </body>
                              </html>";
                }            
            }



            // $body = $input['pago'];
            // $response = new Response($body, 200, $headers);
            // return $response;

            $horainicio = Helper::addTime($input['horainicio'], 30);
            $horafin = Helper::addTime($input['horafin'], 30);
            $minutos = Helper::diferenceHours($horainicio, $horafin);
            $start = Helper::reverseDate($input['fecha']) . 'T' . $horainicio . ':00';
            $end = Helper::reverseDate($input['fecha']) . 'T' . $horafin . ':00';

            $coupon = new Coupon($input['codigo'], $inputPago);
            $alias = $coupon->getAlias();
            // Helper::dd($coupon);
            $this->formatLog('coupon: ' . $this->formatArrayToLog($coupon->getCouponInfoArray()));
            $input['nombre'] = $coupon->getName() . ' ' . $input['nombre'];

            if ($input['tipo'] == 'privado') 
            {
                $data = $input['nombre'] . ' ' . $input['apellidos'] . " ($alias)" .' - ' . $input['personas'] . 'Pax - ' . $minutos . 'min - ' . $input['telefono'] . ' - Ramblas - ' . $coupon->getPayOffline() . '#'; 
            }
            else
            {
                $input['tipo'] = '[share]';
                $data = $input['tipo'] . ' - ' . $input['nombre'] . ' ' . $input['apellidos'] . " ($alias)" .' - ' . $input['personas'] . 'Pax - ' . $minutos . 'min - ' . $input['telefono'] . ' - Ramblas - 0#';  
            }

            

            define('REQUEST_DEV', 'https://sis-t.redsys.es:25443/sis/realizarPago');
            define('REQUEST_PROD', 'https://sis.redsys.es/sis/realizarPago');

            $redsys = new RedsysAPI;
            $url = "";    
            $moneda="978";
            $trans="0";
            $id=time();
            
            $redsys->setParameter("DS_MERCHANT_ORDER", strval($id));
            $redsys->setParameter("DS_MERCHANT_MERCHANTCODE", MERCHANT_DEV);
            $redsys->setParameter("DS_MERCHANT_CURRENCY", $moneda);
            $redsys->setParameter("DS_MERCHANT_TRANSACTIONTYPE", $trans);
            $redsys->setParameter("DS_MERCHANT_TERMINAL", TERMINAL);
            $redsys->setParameter("DS_MERCHANT_MERCHANTURL", URL_MERCHANT);
            $redsys->setParameter("DS_MERCHANT_URLOK", URL_RESPONSE);     
            $redsys->setParameter("DS_MERCHANT_URLKO", URL_ERROR);
            //$redsys->setParameter("DS_MERCHANT_PAN", $_POST['card']);
            //$redsys->setParameter("DS_MERCHANT_EXPIRYDATE", $_POST['fechaCaducidad']);
            //$redsys->setParameter("DS_MERCHANT_CVV2", $_POST['cvc']);
            
            $version="HMAC_SHA256_V1";
            $key = KEY; 

            $dataGoogleCalendar = Session::instance();
            $cache = new Model_Cache();

            $idRedsys = mt_rand(10, 100000000000);
            $dataGoogleCalendar->set('idRedsys', $idRedsys);
            $dataGoogleCalendar->set('data', $data);
            $dataGoogleCalendar->set('start', $start);
            $dataGoogleCalendar->set('end', $end);

            $dataGoogleCalendar->set('idCalendar', $idCalendar);
            $dataGoogleCalendar->set('nombre', $input['nombre']);
            $dataGoogleCalendar->set('apellidos', $input['apellidos']);
            $dataGoogleCalendar->set('tipo', $input['tipo']);
            $dataGoogleCalendar->set('personas', $input['personas']);
            $dataGoogleCalendar->set('fecha', $input['fecha']);
            $dataGoogleCalendar->set('horainicio', $input['horainicio']);
            $dataGoogleCalendar->set('horafin', $input['horafin']);
            $dataGoogleCalendar->set('duracion', $minutos);
            $dataGoogleCalendar->set('telefono', $input['telefono']);
            $dataGoogleCalendar->set('email', $input['email']);
            $dataGoogleCalendar->set('coupon', $coupon);

            $cache->idRedsys = $idRedsys;
            $cache->data = $data;
            $cache->start = $start;
            $cache->end = $end;
            $cache->idCalendar = $idCalendar;
            $cache->nombre = $input['nombre'];
            $cache->apellidos = $input['apellidos'];
            $cache->tipo = $input['tipo'];
            $cache->personas = $input['personas'];
            $cache->fecha = $input['fecha'];
            $cache->horainicio = $input['horainicio'];
            $cache->horafin = $input['horafin'];
            $cache->duracion = $minutos;
            $cache->telefono = $input['telefono'];
            $cache->email = $input['email'];
            $cache->created_at = date("Y-m-d H:i:s", strtotime ( '+1 hour' , strtotime ( date("Y-m-d H:i:s") ) ) );
            $cache->updated_at = date("Y-m-d H:i:s", strtotime ( '+1 hour' , strtotime ( date("Y-m-d H:i:s") ) ) );
            

            if (isset($input['cliente'])) 
            {
                $dataGoogleCalendar->set('urlSuccess', $this->formatUrlResponse($input['cliente'], 'success.html'));
                $dataGoogleCalendar->set('urlSuccessPosible', $this->formatUrlResponse($input['cliente'], 'successPosible.html'));
                $dataGoogleCalendar->set('urlError', $this->formatUrlResponse($input['cliente'], 'error.html'));

                $cache->urlSuccess = $this->formatUrlResponse($input['cliente'], 'success.html');
                $cache->urlSuccessPosible = $this->formatUrlResponse($input['cliente'], 'successPosible.html');
                $cache->urlError = $this->formatUrlResponse($input['cliente'], 'error.html');
            }
            
            if ($input['tipo'] == '[share]') 
            {
                if ($coupon->getPriceWithDiscount() != 0) 
                {
                    $precio = (float)number_format($_POST['personas'] * 25 * 100, 2, '.', '');

                    if ($coupon->name == "cupontest") {
                        $precio = (float)number_format(0.25 * 100, 2, '.', '');
                    }

                    $dataGoogleCalendar->set('precio', $precio);
                    $cache->precio = $precio;
                    $cache->save();
                    $redsys->setParameter("DS_MERCHANT_AMOUNT", $precio);  
                    $params = $redsys->createMerchantParameters();
                    $signature = $redsys->createMerchantSignature($key);

                    $this->formatLog('[share] - Pago con redsys: ' . $precio / 100 . '€ ' . $this->formatArrayToLog($_POST));

                    echo "<html lang='es'>
                                <head>
                                </head>
                                <body>
                                    <form name='frm' action='" . REQUEST_PROD . "' method='POST'>
                                        <input type='hidden' name='Ds_SignatureVersion' value='" . $version . "'/></br>
                                        <input type='hidden' name='Ds_MerchantParameters' value='" . $params . "'/></br>
                                        <input type='hidden' name='Ds_Signature' value='" . $signature . "'/></br>
                                    </form>
                                    <script>document.forms[0].submit();</script>
                                </body>
                              </html>";  
                }
                else
                {
                    $precio = (float)$inputPago;
                    $dataGoogleCalendar->set('precio', $precio);
                    $cache->precio = $precio;
                    $cache->save();
                    $conCupon = true;
                    $this->formatLog('[Share] - Pago con cupon (no pasa por redsys): ' . $precio . '€ ' . $this->formatArrayToLog($_POST));
                }  
            }
            else
            {
                
                if ($coupon->getPriceWithDiscount() != 0) 
                {
                    $precio = (float)number_format($coupon->getPriceWithDiscount() * 100, 2, '.', '');
                    $dataGoogleCalendar->set('precio', $precio);
                    $cache->precio = $precio;
                    $cache->save();
                    $redsys->setParameter("DS_MERCHANT_AMOUNT", $precio);  
                    $params = $redsys->createMerchantParameters();
                    $signature = $redsys->createMerchantSignature($key); 

                     /*$curl = Request::forge(REQUEST_DEV, 'curl');
                     $curl->set_header('Content-Type', 'application/x-www-form-urlencoded');
                     $curl->set_params(array('Ds_SignatureVersion' => $version, 'Ds_MerchantParameters' => $params, 'Ds_Signature' => $signature));
                     $curl->execute();

                     $headers = $this->getHeadersHttpResponse();
                     $response = new Response('trace', 200, $headers);
                     return $response;*/

                    $this->formatLog('[privado] - Pago con redsys: ' . $precio / 100 . '€ ' . $this->formatArrayToLog($_POST));

                    echo "<html lang='es'>
                                <head>
                                </head>
                                <body>
                                    <form name='frm' action='" . REQUEST_PROD . "' method='POST'>
                                        <input type='hidden' name='Ds_SignatureVersion' value='" . $version . "'/></br>
                                        <input type='hidden' name='Ds_MerchantParameters' value='" . $params . "'/></br>
                                        <input type='hidden' name='Ds_Signature' value='" . $signature . "'/></br>
                                    </form>
                                    <script>document.forms[0].submit();</script>
                                </body>
                              </html>";  

                    // return new Response('trace', 200, $headers);
                }
                else
                {
                    $precio = (float)$inputPago;
                    $dataGoogleCalendar->set('precio', $precio);
                    $cache->precio = $precio;
                    $cache->save();
                    $conCupon = true;
                    $this->formatLog('[privado] - Pago con cupon (no pasa por redsys): ' . $precio . ' ' . $this->formatArrayToLog($_POST));
                }    
            } 

            if ($conCupon) 
            {
                

                $event = new Google_Service_Calendar_Event(array(
                  'summary' => $data,
                  'description' => $data,
                  'start' => array(
                    'dateTime' => $start,
                    'timeZone' => 'Europe/Madrid',
                  ),
                  'end' => array(
                    'dateTime' => $end,
                    'timeZone' => 'Europe/Madrid',
                  ),
                ));

                $event = $service->events->insert($idCalendar, $event);

                $this->formatLog('Evento en calendar con cupon (no pasa por redsys): ' .  $this->formatArrayToLog($_POST));

                $dataEmail = [
                                'mensaje' => 'Reserve successfully',
                                'nombre' => $input['nombre'],
                                'apellidos' => $input['apellidos'],
                                'personas' => $input['personas'],
                                'fecha' => $input['fecha'],
                                'horainicio' => $input['horainicio'],
                                'horafin' => $input['horafin'],
                                'duracion' => $minutos,
                                'telefono' => $input['telefono'],
                                'precio' => $precio,
                                'email' => $input['email']
                ];

                $this->sendMail(['email' => $input['email'], 'name' => $input['nombre'] . ' ' . $input['apellidos']], ['data' => $dataEmail], 'reservaRealizada');
                $this->sendMail(['email' => 'diversion@biciloca.com', 'name' => 'Biciloca'], ['data' => $dataEmail], 'reservaRealizada');
                $this->sendMail(['email' => 'yalach@gmail.com', 'name' => 'Julio'], ['data' => $dataEmail], 'reservaRealizada');

                try {
                    $booking = new Model_Bookings();
                    $booking->name = $input['nombre'];
                    $booking->surname = $input['apellidos'];
                    $booking->type = $input['tipo'];
                    $booking->persons = $input['personas'];
                    $booking->date = $input['fecha'];
                    $booking->start_hour = $input['horainicio'];
                    $booking->end_hour = $input['horafin'];
                    $booking->phone = $input['telefono'];
                    $booking->price = (float)number_format($coupon->getPriceWithDiscount(), 2, '.', '');
                    $booking->email = $input['email'];
                    $booking->created_at = date("Y-m-d H:i:s", strtotime ( '+1 hour' , strtotime ( date("Y-m-d H:i:s") ) ) );
                    $booking->updated_at = date("Y-m-d H:i:s", strtotime ( '+1 hour' , strtotime ( date("Y-m-d H:i:s") ) ) );
                    $booking->save();
                    $this->formatLog('Reserva guardada en base de datos (no redsys) ' . $this->formatArrayToLog($_POST));
                } catch (Exception $e) {
                    $this->formatLog('Error al guardar reserva en base de datos: ' . $e->getMessage() . ' ' . $this->formatArrayToLog($_POST));
                    $dataEmail = [
                                'mensaje' => 'Hubo algún error al insertar en base de datos en pago sin redsys: ' . $e->getMessage(),
                                'nombre' => $input['nombre'],
                                'apellidos' => $input['apellidos'],
                                'personas' => $input['personas'],
                                'fecha' => $input['fecha'],
                                'horainicio' => $input['horainicio'],
                                'horafin' => $input['horafin'],
                                'telefono' => $input['telefono'],
                                'tipo' => $input['tipo'],
                                'pago' => $inputPago,
                                'email' => $input['email']
                    ];

                    $this->sendEmail(
                                  ['template' => 'intentoReserva', 'data' => $dataEmail], 
                                  ['email' => 'yalach@gmail.com', 'name' => 'Biciloca'],
                                  'Intento Reserva Biciloca'
                                );
                }

                // $headers = $this->getHeadersHttpResponse();
                //     $response = new Response(isset($input['cliente']), 200, $headers);
                //     return $response;

                $this->formatLog('Evento con cupon reservado con exito: ' . $this->formatArrayToLog($_POST));
                $dataEmail = [
                                'mensaje' => 'evento con cupon reservado con éxito',
                                'nombre' => $input['nombre'],
                                'apellidos' => $input['apellidos'],
                                'personas' => $input['personas'],
                                'fecha' => $input['fecha'],
                                'horainicio' => $input['horainicio'],
                                'horafin' => $input['horafin'],
                                'telefono' => $input['telefono'],
                                'tipo' => $input['tipo'],
                                'pago' => $inputPago,
                                'email' => $input['email']
                    ];

                    $this->sendEmail(
                                  ['template' => 'intentoReserva', 'data' => $dataEmail], 
                                  ['email' => 'yalach@gmail.com', 'name' => 'Biciloca'],
                                  'Intento Reserva Biciloca'
                                );
                
                $url = $this->formatUrlResponse($input['cliente'], 'success.php');
                if ($input['tipo'] == 'privado') 
                {
                    $urlCalendar = $this->formatUrlResponse($input['cliente'], 'barcelonaprivado.html');
                }
                else
                {
                    $urlCalendar = $this->formatUrlResponse($input['cliente'], 'barcelonapublico.html');
                }
                

                echo "<html lang='es'>
                            <head>
                            </head>
                            <body>
                                <form name='frm' action='" . $url . "' method='POST'>
                                    <input type='hidden' name='url' value='" . $urlCalendar . "'>
                                </form>
                                <script>document.forms[0].submit();</script>
                            </body>
                          </html>";
            }

            
        } 
        catch (Exception $e) 
        {
            $this->formatLog('Error en el proceso: ' . $e->getMessage() . ' ' . $this->formatArrayToLog($_POST));
            $dataEmail = [
                                'mensaje' => 'Hubo algún error en el proceso: ' . $e->getMessage(),
                                'nombre' => $input['nombre'],
                                'apellidos' => $input['apellidos'],
                                'personas' => $input['personas'],
                                'fecha' => $input['fecha'],
                                'horainicio' => $input['horainicio'],
                                'horafin' => $input['horafin'],
                                'telefono' => $input['telefono'],
                                'tipo' => $input['tipo'],
                                'pago' => $inputPago,
                                'email' => $input['email']
                    ];

                    $this->sendEmail(
                                  ['template' => 'intentoReserva', 'data' => $dataEmail], 
                                  ['email' => 'yalach@gmail.com', 'name' => 'Biciloca'],
                                  'Intento Reserva Biciloca'
                                );
            $url = $this->formatUrlResponse($input['cliente'], 'error.html');

                    echo "<html lang='es'>
                                <head>
                                </head>
                                <body>
                                    <form name='frm' action='" . $url . "' method='GET'>
                                        
                                    </form>
                                    <script>document.forms[0].submit();</script>
                                </body>
                              </html>";
        }
        $this->formatLog('');
        $this->formatLog('');
    }

    public function post_responseRedsys()
    {
        $this->formatLog('Entrada endpoint respuesta de redsys');
        $this->formatLog('idRedsys: ' . Session::get('idRedsys'));

        $redsys = new RedsysAPI;
        $version = $_POST["Ds_SignatureVersion"];
        $datos = $_POST["Ds_MerchantParameters"];
        $signatureRecibida = $_POST["Ds_Signature"];
        
        $decodec = $redsys->decodeMerchantParameters($datos);    
        $kc = KEY;
        $firma = $redsys->createMerchantSignatureNotif($kc, $datos);  

        $redsysResponse = json_decode($decodec);

        // $cache = Model_Cache::find('all', array(
        //     'where' => array(
        //         array('idRedsys', Session::get('idRedsys')),
        //     )
        // ));

        $cache = Model_Cache::find('last');

        $redsysModel = new Model_Redsys();
        $redsysModel->idRedsys = $cache->idRedsys;
        $redsysModel->name = $cache->nombre;
        $redsysModel->surname = $cache->apellidos;
        $redsysModel->type = $cache->tipo;
        $redsysModel->persons = $cache->personas;
        $redsysModel->date = $cache->fecha;
        $redsysModel->start_hour = $cache->horainicio;
        $redsysModel->end_hour = $cache->horafin;
        $redsysModel->phone = $cache->telefono;
        $redsysModel->order = $redsysResponse->Ds_Order;
        $redsysModel->email = $cache->email;
        $redsysModel->created_at = date("Y-m-d H:i:s", strtotime ( '+1 hour' , strtotime ( date("Y-m-d H:i:s") ) ) );
        $redsysModel->updated_at = date("Y-m-d H:i:s", strtotime ( '+1 hour' , strtotime ( date("Y-m-d H:i:s") ) ) );
        $redsysModel->precio = $cache->precio;
        $redsysModel->save();

        $this->formatLog('Datos de pago guardados en tabla redsys');

        try{
            $service = $this->getServiceCalendar();
            $calendars = $this->getCalendars();
            $headers = $this->getHeadersHttpResponse();

            if (!empty( $_POST ) ) 
            {                            
                $this->formatLog('Respuesta de redsys: ' . $this->formatArrayToLog($_POST));

                $this->formatLog('Código de respuesta de redsys: ' . $redsysResponse->Ds_Response);
            
                if ($firma === $signatureRecibida and $redsysResponse->Ds_Response == 0000)
                {
                    $this->formatLog('Numero de orden: ' .  $redsysResponse->Ds_Order);
                    $event = new Google_Service_Calendar_Event(array(
                      'summary' => $cache->data,
                      'description' => $cache->data . ' - transaction: ' . $redsysResponse->Ds_Order,
                      'start' => array(
                        'dateTime' => $cache->start,
                        'timeZone' => 'Europe/Madrid',
                      ),
                      'end' => array(
                        'dateTime' => $cache->end,
                        'timeZone' => 'Europe/Madrid',
                      ),
                    ));

                    // $this->formatLog('Session idCalendar debug:  ' .  Session::get('idCalendar') );
                    // $this->formatLog('Session debug:  ' .  $this->formatArrayToLog(Session::get()) );
                    // $this->formatLog('Evento debug:  ' .  $this->formatArrayToLog($event) );
                    
                    foreach ($calendars as $key => $calendar) 
                    {

                        $validateEvent = $calendar->getEvent($cache->fecha, $cache->horainicio, $cache->data);

                        if ( ! empty($validateEvent) )
                        {
                            echo "<html lang='es'>
                                            <head>
                                            </head>
                                            <body>
                                                <form name='frm' action='" . $urlError . "' method='GET'>
                                                    
                                                </form>
                                                <script>document.forms[0].submit();</script>
                                            </body>
                                          </html>"; 

                            exit;
                        }
                        
                    }

                    

                    try
                    {
                        $event = $service->events->insert($cache->idCalendar, $event);
                    }
                    catch (Exception $e)
                    {
                        $event = $service->events->insert( '4jahvm8kmedkk8b37on80vepjs@group.calendar.google.com', $event);

                        $dataEmail = [
                                        'mensaje' => 'Reserve successfully',
                                        'nombre' => Session::get('nombre'),
                                        'apellidos' => Session::get('apellidos'),
                                        'personas' => Session::get('personas'),
                                        'fecha' => Session::get('fecha'),
                                        'horainicio' => Session::get('horainicio'),
                                        'horafin' => Session::get('horafin'),
                                        'duracion' => Session::get('duracion'),
                                        'telefono' => Session::get('telefono'),
                                        'precio' => Session::get('precio') / 100,
                                        'email' => Session::get('email') . ' - transaction: ' . $redsysResponse->Ds_Order
                        ];

                        $this->sendMail(['email' => 'julio.perez@vanadis.es', 'name' => 'Julio'], ['data' => $dataEmail], 'reservaRealizada');

                    }

                    $this->formatLog('Evento en calendar que pasa por redsys - Numero de orden: ' .  $redsysResponse->Ds_Order);
                    $cache = Model_Cache::find('last');
                    $dataEmail = [
                                    'mensaje' => 'Reserve successfully',
                                    'nombre' => $cache->nombre,
                                    'apellidos' => $cache->apellidos,
                                    'personas' => $cache->personas,
                                    'fecha' => $cache->fecha,
                                    'horainicio' => $cache->horainicio,
                                    'horafin' => $cache->horafin,
                                    'duracion' => $cache->duracion,
                                    'telefono' => $cache->telefono,
                                    'precio' => $cache->precio / 100,
                                    'email' => $cache->email . ' - transaction: ' . $redsysResponse->Ds_Order
                    ];

                    $this->sendMail(['email' => $cache->email, 'name' => $cache->nombre . ' ' . $cache->apellidos], ['data' => $dataEmail], 'reservaRealizada');
                    $this->sendMail(['email' => 'diversion@biciloca.com', 'name' => 'Biciloca'], ['data' => $dataEmail], 'reservaRealizada');
                    $this->sendMail(['email' => 'yalach@gmail.com', 'name' => 'Julio'], ['data' => $dataEmail], 'reservaRealizada');
            
                    $coupon = Session::get('coupon');

                    // try {
                    //     $booking = new Model_Bookings();
                    //     $booking->name = Session::get('nombre');
                    //     $booking->surname = Session::get('apellidos');
                    //     $booking->type = Session::get('tipo');
                    //     $booking->persons = Session::get('personas');
                    //     $booking->date = Session::get('fecha');
                    //     $booking->start_hour = Session::get('horainicio');
                    //     $booking->end_hour = Session::get('horafin');
                    //     $booking->phone = Session::get('telefono');
                    //     $booking->price = $coupon->getPriceWithDiscount();
                    //     $booking->email = Session::get('email');
                    //     $booking->created_at = date("Y-m-d H:i:s", strtotime ( '+1 hour' , strtotime ( date("Y-m-d H:i:s") ) ) );
                    //     $booking->updated_at = date("Y-m-d H:i:s", strtotime ( '+1 hour' , strtotime ( date("Y-m-d H:i:s") ) ) );
                    //     $booking->save();
                    //     $this->formatLog('Reserva guardada en base de datos que pasa por redsys - Numero de orden: ' .  $redsysResponse->Ds_Order);

                    // } catch (Exception $e) {
                    //     $this->formatLog('Error al guardar reserva en base de datos que pasa por redsys: ' . $e->getMessage() . ' - Numero de orden: ' .  $redsysResponse->Ds_Order);
                    //     $dataEmail = [
                    //                 'mensaje' => 'Hubo un error al insertar en la base de datos la reserva: ' . $e->getMessage(),
                    //                 'nombre' => Session::get('nombre'),
                    //                 'apellidos' => Session::get('apellidos'),
                    //                 'personas' => Session::get('personas'),
                    //                 'fecha' => Session::get('fecha'),
                    //                 'horainicio' => Session::get('horainicio'),
                    //                 'horafin' => Session::get('horafin'),
                    //                 'telefono' => Session::get('telefono'),
                    //                 'tipo' => Session::get('tipo'),
                    //                 'pago' => Session::get('precio') / 100,
                    //                 'email' => Session::get('email')          
                    //     ];

                    //     $this->sendEmail(
                    //                   ['template' => 'intentoReserva', 'data' => $dataEmail], 
                    //                   ['email' => 'yalach@gmail.com', 'name' => 'Biciloca'],
                    //                   'Intento Reserva Biciloca'
                    //                 );
                    // }

                    $urlSuccess = $cache->urlSuccess;
                    echo "<html lang='es'>
                                    <head>
                                    </head>
                                    <body>
                                        <form name='frm' action='" . $urlSuccess . "' method='GET'>
                                            
                                        </form>
                                        <script>document.forms[0].submit();</script>
                                    </body>
                                  </html>"; 
                } 
                else 
                {
                    $this->formatLog('Error al hacer el pago con redsys: - Numero de orden: ' .  $redsysResponse->Ds_Response);
                    // $dataEmail = [
                    //                 'mensaje' => 'Hubo un error al hacer el pago con redsys',
                    //                 'nombre' => Session::get('nombre'),
                    //                 'apellidos' => Session::get('apellidos'),
                    //                 'personas' => Session::get('personas'),
                    //                 'fecha' => Session::get('fecha'),
                    //                 'horainicio' => Session::get('horainicio'),
                    //                 'horafin' => Session::get('horafin'),
                    //                 'telefono' => Session::get('telefono'),
                    //                 'tipo' => Session::get('tipo'),
                    //                 'pago' => Session::get('precio') / 100,
                    //                 'email' => Session::get('email') . ' - transaction: ' . $redsysResponse->Ds_Order 
                    //     ];

                    //     $this->sendEmail(
                    //                   ['template' => 'intentoReserva', 'data' => $dataEmail], 
                    //                   ['email' => 'yalach@gmail.com', 'name' => 'Biciloca'],
                    //                   'Intento Reserva Biciloca'
                    //                 );

                    //     $this->sendEmail(
                    //                   ['template' => 'intentoReserva', 'data' => $dataEmail], 
                    //                   ['email' => 'diversion@biciloca.com', 'name' => 'Biciloca'],
                    //                   'Intento Reserva Biciloca'
                    //                 );

                        $urlError = $cache->urlError;
                        echo "<html lang='es'>
                                        <head>
                                        </head>
                                        <body>
                                            <form name='frm' action='" . $urlError . "' method='GET'>
                                                
                                            </form>
                                            <script>document.forms[0].submit();</script>
                                        </body>
                                      </html>"; 
                    
                }
            }
            else
            {
                // $this->formatLog('La respuesta redsys no paso parametros GET - email: ' . Session::get('email'));
                // $dataEmail = [
                //                     'mensaje' => 'La respuesta de redsys no paso parametros GET',
                //                     'nombre' => Session::get('nombre'),
                //                     'apellidos' => Session::get('apellidos'),
                //                     'personas' => Session::get('personas'),
                //                     'fecha' => Session::get('fecha'),
                //                     'horainicio' => Session::get('horainicio'),
                //                     'horafin' => Session::get('horafin'),
                //                     'telefono' => Session::get('telefono'),
                //                     'tipo' => Session::get('tipo'),
                //                     'pago' => Session::get('precio') / 100,
                //                     'email' => Session::get('email')          
                //         ];

                //         $this->sendEmail(
                //                       ['template' => 'intentoReserva', 'data' => $dataEmail], 
                //                       ['email' => 'yalach@gmail.com', 'name' => 'Biciloca'],
                //                       'Intento Reserva Biciloca'
                //                     );

                $urlSuccess = $cache->urlSuccessPosible;
                echo "<html lang='es'>
                                <head>
                                </head>
                                <body>
                                    <form name='frm' action='" . $urlSuccess . "' method='GET'>
                                        
                                    </form>
                                    <script>document.forms[0].submit();</script>
                                </body>
                              </html>";
            }

        }catch (Exception $e) {
            $this->formatLog('Error en la respuesta de redsys: ' . $e->getMessage());
            $dataEmail = [
                                    'mensaje' => 'Error al recibir la respuesta en redsys: ' . $e->getMessage()        
                        ];

                        $this->sendEmail(
                                      ['template' => 'errores', 'data' => $dataEmail], 
                                      ['email' => 'yalach@gmail.com', 'name' => 'Biciloca'],
                                      'Errores Reserva Biciloca'
                                    );
        }
        
    }

    private function getHeadersHttpResponse()
    {
        return [
                'Cache-Control'     => 'no-cache, no-store, max-age=0, must-revalidate',
                'Pragma'            => 'no-cache',
                'Access-Control-Allow-Origin'  => ACCESS_CONTROL_ALLOW_ORIGIN
               ];
    }

    private function getNumberBicyclesDisponibles($fecha)
    {
        $numberBicycles = Model_Bicycles::find('all', ['where' => ['day' => $fecha]]);

        if (empty($numberBicycles)) 
        {
            $numberBicycles = 3;
        }
        else
        {
            foreach ($numberBicycles as $key => $number) 
            {
                $numberBicycles = $number['numberBicycles'];
            }
        }

        return $numberBicycles;
    }

    private function getIntervalTime($startTime, $endTime)
    {
        $interval = [];
        $isFinish = false;

        $interval[] = $startTime;
        $next30min = $this->addTime($startTime, 30);
        $interval[] = $next30min;

        while( ! $isFinish)
        {
            $next30min = $this->addTime($next30min, 30);
            $interval [] = $next30min;
            if ($next30min == $endTime) 
            { 
                $isFinish = true;
            }
           
        }

        return $interval;
    }

    private function addTime( $time, $addMinutes ) 
    { 
       $timeToSeconds = strtotime($time);
       $addMinutesToSeconds = $addMinutes * 60;
       $newTime = date("H:i", $timeToSeconds + $addMinutesToSeconds);
       return $newTime;
    }
}
