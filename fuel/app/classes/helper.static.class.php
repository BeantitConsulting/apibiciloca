<?php  
namespace Biciloca;

class Helper
{
    
    public static function dd($variable)
    {
        var_dump($variable);
        exit;
    }

    public static function tracer($var, $method = null)
    {
        Log::warning("Data" . $var . "Método:" . $method);
    }

    public static function diferenceHours($startHour, $endHour)
    {
        $startHourSeconds = strtotime($startHour);
        $endHourSeconds = strtotime($endHour);

        $diferenceSeconds = $endHourSeconds - $startHourSeconds;
        return $diferenceSeconds / 60;
    }

    public static function reverseDate($date)
    {
        $arrayDate = explode('-', $date);
        $date = $arrayDate[2] . '-' . $arrayDate[1] . '-' . $arrayDate[0];
        return $date;
    }

    public static function addTime( $time, $addMinutes ) 
    { 
       $timeToSeconds = strtotime($time);
       $addMinutesToSeconds = $addMinutes * 60;
       $newTime = date("H:i", $timeToSeconds + $addMinutesToSeconds);
       return $newTime;
    }
}