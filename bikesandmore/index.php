<!DOCTYPE HTML>
<!--
	Dimension by HTML5 UP
	html5up.net | @ajlkn
	Free for personal and commercial use under the CCA 3.0 license (html5up.net/license)
-->
<html>
	<head>
		<title>Bikes And More</title>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
		
    	<link rel="stylesheet" type="text/css" href="assets/css/normalize.css">
    	<link rel="stylesheet" type="text/css" href="assets/css/foundation.min.css">
		<link rel="stylesheet" type="text/css" href="assets/css/main.css" />
		<!--[if lte IE 9]><link rel="stylesheet" href="assets/css/ie9.css" /><![endif]-->
		<noscript><link rel="stylesheet" href="assets/css/noscript.css" /></noscript>
	</head>
	<body>
			
		<!-- Wrapper -->
			<div id="wrapper">

				<!-- Header -->
					<header id="header">
						<div class="logo">
							<span class="icon fa-diamond"></span>
						</div>
						<div class="content">
							<div class="inner">
								<h1>Bikes And More</h1>
								<p>Un espacio donde alquilar todo lo que necesites</p>
							</div>
						</div>
						<nav>
							<ul>
								<li><a href="downhill.php">Downhill</a></li>
								<li><a href="ski.php">Ski</a></li>
								<li><a href="nosotros.php">Nosotros</a></li>
								<li><a href="#contacto">Contacto</a></li>
								<!--<li><a href="#elements">Elements</a></li>-->
							</ul>
						</nav>
					</header>

					<?php
			            $ids = array("1", "2", "3", "4", "5");
			            $marca = array("commencal","commencal","trek","PROTECTOR DE MTB DAINESE MANIS JACKET", "sixsixone");
			            $images = array("images/commencal-dh-v3.jpg","images/commencal-dh-v4.jpg","images/trek.jpg","images/protector.jpg", "images/sixsixone.jpg");
			            $descripcion = array("<p>Modelo: DH V3</p>
			                                <p>PESO      18,3 kg</p>
			                                <p>RECORRIDO DELANTERO   200mm</p>
			                                <p>RECORRIDO TRASERO     190mm</p>
			                                <p>RUEDAS    650B</p>",
			                                "<p>Modelo: DH V4</p>
			                                <p>PESO      17,95 kg</p>
			                                <p>PESO DEL CUADRO   4,3 kg</p>
			                                <p>RECORRIDO DELANTERO   200mm</p>
			                                <p>RECORRIDO TRASERO     220mm</p>
			                                <p>RUEDAS    650B</p>",
			                                "<p>Modelo: Session 8 27.5</p>
			                                <p>PESO      17,42 kg</p>
			                                <p>PESO DEL CUADRO   4,3 kg</p>
			                                <p>RECORRIDO DELANTERO   200mm</p>
			                                <p>RECORRIDO TRASERO     200mm</p>
			                                <p>RUEDAS    27,5x2,35”</p>",
			                                "<p>El protector de MTB Dainese Manis Jacket ha sido ideado para las situaciones más extremas y garantiza una protección elevada en la parte superior del cuerpo. 
			                                Protector de espalda extraíble, se puede utilizar por separado.El protector de MTB Dainese Manis Jacket ha sido ideado para las situaciones más extremas y garantiza una protección elevada en la parte superior del cuerpo. </p>
			                                <p>Tallas disponibles: M, L, XL</p>",
			                                "<p>Protección contra impactos ligera y ventilada a través de la espuma de EVA
			                                    La mejor protección de hombros y codos con carcasas de plástico
			                                    Facilidad de movilidad articular con protección de columna y certificado CE
			                                    Refrigeración del core con la construcción de malla ventilada
			                                    Ajuste seguro y fino de las correas y cierres regulables</p>
			                                <p>Tallas disponibles: M, L, XL</p>
			                                <p>PESO DEL CUADRO   4,3 kg</p>");
			            $precios = array("100","200","300","400");
			            $nombres = array("commencal","commencal","trek","PROTECTOR DE MTB DAINESE MANIS JACKET", "sixsixone");
			        ?>

				<!-- Main -->
					<div id="main">
							<!-- Bicicletas -->
							<article id="bicicletas">
								<div class="large-12 columns">
						            <div class="titulo"></div>
						            <table class="content_cart panel radius large-12"></table>
						        </div>
								<h2 class="major">Bicicletas</h2>

								<p>Las bicicletas de descenso han sido diseñadas, tanto para arrasar en los circuitos de descenso de la Copa del Mundo, como para volar en las pistas de enduro más complicadas o en los bike parks. Las exclusivas tecnologías, desde la suspensión increíblemente avanzada hasta el cuadro, te permiten ajustar la geometría para lograr un mejor control, convirtiendo a esta bicicleta de descenso en la máquina más ligera y resistente de la montaña.</p>

								<hr>

								<?php for($i=0; $i < count($ids); $i++){ ?>
						            <form class="add" method="POST" action="process_cart/insert.php" accept-charset="UTF-8">
				                    	<h4>Marca: <?php echo  $marca[$i] ?></h4>
				                    	<input name="nombre" type="hidden" value="<?php echo $marca[$i] ?>" id="nombre">
				                    	<input name="id" type="hidden" value="<?php echo $ids[$i] ?>">
										<span class="image main"><img src="<?php echo  $images[$i] ?>" alt="" /></span>
										<input name="cantidad" type="hidden" value="1">
										<?php echo $descripcion[$i] ?>
										<?php echo $precios[$i] ?> <br><br>
										<input name="precio" type="hidden" value="<?php echo $precios[$i] ?>" id="precio">
										<input type="submit" class="button large-12" value="Alquilar">

										<hr>
								    </form>
						        <?php } ?>
							</article>

							<!-- Ski -->
							<article id="ski">
								<h2 class="major">Ski</h2>

								<h4>Marca: SLX</h4>
								<span class="image main"><img src="images/elan.jpg" alt="" /></span>
								
								<p>el SLX Fusion se distingue una vez más por una excelente accesibilidad y eficacia.
									Recomendamos este esquí tanto a los esquiadores de nivel medio a bueno que desean iniciarse en la "performance" como a los esquiadores de nivel bueno y excelente que buscan un modelo deportivo, pero menos exigente técnica y físicamente.</p>
								<p>Preguntar Tallas disponibles antes de hacer la reserva</p>
								<p>Precio: 32,40€/dia</p>

								<hr>

								<h4>Marca: IKONIC</h4>
								<span class="image main"><img src="images/k2.jpg" alt="" /></span>
								
								<p>El IKONIC 80 de esquí cuenta con un doble de madera de construcción redistribuido K2 Konic con Aspen ligero y animado en su centro; mientras que tiene una más densa y más potente de abeto sobre los bordes. La intuitiva Konic Tecnología K2 y todo el diseño de la montaña del IKONIC 80 hace que el esquí en las estaciones de todo el día notablemente más agradable con un rendimiento inmejorable, independientemente de las condiciones o del terreno.</p>
								<p>Preguntar Tallas disponibles antes de hacer la reserva</p>
								<p>Precio: 27,54€/dia</p>

								<hr>

								<h4>Marca: Rossignol</h4>
								<span class="image main"><img src="images/rossignol27.jpg" alt="" /></span>
								
								<p>Modelo: HERO 27</p>
								<p>El nuevo HERO MASTER FACTORY es un arma de competición Masters GS súper cargada. Con la auténtica construcción World Cup race room, le nuevo «Factory» Masters sólo está disponible en 189cm (27m radio) para los apasionados de la velocidad más entregados.</p>
								<p>Preguntar Tallas disponibles antes de hacer la reserva</p>
								<p>Precio: 25€/dia</p>

								<hr>

								<h4>Marca: Rossignol</h4>
								<span class="image main"><img src="images/rossignol90.jpg" alt="" /></span>
								
								<p>Modelo: Alltrack 90</p>
								<p>Botín Medium.</p>
								<p>Botín OPTISENSOR: la construcción favorece la interacción carcasa/botín. El diseño incorpora una zona de envolvimiento en el empeine y una zona de sujeción en el talón. Excelente aislamiento térmico del botín gracias a la utilización de la THINSULATE® y de la THINSULATE PLATINUM®. Fibras técnicas de la sociedad 3M, aseguran un excelente aislamiento térmico.</p>
								<p>Preguntar Tallas disponibles antes de hacer la reserva</p>
								<p>Precio: 25€/dia</p>

								<hr>

								<h4>Marca: Tecnica Cochise</h4>
								<span class="image main"><img src="images/tecnica.jpg" alt="" /></span>
								
								<p>Tecnología IFS Tecnología IFS (intelligent freemountain système), una tecnología con brida desactivable, suela intercambiable , carcasa triax, concepto 3 hebillas + velcro ancho powwer lock. Posición esquí / marcha Posición esquí / marcha para ofrecer una comodidad óptima en posición marcha y precisión en posición esquí, basta con tirar de una lengüeta para cambiar de posición.</p>
								<p>Preguntar Tallas disponibles antes de hacer la reserva</p>
								<p>Precio: 21,25€/dia</p>

							</article>

							<!-- Contacto -->
							<article id="contacto">
								<h2 class="major">Contacto</h2>
								<p>Para cualquier duda:</p>
								<p>email: joaquin.t.a.89@gmail.com</p>
								<p>Teléfono: +376 369 835</p>
								<p>Estamos ubicados en Encamp</p>
								<p>Ubicación perfecta para acceso tanto a Grandvalira como Vallnord</p>
							</article>
					</div>

				<!-- Footer -->
					<footer id="footer">
						<p class="copyright">&copy; Todos los derechos reservados</p>
					</footer>

			</div>

		<!-- BG -->
			<div id="bg"></div>

		<!-- Scripts -->
			<script src="assets/js/jquery.min.js"></script>
			<script src="assets/js/skel.min.js"></script>
			<script src="assets/js/util.js"></script>
			<script src="assets/js/main.js"></script>
			<script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
    		<script type="text/javascript" src="assets/js/functions.js"></script>

	</body>
</html>
