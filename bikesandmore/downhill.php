<!DOCTYPE HTML>
<!--
    Dimension by HTML5 UP
    html5up.net | @ajlkn
    Free for personal and commercial use under the CCA 3.0 license (html5up.net/license)
-->
<html>
    <head>
        <title>Bikes And More</title>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
        
        <link rel="stylesheet" type="text/css" href="assets/css/normalize.css">
        <link rel="stylesheet" type="text/css" href="assets/css/foundation.min.css">
        <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
        <link rel="stylesheet" type="text/css" href="assets/css/main.css" />
        <link rel="stylesheet" type="text/css" href="assets/css/estilos.css" />
        <!--[if lte IE 9]><link rel="stylesheet" href="assets/css/ie9.css" /><![endif]-->
        <noscript><link rel="stylesheet" href="assets/css/noscript.css" /></noscript>
    </head>
    <body>
            
        <!-- Wrapper -->
            <div id="wrapper">

            <div class="carrito">
                <p>
                    
                        <span class="menu-element"><a href="index.php">Home</a></span>
                        <span class="menu-element"><a href="downhill.php">Downhill</a></span>
                        <span class="menu-element"><a href="ski.php">Ski</a></span><br>
                        <span class="menu-element"><a href="carrito.php">Carrito</a></span>
                        <span class="menu-element"><a href="/index.php#contacto">Contacto</a></span>
                        <!--<li><a href="#elements">Elements</a></li>-->
                    
                </p>
               <!--  <div class="titulo"></div>
                <table id='carrito' class="content_cart panel radius large-12"></table> -->
            </div>

                    <?php
                        $ids = array("1", "2", "3", "4", "5");
                        $marca = array("commencal","commencal","trek","MTB DAINESE", "sixsixone");
                        $images = array("images/commencal-dh-v3.jpg","images/commencal-dh-v4.jpg","images/trek.jpg","images/protector.jpg", "images/sixsixone.jpg");
                        $descripcion = array("<p>Modelo: DH V3</p>
                                            <p>Peso     18,3 kg</p>
                                            <p>Recorrido Delantero   200mm</p>
                                            <p>Recorrido Trasero    190mm</p>
                                            <p>Ruedas    650B</p>",
                                            "<p>Modelo: DH V4</p>
                                            <p>Peso      17,95 kg</p>
                                            <p>Recorrido Delantero   200mm</p>
                                            <p>Recorrido Trasero     220mm</p>
                                            <p>Ruedas    650B</p>",
                                            "<p>Modelo: Session 8 27.5</p>
                                            <p>Peso      17,42 kg</p>
                                            <p>Recorrido Delantero   200mm</p>
                                            <p>Recorrido Trasero     200mm</p>
                                            <p>Ruedas    27,5x2,35”</p>",
                                            "<p>El protector de MTB Dainese Manis Jacket ha sido ideado para las situaciones más extremas y garantiza una protección elevada en la parte superior del cuerpo. 
                                            Protector de espalda extraíble, se puede utilizar por separado.El protector de MTB Dainese Manis Jacket ha sido ideado para las situaciones más extremas.</p>
                                            <p>Tallas disponibles: M, L, XL</p>",
                                            "<p>Protección contra impactos ligera y ventilada a través de la espuma de EVA
                                                La mejor protección de hombros y codos con carcasas de plástico
                                                Facilidad de movilidad articular con protección de columna y certificado CE
                                                Refrigeración del core con la construcción de malla ventilada
                                                Ajuste seguro y fino de las correas y cierres regulables</p>
                                            <p>Tallas disponibles: M, L, XL</p>");
                        $precios = array("32.40","41.40","27.54","25", "21.25");
                        $nombres = array("commencal","commencal","trek","PROTECTOR DE MTB DAINESE MANIS JACKET", "sixsixone");
                    ?>

                <!-- Main -->
                    <div id="">
                            <!-- Bicicletas -->
                            <article id="bicicletas" class='product-container'>
                                
                                <h2 class="major">Downhill</h2>

                                <p>Las bicicletas de descenso han sido diseñadas, tanto para arrasar en los circuitos de descenso de la Copa del Mundo, como para volar en las pistas de enduro más complicadas o en los bike parks. Las exclusivas tecnologías, desde la suspensión increíblemente avanzada hasta el cuadro, te permiten ajustar la geometría para lograr un mejor control, convirtiendo a esta bicicleta de descenso en la máquina más ligera y resistente de la montaña.</p>

                                <hr>
                                <div class=''>
                                    <?php for($i=0; $i < count($ids); $i++){ ?>
                                        <form class="add" method="POST" action="process_cart/insert.php" accept-charset="UTF-8">
                                            <div class="col-sm-3 producto">
                                                <h4>Marca: <?php echo  $marca[$i] ?></h4>
                                                <input name="nombre" type="hidden" value="<?php echo $marca[$i] ?>" id="nombre">
                                                <input name="id" type="hidden" value="<?php echo $ids[$i] ?>">
                                                <span class="image main"><img src="<?php echo  $images[$i] ?>" alt="" /></span>
                                                <input name="cantidad" type="hidden" value="1">
                                                <p class=''><?php echo $descripcion[$i] ?></p>
                                                <?php echo $precios[$i] ?> €/día<br><br>
                                                <input name="precio" type="hidden" value="<?php echo $precios[$i] ?>" id="precio">
                                                <input type="submit" class="button large-12 alquilar" value="Alquilar">
                                            </div>
                                        </form>
                                    <?php } ?>
                                </div>
                            </article>

                            
                    </div>

            </div>

        <!-- BG -->
            <div id="bg"></div>

        <!-- Scripts -->
            <script src="assets/js/jquery.min.js"></script>
            <script src="assets/js/skel.min.js"></script>
            <script src="assets/js/util.js"></script>
            <script src="assets/js/main.js"></script>
            <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
            <script type="text/javascript" src="assets/js/functions.js"></script>

    </body>
</html>
