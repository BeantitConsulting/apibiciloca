<!DOCTYPE HTML>
<!--
    Dimension by HTML5 UP
    html5up.net | @ajlkn
    Free for personal and commercial use under the CCA 3.0 license (html5up.net/license)
-->
<html>
    <head>
        <title>Deportiva</title>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
        
        <link rel="stylesheet" type="text/css" href="assets/css/normalize.css">
        <link rel="stylesheet" type="text/css" href="assets/css/foundation.min.css">
        <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
        <link rel="stylesheet" type="text/css" href="assets/css/main.css" />
        <link rel="stylesheet" type="text/css" href="assets/css/estilos.css" />
        <!--[if lte IE 9]><link rel="stylesheet" href="assets/css/ie9.css" /><![endif]-->
        <noscript><link rel="stylesheet" href="assets/css/noscript.css" /></noscript>
    </head>
    <body>
            
        <!-- Wrapper -->
            <div id="wrapper">

             <div class="carrito">
                <p>
                    
                        <span class="menu-element"><a href="index.php">Home</a></span>
                        <span class="menu-element"><a href="downhill.php">Downhill</a></span>
                        <span class="menu-element"><a href="ski.php">Ski</a></span>
                        <!--<li><a href="#elements">Elements</a></li>-->
                    
                </p>
                <div class="titulo"></div>
                <table id='carrito' class="content_cart panel radius large-12"></table>
            </div>

                    <?php
                        $ids = array("6", "7", "8", "9", "10");
                        $marca = array("SLX","IKONIC","Rossignol","Rossignol", "tecnica cosiche");
                        $images = array("images/elan.jpg","images/k2.jpg","images/rossignol27.jpg","images/rossignol90.jpg", "images/tecnica.jpg");
                        $descripcion = array("<p>El SLX Fusion se distingue una vez más por una excelente accesibilidad y eficacia.
                                    Recomendamos este esquí tanto a los esquiadores de nivel medio a bueno que desean iniciarse en la performance como a los esquiadores de nivel bueno y excelente que buscan un modelo deportivo.</p>
                                <p>Preguntar Tallas disponibles antes de hacer la reserva</p>",
                                            "<p>El IKONIC 80 de esquí cuenta con un doble de madera de construcción redistribuido K2 Konic con Aspen ligero y animado en su centro. La intuitiva Konic Tecnología K2 y todo el diseño de la montaña del IKONIC 80 hace que el esquí en las estaciones de todo el día notablemente más agradable.</p>
                                <p>Preguntar Tallas disponibles antes de hacer la reserva</p>",
                                            "<p>El nuevo HERO MASTER FACTORY es un arma de competición Masters GS súper cargada. Con la auténtica construcción World Cup race room, le nuevo «Factory» Masters sólo está disponible en 189cm (27m radio) para los apasionados de la velocidad más entregados.</p>
                                <p>Preguntar Tallas disponibles antes de hacer la reserva</p>",
                                            "<p>Botín OPTISENSOR: la construcción favorece la interacción carcasa/botín. El diseño incorpora una zona de envolvimiento en el empeine y una zona de sujeción en el talón. Excelente aislamiento térmico del botín gracias a la utilización de la THINSULATE® y de la THINSULATE PLATINUM®. Fibras técnicas de la sociedad 3M, aseguran un excelente aislamiento térmico.</p>
                                <p>Preguntar Tallas disponibles antes de hacer la reserva</p>",
                                            "<p>Tecnología IFS Tecnología IFS (intelligent freemountain système), una tecnología con brida desactivable, suela intercambiable , carcasa triax, concepto 3 hebillas + velcro ancho powwer lock. Posición esquí / marcha Posición esquí / marcha para ofrecer una comodidad óptima en posición marcha y precisión en posición esquí, basta con tirar de una lengüeta para cambiar de posición.</p>
                                <p>Preguntar Tallas disponibles antes de hacer la reserva</p>");
                        $precios = array("32.40","27.54","25","25", "21.25");
                        $nombres = array("commencal","commencal","trek","PROTECTOR DE MTB DAINESE MANIS JACKET", "sixsixone");
                    ?>

                <!-- Main -->
                    <div id="">
                            <!-- Bicicletas -->
                            <article id="bicicletas" class='product-container'>
                                <h2 class="major" >Ski</h2>

                                <?php for($i=0; $i < count($ids); $i++){ ?>
                                    <form class="add" method="POST" action="process_cart/insert.php" accept-charset="UTF-8">
                                        <div class="col-sm-3 producto">
                                            <h4>Marca: <?php echo  $marca[$i] ?></h4>
                                            <input name="nombre" type="hidden" value="<?php echo $marca[$i] ?>" id="nombre">
                                            <input name="id" type="hidden" value="<?php echo $ids[$i] ?>">
                                            <span class="image main"><img src="<?php echo  $images[$i] ?>" alt="" /></span>
                                            <input name="cantidad" type="hidden" value="1">
                                            <?php echo $descripcion[$i] ?>
                                            <?php echo $precios[$i] ?> €/día <br><br>
                                            <input name="precio" type="hidden" value="<?php echo $precios[$i] ?>" id="precio">
                                            <input type="submit" class="button large-12" value="Alquilar">
                                        </div>
                                    </form>
                                <?php } ?>
                            </article>

                            
                    </div>

            </div>

        <!-- BG -->
            <div id="bg"></div>

        <!-- Scripts -->
            <script src="assets/js/jquery.min.js"></script>
            <script src="assets/js/skel.min.js"></script>
            <script src="assets/js/util.js"></script>
            <script src="assets/js/main.js"></script>
            <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
            <script type="text/javascript" src="assets/js/functions.js"></script>

    </body>
</html>
