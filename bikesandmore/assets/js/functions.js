//al cargar la página mostramos el contenido del carrito o un mensaje
//si no tiene contenido, como veremos vamos llamando a esta función
//en cada proceso ya que es la encargada de actualizar el carrito
function content_cart()
{
    $.get("process_cart/content_cart.php", function(data){
        var json = JSON.parse(data);
        var html = "";
        if(json.res == "ok")
        {
            $(".titulo").html("<p class='title-carrito'>Contenido del carrito | <span id='abrir'>ABRIR</span> | <span id='cerrar'>CERRAR</span></p>");
            html += "<thead style='text-align:left'>";
            html += "<tr>";
            //html += "<th width='20'>Id</th>";
            html += "<th width='150'>Nombre</th>";
            html += "<th width='100'>Precio</th>";
            html += "<th width='25'>Cant</th>";
            html += "<th width='50'>Total</th>";
            html += "<th width='150'>Eliminar</th>";
            html += "</tr>";
            html += "</thead>";
            html += "<tbody>";
            for(datos in json.content)
            {
                html += "<tr>";
                //html += "<td>" + json.content[datos].id + "</td>";
                html += "<td>" + json.content[datos].nombre + "</td>";
                html += "<td>" + json.content[datos].precio + "</td>";
                html += "<td>" + json.content[datos].cantidad + "</td>";
                html += "<td>" + json.content[datos].total + "</td>";
                html += "<td><a href='#' onclick=\"eliminar('" + json.content[datos].unique_id + "')\">Eliminar</a>";
                html += "</tr>";
            }
            html += "<tr>";
            html += "<td colspan='4'><b>Precio total: " + json.precio_total + "</b></td>";
            html += "<td colspan='3'><b>Total artículos: " + json.articulos_total + "</b></td>";
            html += "</tr>";
            html += "</tbody>";
        }else{
            $(".titulo").html("");
            

            html += "<p class='title-carrito'> El carrito está vacío </p>";
        }
        $(".content_cart").html("");
        $(".content_cart").append(html);
    });
}
 
//añadimos un nuevo producto al carrito 
$(document).ready(function(){
    $(".add").on("submit", function(e){
        e.preventDefault();
        $.ajax({
            type: $(this).attr("method"),
            url: $(this).attr("action"),
            data: $(this).serialize(),
            beforeSend: function()
            {
 
            },
            success: function(data)
            {
                var json = JSON.parse(data);

                //si el proceso es correcto llamamos a la función
                //content_cart() y se actualizará el carrito
                if(json.res == "ok")
                {
                    content_cart();
                }else{
                    alert(json.message);
                }   
            },
            error: function()
            {
                alert("Error");
            }
        })
    });

    $(".alquilar").click(function(){
        alert('producto añadido al carrito. Revisa el carrito en el menú principal');
    });
});
 
 
//función para vaciar el contenido del carrito
$(document).on("click", ".destroy", function(e){
    e.preventDefault();
    $.post("process_cart/destroy.php", function(){
        //llamamos a la función content_cart() para actualizar el carrito
        content_cart();
    });
})


$(document).on("click", "#cerrar", function(){
    $( "#carrito" ).hide();
})

$(document).on("click", "#abrir", function(){
    $( "#carrito" ).show();
})

$(document).on("click", "input[type=submit]", function(){
    $( "#carrito" ).show();
})
 
 
//función para eliminar una fila
function eliminar(unique_id)
{
    $.post("process_cart/remove_product.php",{unique_id : unique_id}, function(data){
        var json = JSON.parse(data);
        if(json.res == "ok")
        {
            //llamamos a la función content_cart() para actualizar el carrito
            content_cart();
        }  
    });

    location.href = 'http://www.bikesandmoreandorra.com/carrito.php';
}

function increment(unique_id)
{
    $.post("process_cart/increment_product.php",{id : unique_id}, function(data){
        var json = JSON.parse(data);
        if(json.res == "ok")
        {
            //llamamos a la función content_cart() para actualizar el carrito
            content_cart();
        }
    });

    location.href = 'http://www.bikesandmoreandorra.com/carrito.php';
}

function decrement(unique_id)
{
    $.post("process_cart/decrement_product.php",{id : unique_id}, function(data){
        var json = JSON.parse(data);
        if(json.res == "ok")
        {
            //llamamos a la función content_cart() para actualizar el carrito
            content_cart();
        }
    });

    location.href = 'http://www.bikesandmoreandorra.com/carrito.php';
}
 
 
 
//el abrir la página ya mostramos el contenido del carrito
$(window).on("load", function(){
    content_cart();
});

