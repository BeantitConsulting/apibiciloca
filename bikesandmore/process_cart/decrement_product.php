<?php

if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') 
{
    require("../Carrito.class.php");
    $carrito = new Carrito();
    $unique_id = $_POST["id"];
    $carrito->decrement_product($unique_id);
    echo json_encode(array("res" => "ok")); 
}