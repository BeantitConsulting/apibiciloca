<!DOCTYPE HTML>
<!--
    Dimension by HTML5 UP
    html5up.net | @ajlkn
    Free for personal and commercial use under the CCA 3.0 license (html5up.net/license)
-->
<html>
    <head>
        <title>Deportiva</title>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
        
        <link rel="stylesheet" type="text/css" href="assets/css/normalize.css">
        <link rel="stylesheet" type="text/css" href="assets/css/foundation.min.css">
        <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
        <link rel="stylesheet" type="text/css" href="assets/css/main.css" />
        <link rel="stylesheet" type="text/css" href="assets/css/estilos.css" />
        <!--[if lte IE 9]><link rel="stylesheet" href="assets/css/ie9.css" /><![endif]-->
        <noscript><link rel="stylesheet" href="assets/css/noscript.css" /></noscript>

        <style type="text/css">
            .proximamente{
                background-color: black;
                color: white;
                padding: 50px 200px 50px 200px;
                border-radius: 4px;
                font-size: 50px;
            }
        </style>
    </head>
    <body>
            
        <!-- Wrapper -->
            <div id="wrapper">

             <div class="carrito">
                <p>
                    <span class="menu-element"><a href="index.php">Home</a></span>
                    <span class="menu-element"><a href="downhill.php">Downhill</a></span>
                    <span class="menu-element"><a href="ski.php">Ski</a></span><br>
                    <span class="menu-element"><a href="carrito.php">Carrito</a></span>
                    <span class="menu-element"><a href="/index.php#contacto">Contacto</a></span>
                    <!--<li><a href="#elements">Elements</a></li>-->
                </p>
                
                
            </div>
                <div class='proximamente'>   
                    <p>PRÓXIMAMENTE</p>    
                </div>
            </div>

        <!-- BG -->
            <div id="bg"></div>

        <!-- Scripts -->
            <script src="assets/js/jquery.min.js"></script>
            <script src="assets/js/skel.min.js"></script>
            <script src="assets/js/util.js"></script>
            <script src="assets/js/main.js"></script>
            <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
            <script type="text/javascript" src="assets/js/functions.js"></script>

    </body>
</html>
