<?php session_start() ?>
<pre>
<?php // var_dump($_SESSION['carrito']); exit; ?>
</pre>
<!DOCTYPE HTML>
<html>
    <head>
        <title>Bikes And More</title>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        
        <link rel="stylesheet" type="text/css" href="assets/css/normalize.css">
        <link rel="stylesheet" type="text/css" href="assets/css/foundation.min.css">
        <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
        <link rel="stylesheet" type="text/css" href="assets/css/main.css" />
        <link rel="stylesheet" type="text/css" href="assets/css/estilos.css" />
        <!--[if lte IE 9]><link rel="stylesheet" href="assets/css/ie9.css" /><![endif]-->
        <noscript><link rel="stylesheet" href="assets/css/noscript.css" /></noscript>
    </head>
    <body>
        <div id="wrapper" class='wrapper-reset'>
            <div class="carrito">
                <p>
                    <span class="menu-element"><a href="index.php">Home</a></span>
                    <span class="menu-element"><a href="downhill.php">Downhill</a></span>
                    <span class="menu-element"><a href="ski.php">Ski</a></span><br>
                    <span class="menu-element"><a href="carrito.php">Carrito</a></span>
                    <span class="menu-element"><a href="/index.php#contacto">Contacto</a></span>
                </p>
            </div>

            <div class="container margin-page-carrito">
                <div class="col-md-6">
                    <div class="col-md-12">
                        <div class="panel panel-default panel-table">
                          <div class="panel-heading">
                            <div class="row">
                              <div class="col col-xs-6">
                                <h3 class="panel-title">Carrito</h3>
                              </div>
                            </div>
                          </div>
                          <div class="panel-body">
                            <table class="table table-striped table-bordered table-list">
                              <thead>
                                  <tr>
                                      <th></th>
                                      <th>Nombre</th>
                                      <th>Precio</th>
                                      <th>Cantidad</th>
                                      <th>Total</th>
                                  </tr> 
                              </thead>
                              <tbody>
                                <?php foreach ($_SESSION['carrito'] as $key => $value) { ?>
                                  <?php if ($key != 'articulos_total' and $key != 'precio_total') { ?>
                                      <tr>
                                        <td align="center">
                                          <a class="btn btn-danger" onclick="eliminar(<?php echo  $value['id']?>)"><em class="fa fa-trash"></em></a>
                                        </td>
                                        <td><?php echo $value['nombre'] ?></td>
                                        <td><?php echo $value['precio'] ?> €</td>
                                        <td><a class="btn btn-primary btn-xs" onclick="decrement(<?php echo  $value['id']?>)"><i class="fa fa-minus" aria-hidden="true"></i></a> <?php echo $value['cantidad'] ?> <a class="btn btn-primary btn-xs" onclick="increment(<?php echo  $value['id']?>)"><i class="fa fa-plus" aria-hidden="true"></i></a></td>
                                        <td><?php echo $value['total'] ?> €</td>
                                      </tr>
                                  <?php } ?>
                                <?php } ?>
                              </tbody>
                            </table>
                          </div>
                          <div class="panel-footer">
                            
                          </div>
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="panel panel-default panel-table">
                          <div class="panel-heading">
                            <div class="row">
                              <div class="col col-xs-12">
                                <h3 class="panel-title">Datos obligatorios de alquiler</h3>
                              </div>
                            </div>
                          </div>
                          <div class="panel-body">
                            <label class='label-reserva'>Fecha de inicio</label>
                            <input type="text" class="input-reserva">
                            <label class='label-reserva'>Fecha de fin</label>
                            <input type="text" class="input-reserva">
                            <label class='label-reserva'>Hora de recogida</label>
                            <input type="text" class="input-reserva">
                            <label class='label-reserva'>Nombre y apellidos</label>
                            <input type="text" class="input-reserva">
                            <label class='label-reserva'>Email</label>
                            <input type="text" class="input-reserva">
                            <label class='label-reserva'>Teléfono</label>
                            <input type="text" class="input-reserva">
                          </div>
                          <div class="panel-footer">
                            
                          </div>
                        </div>
                    </div>

                    
                  
                </div>
                <div class="col-md-1"></div>
                <div class="col-md-5 formulario-reserva">
                    <div class="col-md-12">
                        <div class="panel panel-default panel-table">
                          <div class="panel-heading">
                            <div class="row">
                              <div class="col col-xs-12">
                                <h3 class="panel-title">¿Tienes el codigo de disponibilidad para hacer la reserva?</h3>
                              </div>
                            </div>
                          </div>
                          <div class="panel-body">
                            <label class='label-reserva'>Código</label>
                            <input type="text" class="input-reserva">
                            <input class="btn btn-primary" value='Alquilar' style='margin-bottom: 10px;' onclick="alert('el código no es válido')">
                            <p>Si no lo tienes contacta con nosotros para consultar la disponibilidad</p>


                          </div>
                          <div class="panel-footer">
                            
                          </div>
                        </div>
                    </div>

                    <div class="col-md-12">
                        
                        <a href='index.php#contacto' class="btn btn-danger btn-reserva">Contacta</a>
                    </div>
                    
                    
                </div>
                
                
            </div>
        </div>

        <div id="bg"></div>

        <script src="assets/js/jquery.min.js"></script>
        <script src="assets/js/skel.min.js"></script>
        <script src="assets/js/util.js"></script>
        <script src="assets/js/main.js"></script>
        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
        <script type="text/javascript" src="assets/js/functions.js"></script>
    </body>
</html>
