<?php
/*
Plugin Name: Pinpoint Booking System add-on: Stripe Payment
Version: 1.1.5
Plugin URI: https://pinpoint.world/shop/product/pinpoint-booking-system-wordpress-plugin-add-on-stripe-payment/
Description: Accept payments for Pinpoint bookings using Stripe gateway. The add-on is compatible with <a href="https://pinpoint.world/wordpress/" target="_blank">Pinpoint Booking System PRO</a> (minimum version 2.1.1) and <a href="https://wordpress.org/plugins/booking-system/" target="_blank">Pinpoint Booking System FREE</a> (minimum version 2.1.1).
Author: Dot on Paper
Author URI: https://www.dotonpaper.net
*/

/*
Change log:
 	
        1.1.5 (2018-04-17)
                
                * Payments will be processed correctly when billing address is enabled, bug repaired.
                * Start hour and end hour parameters are sent to Stripe correctly, bug repaired.
                * The prices for currencies 'BIF', 'DJF', 'JPY', 'KRW', 'PYG', 'VND', 'XAF', 'XPF', 'CLP', 'GNF', 'KMF', 'MGA', 'RWF', 'VUV', 'XOF' are calculated correctly, bug repaired.

	1.1.4 (2017-11-16) 
    
		* Compatibility with Stripe latest API has been added.
 	
	1.1.3 (2017-11-10) 
    
		* Stripe cardholder name has been added.
 	
	1.1.2 (2017-10-12) 
    
		* Stripe API has been updated.
		* Stripe library has been updated.
 	
	1.1.1 (2017-05-16) 
    
		* Amount precission bug has been repaired.
		* Error message bug has been repaired.
 	
	1.1.0 (2017-04-13) 
    
		* Double booking bug has been repaired.
		* Refund bug has been repaired.
 	
	1.0.9 (2017-04-06) 
    
		* Billing address bug has been repaired.
		* Credit card error bug has been repaired.
 	
	1.0.8 (2017-03-13) 
    
		* Front end translation bug has been repaired.
		* Licence activation bug has been repaired.
 	
        1.0.7 (2017-03-03)
        
		* Front end translation bug has been repaired.
 	
	1.0.6 (2016-10-11) 
    
		* The licence activation bug has been repaired.
 	
	1.0.5 (2016-08-10) 
 
		* Compatibility added for Pinpoint Booking System PRO 2.3.0 and Pinpoint Booking System FREE 2.3.0
		* The licence activation bug has been repaired.
 	
	1.0.4 (2016-07-29) 
 
		* Card holder functionality works correctly, bug repaired.
		* Omnipay library has been added.
		* Update functionality has been repaired.
 	
	1.0.3 (2015-09-14)
 
		* Compatibility added for Pinpoint Booking System PRO 2.1.1 and Pinpoint Booking System FREE 2.1.1
 	
	1.0.2 (2015-06-10)
 
		* Auto update has been added.
 	
	1.0.1 (2015-02-19)
 
		* "Settings" database has been updated.
		* Payments may be refunded when a reservation is canceled.
 	
	1.0 (2015-01-11)
	
		* Initial release of Pinpoint Booking System (WordPress Plugin) add-on: Stripe Payment.
*/

    if (in_array('dopbsp/dopbsp.php', apply_filters('active_plugins', get_option('active_plugins')))
        || in_array('dopbsp/dopbs.php', apply_filters('active_plugins', get_option('active_plugins')))
        || in_array('booking-system/dopbs.php', apply_filters('active_plugins', get_option('active_plugins')))) {
        
    } else {
        add_action('admin_notices', 'DOPBSPStripeInactive');
        
        return false;
    }
        
    /*
     * Stripe
     */
    include_once 'libraries/php/Stripe/init.php';
    
    DOPBSPStripeErrorsHandler::start();

    try{
        /*
         * Views
         */
        include_once 'views/views-stripe-backend-settings.php';
        
        /*
         * Classes
         */
        include_once 'libraries/php/class-wc-plugin-update.php';
        include_once 'includes/class-stripe-settings.php';
        include_once 'includes/class-stripe-translation-text.php';
        include_once 'includes/class-stripe-update.php';
    }
    catch(Exception $ex){
        add_action('admin_notices', 'DOPBSPStripeMissingFiles');
    }

    DOPBSPStripeErrorsHandler::finish();
    
    /*
     * Global classes.
     */
    global $DOPBSP_stripe;
    
    /*
     * Booking System PRO main PHP class.
     */
    if (!class_exists('DOPBSPStripe')){
        class DOPBSPStripe{
            /*
             * Private variables.
             */
            private $api_key_secret;
            private $redirect;
            
            private $currency_code = '';
            private $payment_amount = 0;
            
            /*
             * Public variables.
             */
            public $paths;
            public $vars;
            
            /*
             * Constructor
             */
            function __construct(){
                $this->classes = new stdClass;
                $this->paths = new stdClass;
                $this->vars = new stdClass;
                
                // Update stripe texts
                $current_db_version_stripe = get_option('DOPBSP_db_version_stripe');
                
                if($current_db_version_stripe == ''
                  || $current_db_version_stripe < 1.08){
                    global $DOPBSP;
                    
                    if(isset($DOPBSP)){
                        require_once(str_replace('\\', '/', ABSPATH).'wp-admin/includes/upgrade.php');
                        $current_db_version_stripe == '' ? add_option('DOPBSP_db_version_stripe', 1.08):update_option('DOPBSP_db_version_stripe', 1.08);
                        update_option('DOPBSP_db_version', 2.2);
                        $DOPBSP->classes->database->updateTranslations2x();
                    }
                }
                
                /*
                 * Add JavaScript files.
                 */
                add_action('wp_enqueue_scripts', array(&$this, 'addScripts'));
                
                /*
                 * Initialize Stripe.
                 */
                add_filter('dopbsp_filter_payment_gateways', array(&$this, 'init'));
                
                /*
                 * Initialize Stripe card.
                 */
                add_filter('dopbsp_filter_payment_gateways_stripe_order_card', array(&$this, 'card'));
                
                /*
                 * Initialize Stripe token.
                 */
                add_filter('dopbsp_filter_payment_gateways_stripe_order_token', array(&$this, 'token'), 10, 2);
                
                /*
                 * Initialize Stripe settings view.
                 */
                add_filter('dopbsp_filter_views', array(&$this, 'view'));
                
                /*
                 * Pay after a booking request has been made.
                 */
                add_action('dopbsp_action_book_payment', array(&$this, 'pay'));
                
                /*
                 * Refund if a reservation is canceled.
                 */
                add_action('dopbsp_action_cancel_payment', array(&$this, 'refund'));
                
                /*
                 * Define plugin paths
                 */
                $this->definePaths();
                
                /*
                 * Initialize classes.
                 */
                $this->initClasses();
            }
            
            /*
             * Add plugin's JavaScript files.
             */
            function addScripts(){
                wp_register_script('DOPBSPStripe-js-stripe', $this->paths->url.'assets/js/frontend-stripe.js', array('jquery'), false, true);

                /*
                 *  Enqueue JavaScript.
                 */
                if (!wp_script_is('jquery', 'queue')){
                    wp_enqueue_script('jquery');
                }
                
                wp_enqueue_script('DOPBSPStripe-js-stripe');
            }
            
            /*
             * Defines plugin's paths constants.
             */
            function definePaths(){
                /*
                 * Plugin URL.
                 */
                $this->paths->url =  plugin_dir_url(__FILE__);

                /*
                 * Absolute path.
                 */
                $this->paths->abs = str_replace('\\', '/', plugin_dir_path(__FILE__));
            }
            
            /*
             * Initialize PHP classes.
             */
            function initClasses(){
                /*
                 * Initialize database class. This class is the 1st initialized.
                 */
                class_exists('DOPBSPStripeSettings') ? new DOPBSPStripeSettings():'Class does not exist!';
    
                /*
                 * Initialize translation class. This class is the 2nd initialized.
                 */
                class_exists('DOPBSPStripeTranslationText') ? new DOPBSPStripeTranslationText():'Class does not exist!';
                
                /*
                 * Initialize update classes.
                 */
                $this->classes->update = class_exists('DOPBSPStripeUpdate') ? new DOPBSPStripeUpdate():'Class does not exist!';
            }
            
            /*
             * Initialize Stripe payment gateway.
             * 
             * @param payment_gateways (array): payment gateways list
             * 
             * @return update payment gateways list
             */
            function init($payment_gateways){
                array_push($payment_gateways, 'stripe');
                
                return $payment_gateways;
            }
            
            /*
             * Initialize Stripe card.
             * 
             * @param card (array): card data
             * 
             * @return update card data
             */
            function card($card){
                $card['enabled'] = true;
                
                $card['expiration_date_month']['attribute'] = 'data-stripe';
                $card['expiration_date_month']['enabled'] = true;
                $card['expiration_date_month']['value'] = 'exp-month';
                
                $card['expiration_date_year']['attribute'] = 'data-stripe';
                $card['expiration_date_year']['enabled'] = true;
                $card['expiration_date_year']['value'] = 'exp-year';
                
                $card['number']['attribute'] = 'data-stripe';
                $card['number']['enabled'] = true;
                $card['number']['value'] = 'number';
                
                $card['name']['attribute'] = 'data-stripe';
                $card['name']['enabled'] = true;
                $card['name']['value'] = 'name';
                
                $card['security_code']['attribute'] = 'data-stripe';
                $card['security_code']['enabled'] = true;
                $card['security_code']['value'] = 'cvc';
                
                return $card;
            }
            
            /*
             * Initialize Stripe token.
             * 
             * @param token (array): token data
             * @param calendar_id (number): calendar ID
             * 
             * @return update token data
             */
            function token($token,
                           $calendar_id){
                global $DOPBSP;
                
                $settings_payment = $DOPBSP->classes->backend_settings->values($calendar_id, 
                                                                               'payment');
                $token = array('enabled' => true,
                               'function' => 'DOPBSPStripe.token('.$calendar_id.', \''.$DOPBSP->classes->prototypes->getRandomString(64).'\')');
                
                return $token;
            }
            
            /*
             * Add view class.
             * 
             * @param views (array): view classes
             * 
             * @return view classes list
             */
            function view($views){
                array_push($views, array('key' => 'backend_settings_stripe',
                                         'name' => 'DOPBSPStripeViewsBackEndSettings'));
                
                return $views;
            }
            
            /*
             *  Verify if the currency accepts decimals
             *  Returns the payment amount = as is if the currency(from the list) doesn't allow decimal values; times 100 if it does
             */
            
            function zeroDecimals($currency_code,
                                  $payment_amount){
              
                $zeroDecimals = [ 'BIF', 'DJF', 'JPY', 'KRW', 'PYG', 'VND', 'XAF', 'XPF', 'CLP', 'GNF', 'KMF', 'MGA', 'RWF', 'VUV', 'XOF' ];

                if (in_array($currency_code, $zeroDecimals)){
                    return $payment_amount;
                }
                else{
                    $payment_amount = round(floatval($payment_amount),2)*100;
                    return $payment_amount;
                } 
            }
            
            /*
             * Pay with Stripe.
             * 
             * @post calendar_id (integer): calendar ID
             * @post language (string): selected language
             * @post currency (string): currency sign
             * @post currency_code (string): ISO 4217 currency code
             * @post cart_data (array): the cart, list of reservations
             * @post form (object): form data
             * @post address_billing_data (array): billing address data
             * @post address_shipping_data (array): shipping address data
             * @post payment_method (string): payment method
             * @post form_addon_data (object): form addon data
             * @post card_data (array): card data
             * @post token (string): payment token (different for payment gateways)
             * @post page_url (string): the page from were the payment is requested
             */
            function pay(){
                global $wpdb;
                global $DOPBSP;
                $calendar_id = $_POST['calendar_id'];
                $currency_code = $_POST['currency_code'];
                $payment_method = $_POST['payment_method'];
                $token = $_POST['token'] == '' ? $DOPBSP->classes->prototypes->getRandomString(64):$_POST['token'];
                $card_data = $_POST['card_data'];
                $address_billing_data = $_POST['address_billing_data'];
                
                /*
                 * If selected payment method is Stripe continue with checkout API.
                 */
                if ($payment_method == 'stripe'){
                    $settings_payment = $DOPBSP->classes->backend_settings->values($calendar_id, 
                                                                                   'payment');
                    /*
                     * Set Stripe configuration.
                     */
                    $this->api_key_secret = $settings_payment->stripe_test_enabled == 'false' ? $settings_payment->stripe_key_secret:$settings_payment->stripe_test_key_secret;
                    $this->redirect = $settings_payment->stripe_redirect;
                    
                    $this->currency_code = $currency_code;
                    
                    /*
                     * Create the charge on Stripe servers. This will charge the user's card.
                     */
                    $reservations = $wpdb->get_results($wpdb->prepare('SELECT * FROM '.$DOPBSP->tables->reservations.' WHERE token="%s" ORDER BY id', 
                                                                      $token));
                    /*
                     * Calculate transaction payment amount & set description.
                     */
                    $descriptions = array();

                    $this->payment_amount = 0;

                    foreach ($reservations as $reservation){
                        $description = array();

                        $this->payment_amount += ($reservation->deposit_price > 0 ? (float)abs($reservation->deposit_price):
                                                                                    (float)abs($reservation->price_total));

                        $calendar = $wpdb->get_row($wpdb->prepare('SELECT * FROM '.$DOPBSP->tables->calendars.' WHERE id=%d', 
                                                                  $reservation->calendar_id));

                        array_push($description, $calendar->name.' - ');
                        array_push($description, $DOPBSP->text('RESERVATIONS_RESERVATION_FRONT_END_TITLE').' #'.$reservation->id.' (');
                        array_push($description, $DOPBSP->text('SEARCH_FRONT_END_CHECK_IN').': '.$reservation->check_in);
                        array_push($description, $reservation->check_out != '' ? ', '.$DOPBSP->text('SEARCH_FRONT_END_CHECK_OUT').': '.$reservation->check_out:'');
                        array_push($description, $reservation->start_hour != '' ? ', '.$DOPBSP->text('SEARCH_FRONT_END_START_HOUR').': '.$reservation->start_hour:'');
                        array_push($description, $reservation->end_hour != '' ? ', '.$DOPBSP->text('SEARCH_FRONT_END_END_HOUR').': '.$reservation->end_hour:'');
                        array_push($description, '...)');

                        array_push($descriptions, implode('', $description));
                    }

                    $mycard = array("name"         => $card_data["name"],
                                    "number" => $card_data['number'],
                                    "exp_month" => $card_data['expiration_date_month'],
                                    "exp_year" => $card_data['expiration_date_year'],
                                    "cvc" => $card_data['security_code']
                                   );
                    
                    if(isset($address_billing_data)) {
                        
//                        if(isset($address_billing_data['first_name'])) {
//                            $mycard['name'] = $address_billing_data['first_name'];
//                        }
//                        
//                        if(isset($address_billing_data['last_name'])) {
//                            $mycard['name'] = $address_billing_data['last_name'];
//                        }
//                        
//                        if(isset($address_billing_data['email'])) {
//                            $mycard['email'] = $address_billing_data['email'];
//                        }
//                        
//                        if(isset($address_billing_data['phone'])) {
//                            $mycard['billingPhone'] = $address_billing_data['phone'];
//                        }
                        
                        if(isset($address_billing_data['address_first'])) {
                            $mycard['address_line1'] = $address_billing_data['address_first'];
                        }
                        
                        if (isset($address_billing_data['address_second'])) {
                            $mycard['address_line2'] = $address_billing_data['address_second'];
                        }
                        
                        if(isset($address_billing_data['country'])) {
                            $mycard['address_country'] = $address_billing_data['country'];
                        }
                        
                        if(isset($address_billing_data['city'])) {
                            $mycard['address_city'] = $address_billing_data['city'];
                        }
                        
                        if(isset($address_billing_data['zip_code'])) {
                            $mycard['address_zip'] = $address_billing_data['zip_code'];
                        }
                        
                        if(isset($address_billing_data['state'])) {
                            $mycard['address_state'] = $address_billing_data['state'];
                        }
                    }
               
                    \Stripe\Stripe::setApiKey($this->api_key_secret);
                    
                    try {
                        $card_token = \Stripe\Token::create(array(
                              "card" => $mycard
                        ));
                        
                        $card_token_data = $card_token->getLastResponse();
                        
                        $this->payment_amount = DOPBSPStripe::zeroDecimals($this->currency_code, $this->payment_amount);
                        
                        $transaction = \Stripe\Charge::create(array(
                          "amount" => $this->payment_amount,
                          "currency" => $this->currency_code,
                          "source" => $card_token_data->json['id'], // obtained with Stripe.js
                          "description" => implode('', $descriptions)
                        ));

                        $response = $transaction->getLastResponse();
                        
                        if ($response->json['paid']
                           && $response->json['status'] == 'succeeded') {

                            /*
                             * Update status to approved if payment succeeded.
                             */
                            $wpdb->update($DOPBSP->tables->reservations, array('status' => 'approved',
                                                                               'transaction_id' => $response->json['id'], //$charge->id,
                                                                               'token' => ''), 
                                                                         array('token' => $token));
                            /*
                             * Send notifications and update availability if the transaction was successful.
                             */
                            foreach ($reservations as $reservation){
                                $DOPBSP->classes->backend_calendar_schedule->setApproved($reservation->id);

                                $DOPBSP->classes->backend_reservation_notifications->send($reservation->id,
                                                                                          'stripe_admin');
                                $DOPBSP->classes->backend_reservation_notifications->send($reservation->id,
                                                                                          'stripe_user');
                            }

                            if ($this->redirect == ''){
                                echo 'success';
                            }
                            else{
                                echo 'success_redirect;;;;;'.$this->redirect;
                            }

                        } else {

                            echo $response->getMessage();

                            /*
                             * The credit card has been declined.
                             */

                            /*
                             * Delete the reservations if the payment did not succeed.
                             */
                            $wpdb->delete($DOPBSP->tables->reservations, array('token' => $token));
                        }
                    } catch (Stripe\Error\Card $e) {
                        /*
                         * The credit card has been declined.
                         */

                        /*
                         * Delete the reservations if the payment did not succeed.
                         */
                        $wpdb->delete($DOPBSP->tables->reservations, array('token' => $token));
                        
                        // internal error, log exception and display a generic message to the customer
                        exit($e->getMessage());
                    }
                    
                    die();

                }
            }
            
            /*
             * Issue a refund if a reservation has been canceled.
             * 
             * @param reservation (object): reservation data   
             */
            function refund($reservation){
                global $wpdb;
                global $DOPBSP;
                
                /*
                 * Check if selected payment method is PayPal access.
                 */
                if ($reservation->payment_method == 'stripe'){
                    $settings_payment = $DOPBSP->classes->backend_settings->values($reservation->calendar_id, 
                                                                                   'payment');
                    
                    /*
                     * Check if selected refunds are enabled.
                     */
                    if ($settings_payment->stripe_refund_enabled == 'true'){
                        /*
                         * Stop if a refund has been made.
                         */
                        if ($reservation->payment_status == 'partially refunded'
                                || $reservation->payment_status == 'refunded'){
                            echo 'success_with_message;;;;;'.$DOPBSP->text('RESERVATIONS_RESERVATION_CANCEL_SUCCESS_REFUND_WARNING');
                            return false;
                        }
                        
                        /*
                         * Set Stripe configuration.
                         */
                        $this->api_key_secret = $settings_payment->stripe_test_enabled == 'false' ? $settings_payment->stripe_key_secret:$settings_payment->stripe_test_key_secret;
                        
                        /*
                         * Create the charge on Stripe servers. This will charge the user's card.
                         */
                        $refund_value = $settings_payment->stripe_refund_type == 'fixed' ? $settings_payment->stripe_refund_value:($reservation->price_total*$settings_payment->stripe_refund_value);
                        
                        
                        
                        \Stripe\Stripe::setApiKey($this->api_key_secret);

                        try {
                            $transaction = \Stripe\Refund::create(array(
                              "amount" => floatval($refund_value),
                              "charge" => $reservation->transaction_id
                            ));

                            $response = $transaction->getLastResponse();

                            if ($response->json['status'] == 'succeeded') {
                                $refund_id = $response->json['id'];

                                /*
                                 * Update status to approved if payment succeeded.
                                 */
                                $wpdb->update($DOPBSP->tables->reservations, array('refund' => $refund_value,
                                                                                   'payment_status' => $refund_value == $reservation->price_total ? 'refunded':'partially refunded'), 
                                                                             array('id' => $reservation->id));

                                /*
                                 * Success message.
                                 */
                                $settings_calendar = $DOPBSP->classes->backend_settings->values($reservation->calendar_id,  
                                                                                                'calendar');
                                echo 'success_with_message;;;;;';
                                printf($DOPBSP->text('RESERVATIONS_RESERVATION_CANCEL_SUCCESS_REFUND'), $DOPBSP->classes->price->set($refund_value/100,
                                                                                                    $reservation->currency,
                                                                                                    $settings_calendar->currency_position));
                            }
                        } catch (Stripe\Error $e) {
                            exit($e->getMessage());
                        }
                    }
                }
            }
        }
        $DOPBSP_stripe = new DOPBSPStripe();
    }
    
    /*
     * Delete all plugin data if you uninstall it. IMPORTANT! The function needs to be in this file.
     */
    function DOPBSPStripeUninstall(){
        /*
         * Delete options.
         */
        delete_option('DOPBSP_db_version_stripe');
    }
          
    /*
     * Hook uninstall function. The registration needs to be in this file.
     */
    register_uninstall_hook(__FILE__, 'DOPBSPStripeUninstall');  
 
 // Files not included errors handler.
    
    /*
     * Booking System PRO errors handler PHP class. IMPORTANT! The class needs to be in this file.
     */
    class DOPBSPStripeErrorsHandler{
        static $IGNORE_DEPRECATED = true;

        /*
         * Start redirecting PHP errors.
         * 
         * @param level (integer): PHP error level to catch (Default = E_ALL & ~E_DEPRECATED)
         */
        static function start($level = null){
            if ($level == null){
                if (defined('E_DEPRECATED')){
                    $level = E_ALL & ~E_DEPRECATED;
                }
                else{
                    $level = E_ALL;
                    self::$IGNORE_DEPRECATED = true;
                }
            }
            set_error_handler(array('DOPBSPStripeErrorsHandler', 'handle'), $level);
        }

        /*
         * Stop redirecting PHP errors.
         */
        static function finish(){
            restore_error_handler();
        }

        /*
         * Handle error exceptions.
         *
         * @param code (string)
         * @param string (string)
         * @param file (string)
         * @param line (string)
         * @param context (string)
         * 
         * @return true if no errors else the errors object
         */
        static function handle($code,
                               $string,
                               $file,
                               $line,
                               $context){
            if (error_reporting() == 0){
                return;
            }

            if (self::$IGNORE_DEPRECATED 
                    && strpos($string, 'deprecated') === true){
                return true;
            }
            throw new Exception($string, $code);
        }
    }
    
    /*
     * Message to be displayed if Booking System PRO is not activated. IMPORTANT! The function needs to be in this file.
     */
    function DOPBSPStripeInactive(){
        $error = array();
        
        array_push($error, '<div class="update-nag">');
        array_push($error, '  <p>WARNING for <strong>Booking System add-on: Stripe Payment</strong>! You need to have Booking System activated to use this plugin. If you think Booking System  is activated and this message still exist, please contact us at <a href="mailto:support@dotonpaper.zendesk.com" target="_blank">support@dotonpaper.zendesk.com</a>.</p>');
        array_push($error, '</div>');
        
        echo implode('', $error);
    }
    
    /*
     * Message to be displayed if not all PHP files are loaded. IMPORTANT! The function needs to be in this file.
     */
    function DOPBSPStripeMissingFiles(){
        $error = array();
        
        array_push($error, '<div class="update-nag">');
        array_push($error, '  <p>WARNING for <strong>Booking System add-on: Stripe Payment</strong>! Not all PHP files needed to run the plugin are on the server, in folder <strong>wp-content/plugins/dopbsp-stripe</strong>. If you are installing or updating the plugin using FTP, please allow all files to upload, or try to upload them again. If you think all files are on the server and this message still exist, please contact us on our <a href="http://envato-support.dotonpaper.net/" target="_blank">Support Forum</a>.</p>');
        array_push($error, '</div>');
        
        echo implode('', $error);
    }