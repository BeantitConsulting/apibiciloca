/*
 * Title                   : Pinpoint Booking System (WordPress Plugin) add-on: Stripe Payment
 * File                    : assets/js/frontend-stripe.js
 * Author                  : Dot on Paper
 * Copyright               : © 2016 Dot on Paper
 * Website                 : http://www.dotonpaper.net
 * Description             : Stripe front end JavaScript class.
 */

var DOPBSPStripe = new function(){
    'use strict';
    
    /*
     * Private variables
     */
    var $ = jQuery.noConflict(),
    formID;

    /*
     * Constructor
     */
    this.DOPBSPStripe = function(){
    };

    /*
     * Create credit card token.
     * 
     * @param id (Number): calendar ID
     * @param key (String): stripe publishable key
     */
    this.token = function(id,
                          token){
        formID = id;
        
        $('#DOPBSPCalendar-payment-token'+formID).val(token);
        $('#DOPBSPCalendar-payment-token-message'+formID).val('');
    };
    
    return this.DOPBSPStripe();
};