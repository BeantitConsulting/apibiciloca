<?php

/*
 * Title                   : Pinpoint Booking System (WordPress Plugin) add-on: Stripe Payment
 * File                    : views/views-stripe-backend-settings.php
 * Author                  : Dot on Paper
 * Copyright               : © 2016 Dot on Paper
 * Website                 : https://www.dotonpaper.net
 * Description             : Stripe user settings views class.
 */

    if (!class_exists('DOPBSPStripeViewsBackEndSettings')){
        class DOPBSPStripeViewsBackEndSettings{
            /*
             * Constructor
             */
            function __construct(){
                add_action('dopbsp_action_views_settings_payment_gateways', array(&$this, 'template'));
                add_action('dopbsp_action_views_settings_notifications', array(&$this, 'templateNotifications'));
                add_action('dopbsp_action_views_settings_licences', array(&$this, 'templateLicenses'));
            }
            
            /*
             * Returns payment Stripe settings template.
             * 
             * @param args (array): function arguments
             *                      * settings_payment (object): payment settings
             * 
             * @return Stripe settings HTML
             */
            function template($args = array()){
                global $DOPBSP;
                
                $id = $args['id'];
                $settings_payment = $args['settings_payment'];
?>
                <div class="dopbsp-inputs-header <?php echo $settings_payment->stripe_enabled == 'true' ? 'dopbsp-hide':'dopbsp-display'; ?>">
                    <h3><?php echo $DOPBSP->text('SETTINGS_PAYMENT_GATEWAYS_STRIPE'); ?></h3>
                    <a href="javascript:DOPBSPBackEnd.toggleInputs('stripe')" id="DOPBSP-inputs-button-stripe" class="dopbsp-button"></a>
                </div>
                <div id="DOPBSP-inputs-stripe" class="dopbsp-inputs-wrapper <?php echo $settings_payment->stripe_enabled == 'true' ? 'dopbsp-displayed':'dopbsp-hidden'; ?>">
<?php
                    /*
                     * Enable Stripe.
                     */
                    $DOPBSP->views->backend_settings->displaySwitchInput(array('id' => 'stripe_enabled',
                                                                               'label' => $DOPBSP->text('SETTINGS_PAYMENT_GATEWAYS_STRIPE_ENABLED'),
                                                                               'value' => $settings_payment->stripe_enabled,
                                                                               'settings_id' => $id,
                                                                               'settings_type' => 'payment',
                                                                               'help' => $DOPBSP->text('SETTINGS_PAYMENT_GATEWAYS_STRIPE_ENABLED_HELP'),
                                                                               'container_class' => ''));  
                    /*
                     * Stripe secret key.
                     */
                    $DOPBSP->views->backend_settings->displayTextInput(array('id' => 'stripe_key_secret',
                                                                             'label' => $DOPBSP->text('SETTINGS_PAYMENT_GATEWAYS_STRIPE_KEY_SECRET'),
                                                                             'value' => $settings_payment->stripe_key_secret,
                                                                             'settings_id' => $id,
                                                                             'settings_type' => 'payment',
                                                                             'help' => $DOPBSP->text('SETTINGS_PAYMENT_GATEWAYS_STRIPE_KEY_SECRET_HELP')));
                    /*
                     * Stripe publishable key.
                     */
                    $DOPBSP->views->backend_settings->displayTextInput(array('id' => 'stripe_key_publishable',
                                                                             'label' => $DOPBSP->text('SETTINGS_PAYMENT_GATEWAYS_STRIPE_KEY_PUBLISHABLE'),
                                                                             'value' => $settings_payment->stripe_key_publishable,
                                                                             'settings_id' => $id,
                                                                             'settings_type' => 'payment',
                                                                             'help' => $DOPBSP->text('SETTINGS_PAYMENT_GATEWAYS_STRIPE_KEY_PUBLISHABLE_HELP')));
                    /*
                     * Enable Stripe test.
                     */
                    $DOPBSP->views->backend_settings->displaySwitchInput(array('id' => 'stripe_test_enabled',
                                                                               'label' => $DOPBSP->text('SETTINGS_PAYMENT_GATEWAYS_STRIPE_TEST_ENABLED'),
                                                                               'value' => $settings_payment->stripe_test_enabled,
                                                                               'settings_id' => $id,
                                                                               'settings_type' => 'payment',
                                                                               'help' => $DOPBSP->text('SETTINGS_PAYMENT_GATEWAYS_STRIPE_TEST_ENABLED_HELP'),
                                                                               'container_class' => ''));  
                    /*
                     * Stripe test secret key.
                     */
                    $DOPBSP->views->backend_settings->displayTextInput(array('id' => 'stripe_test_key_secret',
                                                                             'label' => $DOPBSP->text('SETTINGS_PAYMENT_GATEWAYS_STRIPE_TEST_KEY_SECRET'),
                                                                             'value' => $settings_payment->stripe_test_key_secret,
                                                                             'settings_id' => $id,
                                                                             'settings_type' => 'payment',
                                                                             'help' => $DOPBSP->text('SETTINGS_PAYMENT_GATEWAYS_STRIPE_TEST_KEY_SECRET_HELP')));
                    /*
                     * Stripe test publishable key.
                     */
                    $DOPBSP->views->backend_settings->displayTextInput(array('id' => 'stripe_test_key_publishable',
                                                                             'label' => $DOPBSP->text('SETTINGS_PAYMENT_GATEWAYS_STRIPE_TEST_KEY_PUBLISHABLE'),
                                                                             'value' => $settings_payment->stripe_test_key_publishable,
                                                                             'settings_id' => $id,
                                                                             'settings_type' => 'payment',
                                                                             'help' => $DOPBSP->text('SETTINGS_PAYMENT_GATEWAYS_STRIPE_TEST_KEY_PUBLISHABLE_HELP')));
                    /*
                     * Enable refunds.
                     */
                    $DOPBSP->views->backend_settings->displaySwitchInput(array('id' => 'stripe_refund_enabled',
                                                                               'label' => $DOPBSP->text('SETTINGS_PAYMENT_GATEWAYS_STRIPE_REFUND_ENABLED'),
                                                                               'value' => $settings_payment->stripe_refund_enabled,
                                                                               'settings_id' => $id,
                                                                               'settings_type' => 'payment',
                                                                               'help' => $DOPBSP->text('SETTINGS_PAYMENT_GATEWAYS_STRIPE_REFUND_ENABLED_HELP')));  
                    /*
                     * Refund value.
                     */
                    $DOPBSP->views->backend_settings->displayTextInput(array('id' => 'stripe_refund_value',
                                                                             'label' => $DOPBSP->text('SETTINGS_PAYMENT_GATEWAYS_STRIPE_REFUND_VALUE'),
                                                                             'value' => $settings_payment->stripe_refund_value,
                                                                             'settings_id' => $id,
                                                                             'settings_type' => 'payment',
                                                                             'help' => $DOPBSP->text('SETTINGS_PAYMENT_GATEWAYS_STRIPE_REFUND_VALUE_HELP'),
                                                                             'input_class' => 'dopbsp-small'));   
                    /*
                     * Refund type.
                     */
                    $DOPBSP->views->backend_settings->displaySelectInput(array('id' => 'stripe_refund_type',
                                                                               'label' => $DOPBSP->text('SETTINGS_PAYMENT_GATEWAYS_STRIPE_REFUND_TYPE'),
                                                                               'value' => $settings_payment->stripe_refund_type,
                                                                               'settings_id' => $id,
                                                                               'settings_type' => 'payment',
                                                                               'help' => $DOPBSP->text('SETTINGS_PAYMENT_GATEWAYS_STRIPE_REFUND_TYPE_HELP'),
                                                                               'options' => $DOPBSP->text('SETTINGS_PAYMENT_GATEWAYS_STRIPE_REFUND_TYPE_FIXED').';;'.$DOPBSP->text('SETTINGS_PAYMENT_GATEWAYS_STRIPE_REFUND_TYPE_PERCENT'),
                                                                               'options_values' => 'fixed;;percent'));
                    /*
                     * Stripe redirect.
                     */
                    $DOPBSP->views->backend_settings->displayTextInput(array('id' => 'stripe_redirect',
                                                                             'label' => $DOPBSP->text('SETTINGS_PAYMENT_GATEWAYS_STRIPE_REDIRECT'),
                                                                             'value' => $settings_payment->stripe_redirect,
                                                                             'settings_id' => $id,
                                                                             'settings_type' => 'payment',
                                                                             'help' => $DOPBSP->text('SETTINGS_PAYMENT_GATEWAYS_STRIPE_REDIRECT_HELP'),
                                                                             'container_class' => 'dopbsp-last'));
?>
                </div>
<?php
            }
            
            /*
             * Returns notifications Stripe settings template.
             * 
             * @param args (array): function arguments
             *                      * settings_notifications (object): notifications settings
             * 
             * @return Stripe settings HTML
             */
            function templateNotifications($args = array()){
                global $DOPBSP;
                
                $settings_notifications = $args['settings_notifications'];
                
                /*
                 * Send on Stripe payment to admin.
                 */
                $DOPBSP->views->backend_settings->displaySwitchInput(array('id' => 'send_stripe_admin',
                                                                           'label' => $DOPBSP->text('SETTINGS_PAYMENT_GATEWAYS_STRIPE_SEND_ADMIN'),
                                                                           'value' => $settings_notifications->send_stripe_admin,
                                                                           'settings_id' => $id,
                                                                           'settings_type' => 'notifications',
                                                                           'help' => $DOPBSP->text('SETTINGS_PAYMENT_GATEWAYS_STRIPE_SEND_ADMIN_HELP')));
                /*
                 * Send on Stripe payment to user.
                 */
                $DOPBSP->views->backend_settings->displaySwitchInput(array('id' => 'send_stripe_user',
                                                                           'label' => $DOPBSP->text('SETTINGS_PAYMENT_GATEWAYS_STRIPE_SEND_USER'),
                                                                           'value' => $settings_notifications->send_stripe_user,
                                                                           'settings_id' => $id,
                                                                           'settings_type' => 'notifications',
                                                                           'help' => $DOPBSP->text('SETTINGS_PAYMENT_GATEWAYS_STRIPE_SEND_USER_HELP')));
            }
            
            /*
             * Returns licences Stripe settings template.
             * 
             * @param args (array): function arguments
             *                      * settings_global (object): global settings
             * 
             * @return Stripe settings HTML
             */
            function templateLicenses($args = array()){
                global $DOPBSP;
                
                $settings_global = $args['settings_general'];
?>
                <div class="dopbsp-inputs-header dopbsp-hide">
                    <h3><?php echo $DOPBSP->text('SETTINGS_PAYMENT_GATEWAYS_STRIPE_LICENSE'); ?></h3>
                    <a href="javascript:DOPBSPBackEnd.toggleInputs('stripe')" id="DOPBSP-inputs-button-stripe" class="dopbsp-button"></a>
                    <a href="javascript:DOPBSPBackEndSettingsLicences.set('stripe', 'deactivate')" id="DOPBSP-inputs-button-stripe-deactivate" class="dopbsp-button-text" style="display: <?php echo $settings_global->stripe_licence_status == 'activated' ? 'block':'none'; ?>"><?php echo $DOPBSP->text('SETTINGS_LICENCES_STATUS_DEACTIVATE'); ?></a>
                    <a href="javascript:DOPBSPBackEndSettingsLicences.set('stripe', 'activate')" id="DOPBSP-inputs-button-stripe-activate" class="dopbsp-button-text" style="display: <?php echo $settings_global->stripe_licence_status == 'activated' ? 'none':'block'; ?>"><?php echo $DOPBSP->text('SETTINGS_LICENCES_STATUS_ACTIVATE'); ?></a>
                </div>
                <div id="DOPBSP-inputs-stripe" class="dopbsp-inputs-wrapper dopbsp-displayed">
                    <!--
                        Stripe licence status.
                    -->
                    <div class="dopbsp-input-wrapper">
                        <label><?php echo $DOPBSP->text('SETTINGS_LICENCES_STATUS'); ?></label>
                        <em id="DOPBSP-settings-stripe-licence-status" class="dopbsp-licence-status<?php echo $settings_global->stripe_licence_status == 'activated' ? ' dopbsp-activated':' dopbsp-deactivated'; ?>">
                            <?php echo $settings_global->stripe_licence_status == 'activated' ? $DOPBSP->text('SETTINGS_LICENCES_STATUS_ACTIVATED'):$DOPBSP->text('SETTINGS_LICENCES_STATUS_DEACTIVATED'); ?>
                        </em>
                    </div>
                    
                    <!--
                        Stripe licence key.
                    -->
                    <div class="dopbsp-input-wrapper">
                        <label for="DOPBSP-settings-stripe-licence-key"><?php echo $DOPBSP->text('SETTINGS_LICENCES_KEY'); ?></label>
                        <input type="text" name="DOPBSP-settings-stripe-licence-key" id="DOPBSP-settings-stripe-licence-key" value="<?php echo $settings_global->stripe_licence_key; ?>"<?php echo $settings_global->stripe_licence_status == 'activated' ? ' disabled="disabled"':''; ?> />
                        <a href="<?php echo DOPBSP_CONFIG_HELP_DOCUMENTATION_URL; ?>" target="_blank" class="dopbsp-button dopbsp-help"><span class="dopbsp-info dopbsp-help"><?php echo $DOPBSP->text('SETTINGS_LICENCES_KEY_HELP'); ?><br /><br /><?php echo $DOPBSP->text('HELP_VIEW_DOCUMENTATION'); ?></span></a>
                    </div>
                    
                    <!--
                        Stripe licence email.
                    -->
                    <div class="dopbsp-input-wrapper dopbsp-last">
                        <label for="DOPBSP-settings-stripe-licence-email"><?php echo $DOPBSP->text('SETTINGS_LICENCES_EMAIL'); ?></label>
                        <input type="text" name="DOPBSP-settings-stripe-licence-email" id="DOPBSP-settings-stripe-licence-email" value="<?php echo $settings_global->stripe_licence_email; ?>"<?php echo $settings_global->stripe_licence_status == 'activated' ? ' disabled="disabled"':''; ?> />
                        <a href="<?php echo DOPBSP_CONFIG_HELP_DOCUMENTATION_URL; ?>" target="_blank" class="dopbsp-button dopbsp-help"><span class="dopbsp-info dopbsp-help"><?php echo $DOPBSP->text('SETTINGS_LICENCES_EMAIL_HELP'); ?><br /><br /><?php echo $DOPBSP->text('HELP_VIEW_DOCUMENTATION'); ?></span></a>
                    </div>
                </div>
<?php
            }
        }
    }