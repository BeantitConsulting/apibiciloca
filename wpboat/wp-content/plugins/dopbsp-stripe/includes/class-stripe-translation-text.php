<?php

/*
 * Title                   : Pinpoint Booking System (WordPress Plugin) add-on: Stripe Payment
 * File                    : includes/class-stripe-translation-text.php
 * Author                  : Dot on Paper
 * Copyright               : © 2016 Dot on Paper
 * Website                 : http://www.dotonpaper.net
 * Description             : Stripe translation text PHP class.
 */

    if (!class_exists('DOPBSPStripeTranslationText')){
        class DOPBSPStripeTranslationText{
            /*
             * Constructor
             */
            function __construct(){
                /*
                 * Initialize settings text.
                 */
                add_filter('dopbsp_filter_translation_text', array(&$this, 'settings'));
                add_filter('dopbsp_filter_translation_text', array(&$this, 'settingsHelp'));
                
                /*
                 * Initialize order text.
                 */
                add_filter('dopbsp_filter_translation_text', array(&$this, 'order'));
                
                /*
                 * Initialize email text.
                 */
                add_filter('dopbsp_filter_translation_text', array(&$this, 'email'));
            }
            
            /*
             * Stripe settings text.
             * 
             * @param lang (array): current translation
             * 
             * @return array with updated translation
             */
            function settings($text){
                array_push($text, array('key' => 'PARENT_SETTINGS_PAYMENT_GATEWAYS_STRIPE',
                                        'parent' => '',
                                        'text' => 'Settings - Stripe'));
               
                array_push($text, array('key' => 'SETTINGS_PAYMENT_GATEWAYS_STRIPE',
                                        'parent' => 'PARENT_SETTINGS_PAYMENT_GATEWAYS_STRIPE',
                                        'text' => 'Stripe'));
                array_push($text, array('key' => 'SETTINGS_PAYMENT_GATEWAYS_STRIPE_ENABLED',
                                        'parent' => 'PARENT_SETTINGS_PAYMENT_GATEWAYS_STRIPE',
                                        'text' => 'Enable Stripe payment'));
                array_push($text, array('key' => 'SETTINGS_PAYMENT_GATEWAYS_STRIPE_KEY_SECRET',
                                        'parent' => 'PARENT_SETTINGS_PAYMENT_GATEWAYS_STRIPE',
                                        'text' => 'Stripe secret key'));
                array_push($text, array('key' => 'SETTINGS_PAYMENT_GATEWAYS_STRIPE_KEY_PUBLISHABLE',
                                        'parent' => 'PARENT_SETTINGS_PAYMENT_GATEWAYS_STRIPE',
                                        'text' => 'Stripe publishable key'));
                array_push($text, array('key' => 'SETTINGS_PAYMENT_GATEWAYS_STRIPE_TEST_ENABLED',
                                        'parent' => 'PARENT_SETTINGS_PAYMENT_GATEWAYS_STRIPE',
                                        'text' => 'Enable Stripe test'));
                array_push($text, array('key' => 'SETTINGS_PAYMENT_GATEWAYS_STRIPE_TEST_KEY_SECRET',
                                        'parent' => 'PARENT_SETTINGS_PAYMENT_GATEWAYS_STRIPE',
                                        'text' => 'Stripe test secret key'));
                array_push($text, array('key' => 'SETTINGS_PAYMENT_GATEWAYS_STRIPE_TEST_KEY_PUBLISHABLE',
                                        'parent' => 'PARENT_SETTINGS_PAYMENT_GATEWAYS_STRIPE',
                                        'text' => 'Stripe test publishable key'));
                array_push($text, array('key' => 'SETTINGS_PAYMENT_GATEWAYS_STRIPE_REFUND_ENABLED',
                                        'parent' => 'PARENT_SETTINGS_PAYMENT_GATEWAYS_STRIPE',
                                        'text' => 'Enable refunds'));
                array_push($text, array('key' => 'SETTINGS_PAYMENT_GATEWAYS_STRIPE_REFUND_VALUE',
                                        'parent' => 'PARENT_SETTINGS_PAYMENT_GATEWAYS_STRIPE',
                                        'text' => 'Refund value'));
                array_push($text, array('key' => 'SETTINGS_PAYMENT_GATEWAYS_STRIPE_REFUND_TYPE',
                                        'parent' => 'PARENT_SETTINGS_PAYMENT_GATEWAYS_STRIPE',
                                        'text' => 'Refund type'));
                array_push($text, array('key' => 'SETTINGS_PAYMENT_GATEWAYS_STRIPE_REFUND_TYPE_FIXED',
                                        'parent' => 'PARENT_SETTINGS_PAYMENT_GATEWAYS_STRIPE',
                                        'text' => 'Fixed'));
                array_push($text, array('key' => 'SETTINGS_PAYMENT_GATEWAYS_STRIPE_REFUND_TYPE_PERCENT',
                                        'parent' => 'PARENT_SETTINGS_PAYMENT_GATEWAYS_STRIPE',
                                        'text' => 'Percent'));
                array_push($text, array('key' => 'SETTINGS_PAYMENT_GATEWAYS_STRIPE_REDIRECT',
                                        'parent' => 'PARENT_SETTINGS_PAYMENT_GATEWAYS_STRIPE',
                                        'text' => 'Redirect after payment'));
                /*
                 * Notifications
                 */     
                array_push($text, array('key' => 'SETTINGS_PAYMENT_GATEWAYS_STRIPE_SEND_ADMIN',
                                        'parent' => 'PARENT_SETTINGS_PAYMENT_GATEWAYS_STRIPE',
                                        'text' => 'Stripe - Notify admin'));
                array_push($text, array('key' => 'SETTINGS_PAYMENT_GATEWAYS_STRIPE_SEND_USER',
                                        'parent' => 'PARENT_SETTINGS_PAYMENT_GATEWAYS_STRIPE',
                                        'text' => 'Stripe - Notify user'));
                
                /*
                 * Licenses
                 */
                array_push($text, array('key' => 'SETTINGS_PAYMENT_GATEWAYS_STRIPE_LICENSE',
                                        'parent' => 'PARENT_SETTINGS_PAYMENT_GATEWAYS_STRIPE',
                                        'text' => 'Stripe Payment add-on activation'));
                
                return $text;
            }
            
            /*
             * Stripe settings help text.
             * 
             * @param lang (array): current translation
             * 
             * @return array with updated translation
             */
            function settingsHelp($text){
                array_push($text, array('key' => 'PARENT_SETTINGS_PAYMENT_GATEWAYS_STRIPE_HELP',
                                        'parent' => '',
                                        'text' => 'Settings - Stripe - Help'));
                
                array_push($text, array('key' => 'SETTINGS_PAYMENT_GATEWAYS_STRIPE_ENABLED_HELP',
                                        'parent' => 'PARENT_SETTINGS_PAYMENT_GATEWAYS_STRIPE_HELP',
                                        'text' => 'Default value: Disabled. Allow users to pay with Stripe. The period is instantly booked.'));
                array_push($text, array('key' => 'SETTINGS_PAYMENT_GATEWAYS_STRIPE_KEY_SECRET_HELP',
                                        'parent' => 'PARENT_SETTINGS_PAYMENT_GATEWAYS_STRIPE_HELP',
                                        'text' => 'Enter Stripe API secret key. View documentation to see from were you can get it.'));
                array_push($text, array('key' => 'SETTINGS_PAYMENT_GATEWAYS_STRIPE_KEY_PUBLISHABLE_HELP',
                                        'parent' => 'PARENT_SETTINGS_PAYMENT_GATEWAYS_STRIPE_HELP',
                                        'text' => 'Enter Stripe API publishable key. View documentation to see from were you can get it.'));
                array_push($text, array('key' => 'SETTINGS_PAYMENT_GATEWAYS_STRIPE_TEST_ENABLED_HELP',
                                        'parent' => 'PARENT_SETTINGS_PAYMENT_GATEWAYS_STRIPE_HELP',
                                        'text' => 'Default value: Disabled. Enable Stripe test.'));
                array_push($text, array('key' => 'SETTINGS_PAYMENT_GATEWAYS_STRIPE_TEST_KEY_SECRET_HELP',
                                        'parent' => 'PARENT_SETTINGS_PAYMENT_GATEWAYS_STRIPE_HELP',
                                        'text' => 'Enter Stripe API secret key for testing. View documentation to see from were you can get it.'));
                array_push($text, array('key' => 'SETTINGS_PAYMENT_GATEWAYS_STRIPE_TEST_KEY_PUBLISHABLE_HELP',
                                        'parent' => 'PARENT_SETTINGS_PAYMENT_GATEWAYS_STRIPE_HELP',
                                        'text' => 'Enter Stripe API publishable key for testing. View documentation to see from were you can get it.'));
                array_push($text, array('key' => 'SETTINGS_PAYMENT_GATEWAYS_STRIPE_REFUND_ENABLED_HELP',
                                        'parent' => 'PARENT_SETTINGS_PAYMENT_GATEWAYS_STRIPE_HELP',
                                        'text' => 'Default value: Disabled. Users that paid with Stripe will be refunded automatically if a reservation is canceled.'));
                array_push($text, array('key' => 'SETTINGS_PAYMENT_GATEWAYS_STRIPE_REFUND_VALUE_HELP',
                                        'parent' => 'PARENT_SETTINGS_PAYMENT_GATEWAYS_STRIPE_HELP',
                                        'text' => 'Default value: 100. Enter the refund value from reservation total price.'));
                array_push($text, array('key' => 'SETTINGS_PAYMENT_GATEWAYS_STRIPE_REFUND_TYPE_HELP',
                                        'parent' => 'PARENT_SETTINGS_PAYMENT_GATEWAYS_STRIPE_HELP',
                                        'text' => 'Default value: Percent. Select refund value type. It can be a fixed value or a percent from reservation price.'));
                array_push($text, array('key' => 'SETTINGS_PAYMENT_GATEWAYS_STRIPE_REDIRECT_HELP',
                                        'parent' => 'PARENT_SETTINGS_PAYMENT_GATEWAYS_STRIPE_HELP',
                                        'text' => 'Enter the URL where to redirect after the payment has been completed. Leave it blank to redirect back to the calendar.'));
                /*
                 * Notifications
                 */
                array_push($text, array('key' => 'SETTINGS_PAYMENT_GATEWAYS_STRIPE_SEND_ADMIN_HELP',
                                        'parent' => 'PARENT_SETTINGS_PAYMENT_GATEWAYS_STRIPE_HELP',
                                        'text' => 'Enable to send an email notification to admin on book request payed with Stripe.'));
                array_push($text, array('key' => 'SETTINGS_PAYMENT_GATEWAYS_STRIPE_SEND_USER_HELP',
                                        'parent' => 'PARENT_SETTINGS_PAYMENT_GATEWAYS_STRIPE_HELP',
                                        'text' => 'Enable to send an email notification to user on book request payed with Stripe.'));
                
                return $text;
            }

            /*
             * Stripe order text.
             * 
             * @param lang (array): current translation
             * 
             * @return array with updated translation
             */
            function order($text){
                array_push($text, array('key' => 'PARENT_ORDER_PAYMENT_GATEWAYS_STRIPE',
                                        'parent' => '',
                                        'text' => 'Order - Stripe'));
                /*
                 * Payment method.
                 */
                array_push($text, array('key' => 'ORDER_PAYMENT_METHOD_STRIPE',
                                        'parent' => 'PARENT_RESERVATIONS_RESERVATION',
                                        'text' => 'Stripe',
                                        'location' => 'all'));
                /*
                 * Front end.
                 */
                array_push($text, array('key' => 'ORDER_PAYMENT_GATEWAYS_STRIPE',
                                        'parent' => 'PARENT_ORDER_PAYMENT_GATEWAYS_STRIPE',
                                        'text' => 'Pay on Stripe (instant booking)',
                                        'de' => 'Bezahlung mit Stripe',
                                        'nl' => 'Betaal via Stripe (direct boeken)',
                                        'fr' => 'Payer sur Stripe (réservation instantanée)',
                                        'pl' => 'Pay on Stripe (instant booking)',
                                        'location' => 'all'));
                
                array_push($text, array('key' => 'ORDER_PAYMENT_GATEWAYS_STRIPE_CARD_TITLE',
                                        'parent' => 'PARENT_ORDER_PAYMENT_GATEWAYS_STRIPE',
                                        'text' => 'Stripe - Credit card',
                                        'location' => 'all'));
                array_push($text, array('key' => 'ORDER_PAYMENT_GATEWAYS_STRIPE_CARD_NUMBER',
                                        'parent' => 'PARENT_ORDER_PAYMENT_GATEWAYS_STRIPE',
                                        'text' => 'Card number (no dashes or spaces)',
                                        'location' => 'all'));
                array_push($text, array('key' => 'ORDER_PAYMENT_GATEWAYS_STRIPE_CARD_SECURITY_CODE',
                                        'parent' => 'PARENT_ORDER_PAYMENT_GATEWAYS_STRIPE',
                                        'text' => 'Security code (3 on back, Amex: 4 on front)',
                                        'location' => 'all'));
                array_push($text, array('key' => 'ORDER_PAYMENT_GATEWAYS_STRIPE_CARD_EXPIRATION_DATE',
                                        'parent' => 'PARENT_ORDER_PAYMENT_GATEWAYS_STRIPE',
                                        'text' => 'Expiration date (MM/YYYY)',
                                        'location' => 'all'));
                array_push($text, array('key' => 'ORDER_PAYMENT_GATEWAYS_STRIPE_CARD_NAME',
                                        'parent' => 'PARENT_ORDER_PAYMENT_GATEWAYS_STRIPE',
                                        'text' => 'Name (as it appears on your card)',
                                        'location' => 'all'));
                
                array_push($text, array('key' => 'ORDER_PAYMENT_GATEWAYS_STRIPE_SUCCESS',
                                        'parent' => 'PARENT_ORDER_PAYMENT_GATEWAYS_STRIPE',
                                        'text' => 'Your payment was approved and services are booked.',
                                        'de' => 'Ihre zahlung wurde akzeptiert und die leistungen sind gebucht.',
                                        'nl' => 'Uw betaling is goedgekeurd en de diensten zijn geboekt.',
                                        'fr' => 'Votre paiement a été approuvé et vos services sont réservés.',
                                        'pl' => 'Płatność została zaakceptowana i rezerwacj potwierdzona.',
                                        'location' => 'all')); 
                array_push($text, array('key' => 'ORDER_PAYMENT_GATEWAYS_STRIPE_CANCEL',
                                        'parent' => 'PARENT_ORDER_PAYMENT_GATEWAYS_STRIPE',
                                        'text' => 'You canceled your payment with Stripe. Please try again.',
                                        'location' => 'all')); 
                array_push($text, array('key' => 'ORDER_PAYMENT_GATEWAYS_STRIPE_ERROR',
                                        'parent' => 'PARENT_ORDER_PAYMENT_GATEWAYS_STRIPE',
                                        'text' => 'There was an error while processing Stripe payment. Please try again.',
                                        'de' => 'Es gab einen fehler während der Paypal-bezahlung. Bitte versuchen Sie es erneut.',
                                        'nl' => 'Er is een fout opgetreden tijdens het verwerken van Stripe-betaling. Probeer het opnieuw.',
                                        'fr' => 'Il y a eu une erreur lors du traitement de paiement Stripe. Veuillez essayer à nouveau.',
                                        'pl' => 'Wystapił bład podczas płatności, prosimy spróbować ponownie.',
                                        'location' => 'all'));
                
                return $text;
            }

            /*
             * Stripe email text.
             * 
             * @param lang (array): current translation
             * 
             * @return array with updated translation
             */
            function email($text){
                array_push($text, array('key' => 'PARENT_EMAILS_DEFAULT_STRIPE',
                                        'parent' => '',
                                        'text' => 'Email templates - Stripe default messages'));
                /*
                 * Admin
                 */
                array_push($text, array('key' => 'EMAILS_EMAIL_TEMPLATE_SELECT_STRIPE_ADMIN',
                                        'parent' => 'PARENT_EMAILS_DEFAULT_STRIPE',
                                        'text' => 'Stripe admin notification'));
                array_push($text, array('key' => 'EMAILS_DEFAULT_STRIPE_ADMIN_SUBJECT',
                                        'parent' => 'PARENT_EMAILS_DEFAULT_STRIPE',
                                        'text' => 'You received a booking request.',
                                        'de' => 'Sie haben eine buchungsanfrage erhalten.',
                                        'nl' => 'U heeft een boekingsaanvraag ontvangen',
                                        'fr' => 'Vous avez reçu une demande de réservation.',
                                        'pl' => 'Otrzymałeś nową rezerwację.'));
                array_push($text, array('key' => 'EMAILS_DEFAULT_STRIPE_ADMIN',
                                        'parent' => 'PARENT_EMAILS_DEFAULT_STRIPE',
                                        'text' => 'Below are the details. Payment has been done via Stripe and the period has been booked.',
                                        'de' => 'Details finden sie nachfolgend. Es wurde mit Stripe bezahlt und der zeitraum wurde gebucht.',
                                        'nl' => 'Hieronder staan de gegevens. De betaling is gedaan via Stripe en de periode is geboekt.',
                                        'fr' => 'Voici les détails. Le paiement a été effectué via Stripe et la période a été réservée.',
                                        'pl' => 'Szczegóły zamówienia, rezerwacja została opłacona i termin zarezerwowany.'));
                /*
                 * User
                 */
                array_push($text, array('key' => 'EMAILS_EMAIL_TEMPLATE_SELECT_STRIPE_USER',
                                        'parent' => 'PARENT_EMAILS_DEFAULT_STRIPE',
                                        'text' => 'Stripe user notification'));
                array_push($text, array('key' => 'EMAILS_DEFAULT_STRIPE_USER_SUBJECT',
                                        'parent' => 'PARENT_EMAILS_DEFAULT_STRIPE',
                                        'text' => 'Your booking request has been sent.',
                                        'de' => 'Ihre Buchungsanfrage wurde versendet.',
                                        'nl' => 'Uw boekingsverzoek is verzonden.',
                                        'fr' => 'Votre demande de réservation a été envoyée.',
                                        'pl' => 'Zamówienie została wysłane.'));
                array_push($text, array('key' => 'EMAILS_DEFAULT_STRIPE_USER',
                                        'parent' => 'PARENT_EMAILS_DEFAULT_STRIPE',
                                        'text' => 'The period has been book. Below are the details.',
                                        'de' => 'Der zeitraum wurde gebucht. Details finden sie nachfolgend.',
                                        'nl' => 'De periode is geboekt. Hieronder staan de gegevens.',
                                        'fr' => 'La période a été réservée. Voici les détails.',
                                        'pl' => 'Termin został zarezerwowany.'));
                
                return $text;
            }
        }
    }