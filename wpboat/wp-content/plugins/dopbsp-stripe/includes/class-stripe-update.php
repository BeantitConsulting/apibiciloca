<?php

/*
 * Title                   : Pinpoint Booking System (WordPress Plugin) add-on: Stripe Payment
 * File                    : includes/class-stripe-update.php
 * Author                  : Dot on Paper
 * Copyright               : © 2016 Dot on Paper
 * Website                 : https://www.dotonpaper.net
 * Description             : Stripe update PHP class.
 */

    if (!class_exists('DOPBSPStripeUpdate')){
        class DOPBSPStripeUpdate{
            /*
             * Public variables.
             */
            public $platform = '';
            public $plugin_name = '';
            public $product_id = '';
            public $key = '';
            public $email = '';
            public $renew_url = '';
            public $instance = '';
            public $domain = '';
            public $software_version = '';
            public $plugin_or_theme = '';
            public $text_domain = '';
            
            /*
             * Constructor
             */
            function __construct(){
                add_action('init', array(&$this, 'update'), 12);
                
                $this->init();
            }
            
            /*
             * Initialize licence data.
             */
            function init(){
                $this->platform = 'https://pinpoint.world/shop/';
                $this->plugin_name = 'dopbsp-stripe/dopbsp-stripe.php';
                $this->product_id = 'Pinpoint Booking System WordPress Plugin add-on: Stripe Payment';
                $this->key = '';
                $this->email = '';
                $this->renew_url = $this->platform.'my-account';
                $this->instance = '';
                $this->domain = str_ireplace(array('http://', 'https://'), '', home_url());
                $this->software_version = '1.1.5';
                $this->plugin_or_theme = 'plugin';
                $this->text_domain = 'dopbsp-stripe';
            }
               
            /*
             * Check for updates.
             */
            function update(){
                global $DOPBSP;

                $settings_general = $DOPBSP->classes->backend_settings->values(0,
                                                                               'general');

                $this->email = $settings_general->stripe_licence_email;
                $this->instance = $settings_general->stripe_licence_instance;
                $this->key = $settings_general->stripe_licence_key;
                $this->status = $settings_general->stripe_licence_status;
                
                return DOPBSPStripeUpdateAPI::instance($this->platform,
                                                          $this->plugin_name,
                                                          $this->product_id,
                                                          $this->key,
                                                          $this->email,
                                                          $this->renew_url,
                                                          $this->instance,
                                                          $this->domain,
                                                          $this->software_version,
                                                          $this->plugin_or_theme,
                                                          $this->text_domain);
            }
        }
    }