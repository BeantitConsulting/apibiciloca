<?php

/*
 * Title                   : Pinpoint Booking System (WordPress Plugin) add-on: Stripe Payment
 * File                    : includes/class-stripe-settings.php
 * Author                  : Dot on Paper
 * Copyright               : © 2016 Dot on Paper
 * Website                 : https://www.dotonpaper.net
 * Description             : Stripe settings PHP class.
 */

    if (!class_exists('DOPBSPStripeSettings')){
        class DOPBSPStripeSettings{
            /*
             * Constructor
             */
            function __construct(){
                add_filter('dopbsp_filter_default_settings_general', array(&$this, 'defaults'), 10);
                add_filter('dopbsp_filter_default_settings_payment', array(&$this, 'set'), 10);
                add_filter('dopbsp_filter_default_settings_notifications', array(&$this, 'setNotifications'), 10);
            }
            
            /*
             * Set default licenses settings for Stripe.
             * 
             * @param default_global (array): default global options values
             * 
             * @return default licenses settings array for Stripe
             */
            function defaults($default_general){
                $default_general['stripe_licence_email'] = '';
                $default_general['stripe_licence_instance'] = '';
                $default_general['stripe_licence_key'] = '';
                $default_general['stripe_licence_status'] = 'deactivated';
                
                return $default_general;
            }
            
            /*
             * Set default payment settings for Stripe.
             * 
             * @param default_payment (array): default payment options values
             * 
             * @return default payment settings array for Stripe
             */
            function set($default_payment){
                $default_payment['stripe_enabled'] = 'false';
                $default_payment['stripe_outward'] = 'false';
                $default_payment['stripe_key_publishable'] = '';
                $default_payment['stripe_key_secret'] = '';
                $default_payment['stripe_test_enabled'] = 'false';
                $default_payment['stripe_test_key_publishable'] = '';
                $default_payment['stripe_test_key_secret'] = '';
                $default_payment['stripe_refund_enabled'] = 'false';
                $default_payment['stripe_refund_value'] = '100';
                $default_payment['stripe_refund_type'] = 'percent';
                $default_payment['stripe_redirect'] = '';
                
                return $default_payment;
            }
            
            /*
             * Set default notifications settings for Stripe.
             * 
             * @param default_notifications (array): default notifications options values
             * 
             * @return default notifications settings array for Stripe
             */
            function setNotifications($default_notifications){
                $default_notifications['send_stripe_admin'] = 'true';
                $default_notifications['send_stripe_user'] = 'true';
                
                return $default_notifications;
            }
        }
    }