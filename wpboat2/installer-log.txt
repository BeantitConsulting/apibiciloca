********************************************************************************
* DUPLICATOR-LITE: INSTALL-LOG
* VERSION: 1.2.30
* STEP-1 START @ 01:30:20
* NOTICE: Do NOT post this data to public sites or forums
********************************************************************************
PHP VERSION:	7.0.22-0ubuntu0.16.04.1 | SAPI: apache2handler
PHP TIME LIMIT:	[0] time limit restriction disabled
PHP MEMORY:	2048M | SUHOSIN: disabled
SERVER:		Apache/2.4.18 (Ubuntu)
DOC ROOT:	/var/www/apibiciloca/apibiciloca/wpboat2
DOC ROOT 755:	false
LOG FILE 644:	true
REQUEST URL:	http://www.h2683468.stratoserver.net/wpboat2/installer.php
SAFE MODE :	0
--------------------------------------
ARCHIVE EXTRACTION
--------------------------------------
NAME:	20171226_wpboat_80003ec400f2455e9579171226012431_archive.zip
SIZE:	46.88MB
ZIP:	Enabled (ZipArchive Support)

>>> START EXTRACTION:
ZipArchive Object
(
    [status] => 0
    [statusSys] => 0
    [numFiles] => 6867
    [filename] => /var/www/apibiciloca/apibiciloca/wpboat2/20171226_wpboat_80003ec400f2455e9579171226012431_archive.zip
    [comment] => 
)
File timestamp is 'Current' mode: 2017-12-26 01:30:22
<<< EXTRACTION COMPLETE: true

WEB SERVER CONFIGURATION FILE RESET:
- Backup of .htaccess/web.config made to *.171226013022.orig
- Reset of .htaccess/web.config files

STEP-1 COMPLETE @ 01:30:22 - RUNTIME: 2.1760 sec.


********************************************************************************
* DUPLICATOR-LITE: INSTALL-LOG
* STEP-2 START @ 01:35:34
* NOTICE: Do NOT post to public sites or forums
********************************************************************************
--------------------------------------
DATABASE ENVIRONMENT
--------------------------------------
MYSQL VERSION:	This Server: 5.7.20 -- Build Server: 5.7.20
FILE SIZE:	database.sql (1.54MB) - installer-data.sql (1.54MB)
TIMEOUT:	5000
MAXPACK:	16777216
SQLMODE:	ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION
NEW SQL FILE:	[/var/www/apibiciloca/apibiciloca/wpboat2/installer-data.sql]
--------------------------------------
DATABASE RESULTS
--------------------------------------
ERRORS FOUND:	0
TABLES DROPPED:	0
QUERIES RAN:	185

wp_commentmeta: (0)
wp_comments: (1)
wp_duplicator_packages: (1)
wp_links: (0)
wp_nextend2_image_storage: (5)
wp_nextend2_section_storage: (2)
wp_nextend2_smartslider3_generators: (0)
wp_nextend2_smartslider3_sliders: (1)
wp_nextend2_smartslider3_sliders_xref: (0)
wp_nextend2_smartslider3_slides: (1)
wp_options: (235)
wp_postmeta: (568)
wp_posts: (210)
wp_term_relationships: (7)
wp_term_taxonomy: (2)
wp_termmeta: (0)
wp_terms: (2)
wp_usermeta: (23)
wp_users: (1)

Removed '84' cache/transient rows

CREATE/INSTALL RUNTIME: 1.4229 sec.
STEP-2 COMPLETE @ 01:35:36 - RUNTIME: 1.4342 sec.


********************************************************************************
* DUPLICATOR-LITE: INSTALL-LOG
* STEP-3 START @ 01:35:44
* NOTICE: Do NOT post to public sites or forums
********************************************************************************
CHARSET SERVER:	latin1
CHARSET CLIENT:	utf8
--------------------------------------
SERIALIZER ENGINE
[*] scan every column
[~] scan only text columns
[^] no searchable columns
--------------------------------------
wp_commentmeta^ (0)
wp_comments~ (1)
wp_duplicator_packages^ (0)
wp_links^ (0)
wp_nextend2_image_storage~ (5)
wp_nextend2_section_storage~ (2)
wp_nextend2_smartslider3_generators^ (0)
wp_nextend2_smartslider3_sliders~ (1)
wp_nextend2_smartslider3_sliders_xref^ (0)
wp_nextend2_smartslider3_slides~ (1)
wp_options~ (150)
wp_postmeta~ (568)
wp_posts~ (210)
wp_term_relationships~ (7)
wp_term_taxonomy~ (2)
wp_termmeta^ (0)
wp_terms~ (2)
wp_usermeta~ (23)
wp_users~ (1)
--------------------------------------
Search1:	'/var/www/apibiciloca/apibiciloca/wpboat' 
Change1:	'/var/www/apibiciloca/apibiciloca/wpboat2' 
Search2:	'\/var\/www\/apibiciloca\/apibiciloca\/wpboat' 
Change2:	'\/var\/www\/apibiciloca\/apibiciloca\/wpboat2' 
Search3:	'%2Fvar%2Fwww%2Fapibiciloca%2Fapibiciloca%2Fwpboat%2F' 
Change3:	'%2Fvar%2Fwww%2Fapibiciloca%2Fapibiciloca%2Fwpboat2%2F' 
Search4:	'\var\www\apibiciloca\apibiciloca\wpboat' 
Change4:	'/var/www/apibiciloca/apibiciloca/wpboat2' 
Search5:	'//www.h2683468.stratoserver.net/wpboat' 
Change5:	'//www.h2683468.stratoserver.net/wpboat2' 
Search6:	'\/\/www.h2683468.stratoserver.net\/wpboat' 
Change6:	'\/\/www.h2683468.stratoserver.net\/wpboat2' 
Search7:	'%2F%2Fwww.h2683468.stratoserver.net%2Fwpboat' 
Change7:	'%2F%2Fwww.h2683468.stratoserver.net%2Fwpboat2' 
SCANNED:	Tables:19 	|	 Rows:973 	|	 Cells:7914 
UPDATED:	Tables:1 	|	 Rows:216 	|	 Cells:253 
ERRORS:		0 
RUNTIME:	0.082400 sec

====================================
CONFIGURATION FILE UPDATES:
====================================

UPDATED WP-CONFIG: /wp-config.php' (if present)

WEB SERVER CONFIGURATION FILE BASIC SETUP:
- Preparing .htaccess file with basic setup.
Basic .htaccess file edit complete.  If using IIS web.config this process will need to be done manually.

====================================
GENERAL UPDATES & CLEANUP:
====================================

- Created directory wp-snapshots
- Created file wp-snapshots/index.php

====================================
NOTICES
====================================

No Notices Found


STEP 3 COMPLETE @ 01:35:44 - RUNTIME: 0.1845 sec.


